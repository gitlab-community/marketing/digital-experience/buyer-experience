`For small requests, please use the engineering-issue or marketing-site-bug templates.`

## Objective 
`Please provide a brief description of your request and check one of the options below`

- [ ] Update existing page(s) `Provide URL(s)`
- [ ] Net-new page
- [ ] Add promotional assets `Include promo start and end dates`
- [ ] Other `Please specify`

### Outcome
 - `Please describe how success will be measured`

## Details
`Please answer the questions below`

### Content
Please use this [Content template](https://docs.google.com/document/d/18bI2kV0wilJhuf7hba7OZvGqv5XfGpKtlOJbcSv7Zr4/edit?usp=sharing)

- [ ] Content is ready `Insert Google doc link`
- [ ] I will provide content `Insert date when content will be provided`
- [ ] I need content support `Please connect with @gmadlinger from the Content team`

### Stakeholders
`List team members who will need to review`
- [ ] `@name`
- [ ] `@name`

### Desired Go To Market date and why
- `Insert due date:` YYYY-MM-DD
- `Include business reason`

#### Priority level
- [ ] Low
- [ ] Medium
- [ ] High `Include business reason`

### Localization
- [ ] This content is already localized
- [ ] This content needs to be localized `Please connect with @mjsibanez from the Localization team`
- [ ] This content does not need to be localized

## For DEX
`This section is for the Digital Experience team to complete`

### Files
- [Final content doc] `Insert when ready`
- [Approved Figma design] `Insert when ready`

### URL
`Provide URL if the page is new`
- `URL`

### Requirements
`Please check all that apply`

- [ ] Copywriting
- [ ] SEO
- [ ] Research
- [ ] Data / Analytics
- [ ] UX Design
- [ ] Engineering

### Scope
- [ ] Deliverable 1
- [ ] Deliverable 2

### Localization details
- `Provide details when available`

## DCI
[DRI, Consulted, Informed](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/#dri-consulted-informed-dci)

- [ ] DRI:
- [ ] Consulted:
- [ ] Informed: `@fqureshi`

/label ~"dex-status::triage"
