## Goal

We would like to add a DevSecOps World Tour event to the marketing site. 

### Details

- [ ] `city`
- [ ] `dates of event`
- [ ] `content link (agenda, copy, ect)` or `use content from this DSOWT page`
- [ ] `languages` please note, i18n is not available at this time. List your preferences here.
- [ ] `Marketo Form ID` This is needed from Marketing Operations
- [ ] `Release Date` This is needed from Marketing Operations

### Steps - Self Serve Resources

Handbook documnetation

1. Create contentful entry
2. Add event to /events/ 
3. Add event to /events/devsecops-world-tour/ 

### Graphics 

- [ ] `image of city`
- [ ] `landing page hero image of city`
- [ ] `hover image`

## Page(s)
Which page(s) are involved in this request?
* https://about.gitlab.com/events/devsecops-world-tour 
* https://about.gitlab.com/events/devsecops-world-tour/city/
* https://about.gitlab.com/events/devsecops-world-tour/city/partner-leadership-summit/


/label ~"dex-status::triage"

