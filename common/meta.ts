import {
  MetaInfo,
  MetaPropertyCharset,
  MetaPropertyEquiv,
  MetaPropertyMicrodata,
  MetaPropertyName,
  MetaPropertyProperty,
} from 'vue-meta/types/vue-meta';
import { NuxtI18nMeta } from '@nuxtjs/i18n/types/vue';
import { MetadataDTO } from '../models/page.dto';
import { createSchemaFaq, createSchemaPage } from './craftSchema';
import {
  DEFAULT_META_DESCRIPTION,
  DEFAULT_OPENGRAPH_IMAGE,
  DEFAULT_SCHEMA_ORG,
  LOCALES,
  META_NAME,
  SITE_URL,
  TWITTER_CARD_CONTENT,
  TWITTER_CREATOR_CONTENT,
  TWITTER_SITE_CONTENT,
} from '~/common/constants';
import { localizedPageNames } from '../common/localized-pages';
import { noIndexUrl } from './no-index-url';

function createImageUrl(imageUrl) {
  if (imageUrl.toString().includes('ctfassets')) {
    return `https:${imageUrl}`;
  } else {
    return `${SITE_URL}${imageUrl}`;
  }
}

function buildSchemaOrg(schemaOrg: string, description: string) {
  const schemaScript = {
    hid: 'schemaOrg',
    innerHTML: '',
    type: 'application/ld+json',
  };

  if (schemaOrg) {
    schemaScript.innerHTML = schemaOrg;
  } else {
    const schema = {
      ...DEFAULT_SCHEMA_ORG,
      description: description || DEFAULT_SCHEMA_ORG.description,
    };

    schemaScript.innerHTML = JSON.stringify(schema);
  }

  return schemaScript;
}

function buildTopicsSchema({
  title,
  description,
  path,
  topicName,
  topicsHeader,
  dateModified,
  datePublished,
}: {
  title?: string;
  description?: string;
  path?: string;
  datePublished?: string;
  dateModified?: string;
  topicName?: string;
  topicsHeader?: any;
}) {
  const schemaValues = {
    title,
    description,
    datePublished,
    dateModified: dateModified || datePublished,
    url: `${SITE_URL}${path}`,
    articleSection: topicName,
    timeRequired: topicsHeader?.data.read_time || '',
    image: '/nuxt-images/topics/gitops-topic.png',
  };

  return {
    hid: 'schemaTopics',
    json: createSchemaPage(schemaValues),
    type: 'application/ld+json',
  };
}

/**
 * This function verifies if the page has or does not have enabled the "nuxtI18nHead" as there is not a built-in way to do it.
 * When the option is not enabled and the nuxtI18nHead is used, the `href` attribute is the same for all positions, allowing to validate this if the page has enabled localization
 * @param nuxtI18nHead
 * @param path
 * Extra else if needed generate hreflang attributes with trailing slashes other than the homepage. See https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/issues/2556.
 * TODO: Remove this method when it is not used anymore in favor of "createLocalizedMeta"
 */
function getLocalizedMeta(
  path: string,
  nuxtI18nHead?: NuxtI18nMeta,
): Record<string, any>[] {
  const allMetaHrefs: string[] =
    nuxtI18nHead?.link.map(
      (metaObject) =>
        `${metaObject.href}${metaObject.href.at(-1) !== '/' ? '/' : ''}`,
    ) ?? [];

  const uniqueMetaHrefs: string[] = [...new Set(allMetaHrefs)];
  const pageNameSections = path.split('/');
  const pageName = pageNameSections[pageNameSections.length - 1];
  // noIndex page will include last 2 sections of the path and ensures that the path ends with a slash
  const noIndexPage = `/${pageNameSections.slice(1).join('/')}/`.replace(
    /\/+$/,
    '/',
  );
  const isTemplatePage = path.includes('/get-started/');
  const isLocalized = localizedPageNames.includes(pageName);
  const isNoIndexUrl = noIndexUrl.includes(noIndexPage);
  if (isNoIndexUrl) {
    return [];
  } else if (!isLocalized && isTemplatePage) {
    const newMeta = {
      rel: 'alternate',
      hreflang: 'en-us',
      href: `${SITE_URL}${path}`,
    };
    return [newMeta];
  } else {
    const regexPathHasLang = /^\/([a-z]{2})(-[a-z]{2})?\//im;
    const newMeta: any[] = uniqueMetaHrefs
      .map((href) => {
        const langCodeMatch: string[] = href.match(regexPathHasLang) ?? [];
        const langCode =
          langCodeMatch.length > 0
            ? langCodeMatch[1] + (langCodeMatch[2] || '')
            : 'x-default';
        const rel = 'alternate';
        const hreflang = langCode;
        let newHref = `${SITE_URL}${href}`;
        if (newHref.includes('?')) {
          return (newHref = newHref.split('?')[0].toLowerCase());
        }
        const additionalHreflangs = [];
        // Add additional hreflangs for non-region alternatives only for French and German
        if (langCode === 'fr-fr') {
          additionalHreflangs.push('fr');
        } else if (langCode === 'de-de') {
          additionalHreflangs.push('de');
        }
        const allHreflangs = [hreflang, ...additionalHreflangs];
        return allHreflangs.map((hreflang) => ({
          rel,
          hreflang,
          href: newHref,
        }));
      })
      .flat();
    return newMeta;
  }
}

/**
 * Function that receives the available languages of a page and builds the respective hreflang attributes of these languages avoiding the generation of the
 * hreflang attributes of the languages that does not belong to that page.
 * @param availableLocales
 * @param nuxtI18nHead
 */
export const buildLocalizedMeta = (
  availableLocales: Array<string>,
  nuxtI18nHead: NuxtI18nMeta,
) => {
  const [defaultLocale] = LOCALES.DEFAULT.split('-');

  return nuxtI18nHead.link
    .filter((item) => {
      // Avoids duplicated 'x-default' language
      if (item.hreflang === defaultLocale) {
        return false;
      }

      const hasLanguage = availableLocales.some((locale) => {
        const [lang] = locale.split('-');
        return item.hreflang?.includes(lang) || item.hreflang === 'x-default';
      });

      return !(
        !hasLanguage ||
        (hasLanguage && item.hreflang?.toLowerCase() === LOCALES.DEFAULT)
      );
    })
    .map((item) => {
      if (item.href) {
        const hasQueryParams = item.href.includes('?');
        item.href = `${SITE_URL}${item.href}${hasQueryParams ? '' : '/'}`;
        item.hreflang = item.hreflang.toLowerCase();
      }
      return item;
    });
};

export function getPageMetadata(
  pageData: MetadataDTO,
  path: string,
  nuxtI18nHead?: NuxtI18nMeta,
  availableLanguages?: Array<string>,
): MetaInfo {
  let {
    description,
    title,
    image_title: imageTitle,
    twitter_image: twitterImage,
    image_alt: imageAlt,
    schema_faq: schemaFaq,
    schema_org: schemaOrg,
    topic_name: topicName,
    topicsHeader,
    date_published: datePublished,
    date_modified: dateModified,
    noIndex: noindex,
  } = pageData || {};
  const meta: (
    | MetaPropertyCharset
    | MetaPropertyEquiv
    | MetaPropertyName
    | MetaPropertyMicrodata
    | MetaPropertyProperty
    | any
  )[] = [];
  const script: any = [];
  let link = [];

  if (!description) {
    description = DEFAULT_META_DESCRIPTION;
  }

  meta.push({
    hid: META_NAME.description,
    name: META_NAME.description,
    content: description,
  });
  meta.push({
    hid: META_NAME.twitterDescription,
    name: META_NAME.twitterDescription,
    content: description,
  });

  meta.push({
    hid: META_NAME.ogDescription,
    property: META_NAME.ogDescription,
    content: description,
  });

  meta.push({
    hid: META_NAME.ogTitle,
    property: META_NAME.ogTitle,
    content: title,
  });

  meta.push({
    hid: META_NAME.twitterCreator,
    name: META_NAME.twitterCreator,
    content: TWITTER_CREATOR_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterSite,
    name: META_NAME.twitterSite,
    content: TWITTER_SITE_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterCard,
    name: META_NAME.twitterCard,
    content: TWITTER_CARD_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterAltImage,
    name: META_NAME.twitterAltImage,
    content: imageAlt,
  });

  meta.push({
    hid: META_NAME.ogImageAlt,
    property: META_NAME.ogImageAlt,
    content: imageAlt,
  });

  if (path) {
    meta.push({
      hid: META_NAME.ogUrl,
      name: META_NAME.ogUrl,
      content: `${SITE_URL}${path}`,
    });
  }

  if (title) {
    meta.push({
      hid: META_NAME.ogType,
      property: META_NAME.ogType,
      content: META_NAME.article,
    });
    meta.push({
      hid: META_NAME.twitterTitle,
      name: META_NAME.twitterTitle,
      content: title,
    });
    title = `${title} | GitLab`;
  } else {
    title = `GitLab`;
    meta.push({
      hid: META_NAME.ogType,
      content: META_NAME.website,
      property: META_NAME.ogType,
    });
  }

  if (
    noindex === true ||
    (process.env.NODE_ENV === 'development' && !process.env.CI_COMMIT_BRANCH) ||
    process.env.CI_COMMIT_BRANCH !== process.env.CI_DEFAULT_BRANCH
  ) {
    meta.push({
      hid: 'robots',
      name: 'robots',
      content: 'noindex, nofollow',
    });
  }

  if (imageTitle) {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: createImageUrl(imageTitle),
    });
    meta.push({
      hid: META_NAME.ogImage,
      property: META_NAME.ogImage,
      content: createImageUrl(imageTitle),
    });
  } else if (twitterImage) {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: createImageUrl(twitterImage),
    });
    meta.push({
      hid: META_NAME.ogImage,
      property: META_NAME.ogImage,
      content: createImageUrl(twitterImage),
    });
  } else {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${DEFAULT_OPENGRAPH_IMAGE}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      property: META_NAME.ogImage,
      content: `${SITE_URL}${DEFAULT_OPENGRAPH_IMAGE}`,
    });
  }

  if (schemaFaq) {
    script.push({
      hid: 'schemaFaq',
      json: createSchemaFaq(schemaFaq),
      type: 'application/ld+json',
    });
  }

  if (path.includes('topics')) {
    script.push(
      buildTopicsSchema({
        title,
        description,
        path,
        topicName,
        topicsHeader,
        dateModified,
        datePublished,
      }),
    );
  }

  script.push(buildSchemaOrg(schemaOrg, description));

  if (!availableLanguages) {
    const localizedMeta = getLocalizedMeta(path, nuxtI18nHead);
    link = [...link, ...localizedMeta];
  } else if (availableLanguages) {
    const localizedMeta = buildLocalizedMeta(availableLanguages, nuxtI18nHead);
    link = [...link, ...localizedMeta];
  }

  return {
    title,
    meta,
    link,
    script,
    __dangerouslyDisableSanitizers: ['script', 'innerHTML'],
  };
}

export function generateCanonical(path: string) {
  let canonicalPath;
  if (path === '/') {
    canonicalPath = `${SITE_URL}/`;
  } else if (path.endsWith('/')) {
    canonicalPath = `${SITE_URL}${path}`;
  } else {
    canonicalPath = `${SITE_URL}${path}/`;
  }

  return {
    rel: 'canonical',
    href: canonicalPath,
  };
}

export function generateLayoutMeta(path, locale): MetaInfo {
  return {
    link: [generateCanonical(path)],
    htmlAttrs: {
      lang: locale,
    },
  };
}
