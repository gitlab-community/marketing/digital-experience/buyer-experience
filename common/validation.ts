import { BaseForm } from '~/interfaces';
import { CtfEntry } from '../models';
interface Navigation {
  freeTrial: CtfEntry<any>;
  sales: CtfEntry<any>;
  login: CtfEntry<any>;
  navItems: CtfEntry<any>[];
  logo: CtfEntry<any>;
}

interface Footer {
  text: string;
  columns: CtfEntry<any>[];
  sourceCtaText: string;
  editCtaText: string;
  contributeCtaText: string;
}

export const validateNavigationResponse = (
  navigation: CtfEntry<Navigation>,
) => {
  const fields: Navigation = navigation.fields;

  if (fields.navItems.length < 1) {
    // eslint-disable-next-line
    console.error(`Check nav items array for missing links`);
    throw new Error();
  }

  return navigation;
};

export const validateFooterResponse = (footer: CtfEntry<Footer>) => {
  const fields: Footer = footer.fields;

  if (fields.columns.length < 1) {
    console.error(`Check footer columns array for missing links`);
    throw new Error();
  }

  return footer;
};

export const validateMarketoForm = (formData: BaseForm, pageRoute: string) => {
  if (formData.external_form) {
    return;
  }
  if (!formData.formId) {
    // eslint-disable-next-line no-console
    console.error(
      `Marketo Form error for ${pageRoute}: formId is required but missing in the Marketo Form used on this page.`,
    );
    throw new Error();
  }

  if (!formData.datalayer && !formData.formDataLayer && !formData.dataLayer) {
    // eslint-disable-next-line no-console
    console.warn(
      `Marketo Form warning for ${pageRoute}: A datalayer/form name value is not provided in the form component with formId: ${formData.formId}. Using fallback value "mktoFormFill_uncaught".`,
    );
  }
};

export const validateNavigationVariantResponse = (
  variant: CtfEntry<Navigation>,
) => {
  const fields: Navigation = variant.fields;

  if (fields.navItems.length < 1) {
    // eslint-disable-next-line
    console.error(`Check nav items array for missing links`);
    throw new Error();
  }

  return variant;
};
