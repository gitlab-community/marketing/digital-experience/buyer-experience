---
  title: Plattform
  description: Erfahre mehr darüber, wie die GitLab-Plattform Teams dabei helfen kann, zusammenzuarbeiten und Software schneller zu entwickeln.
  hero:
    note: Die umfangreichste
    header: KI-gestützte
    sub_header: DevSecOps-Plattform
    description: Liefere bessere Software schneller mit einer Plattform für deinen gesamten Lebenszyklus der Softwareentwicklung (SDLC).
    image:
      src: /nuxt-images/platform/loop-shield-duo.svg
      alt: The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).
    button:
      text: Kostenlos testen
      href: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/de-de/platform&glm_content=default-saas-trial
      variant: secondary
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      text: Erfahre mehr über die Preise
      href: /pricing/
      variant: tertiary
      icon: chevron-lg-right
      data_ga_name: pricing
      data_ga_location: hero
  table_data:
    - name: "Planen"
      link: "https://about.gitlab.com/solutions/visibility-measurement/"
      icon: "plan-alt-2"
      features:
        - name: "DevOps-Berichte"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#devops-reports"
        - name: "DORA-Metriken"
          link: "https://about.gitlab.com/solutions/value-stream-management/dora/"
        - name: "Verwaltung der Wertschöpfungskette"
          link: "https://about.gitlab.com/solutions/value-stream-management/"
        - name: "Pages"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#pages"
        - name: "Wiki"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#wiki"
        - name: "Portfolio-Management"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#portfolio-management"
        - name: "Teamplanung"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#team-planning"
        - name: "Ticket-Beschreibung generieren"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#generate-issue-description"
        - name: "Diskussionszusammenfassung"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#discussion-summary"
      replacement:
        text: Replacement for
        product: Sentry
    - name: "Quellcodeverwaltung"
      link: "https://about.gitlab.com/solutions/delivery-automation/"
      icon: "cog-code"
      features:
        - name: "Remote-Entwicklung"
          link: "https://about.gitlab.com/solutions/delivery-automation/#remote-development"
        - name: "Quellcodeverwaltung"
          link: "https://about.gitlab.com/solutions/source-code-management/"
        - name: "Web IDE"
          link: "https://about.gitlab.com/solutions/delivery-automation/#web-ide"
        - name: "GitLab CLI"
          link: "https://about.gitlab.com/solutions/delivery-automation/#gitlab-cli"
        - name: "Code-Review-Workflow"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-review-workflow"
        - name: "Codevorschläge"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-suggestions"
        - name: "Codeerläuterung"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-explanation"
        - name: "Code-Review-Zusammenfassung"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-review-summary"
        - name: "Testgenerierung"
          link: "https://about.gitlab.com/solutions/delivery-automation/#test-generation"
        - name: "Erstellung von Vorlagen für Merge Requests"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#merge-request-template-population"
      replacement:
        text: Replacement for
        product: GitHub
    - name: "Kontinuierliche Integration"
      link: "https://about.gitlab.com/solutions/delivery-automation/"
      icon: "automated-code-alt"
      features:
        - name: "Geheimnismanagement"
          link: "https://about.gitlab.com/solutions/delivery-automation/#secrets-management"
        - name: "Review Apps"
          link: "https://about.gitlab.com/solutions/delivery-automation/#review-apps"
        - name: "Codetests und -abdeckung"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-testing-and-coverage"
        - name: "Merge-Züge"
          link: "https://about.gitlab.com/solutions/delivery-automation/#merge-trains"
        - name: "Vorgeschlagene Prüfer(innen)"
          link: "https://about.gitlab.com/solutions/delivery-automation/#suggested-reviewers"
        - name: "Zusammenfassung von Merge Requests"
          link: "https://about.gitlab.com/solutions/delivery-automation/#merge-request-summary"
        - name: "Analyse der Grundursachen"
          link: "https://about.gitlab.com/solutions/delivery-automation/#root-cause-analysis"
        - name: "Diskussionszusammenfassung"
          link: "https://about.gitlab.com/solutions/delivery-automation/#discussion-summary"
      replacement:
        text: Replacement for <span>Jenkins</span>
    - name: "Sicherheit"
      link: "https://about.gitlab.com/solutions/security-compliance/"
      icon: "secure-alt-2"
      features:
        - name: "Container-Scanning"
          link: "https://about.gitlab.com/solutions/security-compliance/#container-scanning"
        - name: "Analyse der Softwarezusammensetzung"
          link: "https://about.gitlab.com/solutions/security-compliance/#software-composition-analysis"
        - name: "API-Sicherheit"
          link: "https://about.gitlab.com/solutions/security-compliance/#api-security"
        - name: "Abdeckungs-Fuzzing"
          link: "https://about.gitlab.com/solutions/security-compliance/#fuzz-testing"
        - name: "DAST"
          link: "https://about.gitlab.com/solutions/security-compliance/#dast"
        - name: "Codequalität"
          link: "https://about.gitlab.com/solutions/security-compliance/#code-quality"
        - name: "Erkennung von Geheimnissen"
          link: "https://about.gitlab.com/solutions/security-compliance/#secret-detection"
        - name: "SAST"
          link: "https://about.gitlab.com/solutions/security-compliance/#sast"
        - name: "Erläuterung von Sicherheitslücken"
          link: "https://about.gitlab.com/solutions/security-compliance/#vulnerability-explanation"
        - name: "Behebung von Sicherheitslücken"
          link: "https://about.gitlab.com/solutions/security-compliance/#vulnerability-resolution"
      replacement:
        text: Replacement for
        product: Snyk
    - name: "Konformität"
      icon: "protect-alt-2"
      link: "https://about.gitlab.com/solutions/security-compliance/"
      features:
        - name: "Release-Nachweis"
          link: "https://about.gitlab.com/solutions/security-compliance/#release-evidence"
        - name: "Konformitätsmanagement"
          link: "https://about.gitlab.com/solutions/security-compliance/#compliance-management"
        - name: "Audit-Ereignisse"
          link: "https://about.gitlab.com/solutions/security-compliance/#audit-events"
        - name: "Software-Stückliste"
          link: "https://about.gitlab.com/solutions/security-compliance/#software-bill-of-materials"
        - name: "Abhängigkeitsmanagement"
          link: "https://about.gitlab.com/solutions/security-compliance/#dependency-management"
        - name: "Verwaltung von Sicherheitslücken"
          link: "https://about.gitlab.com/solutions/security-compliance/#vulnerability-management"
        - name: "Verwaltung von Sicherheitsrichtlinien"
          link: "https://about.gitlab.com/solutions/security-compliance/#security-policy-management"
    - name: "Artefakt-Registry"
      link: "https://about.gitlab.com/solutions/delivery-automation/"
      icon: "package-alt-2"
      features:
        - name: "Virtuelle Registry"
          link: "https://about.gitlab.com/solutions/delivery-automation/#virtual-registry"
        - name: "Container-Registry"
          link: "https://about.gitlab.com/solutions/delivery-automation/#container-registry"
        - name: "Helm-Chart-Registry"
          link: "https://about.gitlab.com/solutions/delivery-automation/#helm-chart-registry"
        - name: "Paket-Registry"
          link: "https://about.gitlab.com/solutions/delivery-automation/#package-registry"
      replacement:
        text: Replacement for
        product: JFrog
    - name: "Kontinuierliche Bereitstellung"
      icon: "continuous-delivery-alt"
      link: "https://about.gitlab.com/solutions/delivery-automation/"
      features:
        - name: "Freigabe-Orchestrierung"
          link: "https://about.gitlab.com/solutions/delivery-automation/#release-orchestration"
        - name: "Infrastructure as Code"
          link: "https://about.gitlab.com/solutions/delivery-automation/#infrastructure-as-code"
        - name: "Feature-Flags"
          link: "https://about.gitlab.com/solutions/delivery-automation/#feature-flags"
        - name: "Umgebungsmanagement"
          link: "https://about.gitlab.com/solutions/delivery-automation/#environment-management"
        - name: "Bereitstellungsmanagement"
          link: "https://about.gitlab.com/solutions/delivery-automation/#deployment-management"
        - name: "Auto-DevOps"
          link: "https://about.gitlab.com/solutions/delivery-automation/"
      replacement:
        text: Replacement for
        product: Harness
    - name: "Beobachtbarkeit"
      icon: "monitor-alt-2"
      link: "https://about.gitlab.com/solutions/visibility-measurement/"
      features:
        - name: "Service-Desk"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#service-desk"
        - name: "Verwaltung des Bereitschaftsdienstes"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#oncall-schedule-management"
        - name: "Vorfallmanagement"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#incident-management"
        - name: "Fehlerverfolgung"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#error-tracking"
        - name: "Visualisierung der Produktanalyse"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#product-analytics-visualization"
        - name: "Prognose zur Wertschöpfungskette"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#value-stream-forecasting"
        - name: "KI-Produktanalyse"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#ai-product-analytics"
        - name: "Metrics"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#metrics"
        - name: "Distributed Tracing"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#distributed-tracing"
        - name: "Logs"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#logs"
      replacement:
        text: Replacement for
        product: Sentry
  benefits:
    title: Einheitliche Plattform
    subtitle: zur Förderung von Dev-, Sec- und Ops-Teams
    image:
      image_url: "/nuxt-images/platform/one-platform.svg"
      alt: code source image
    is_accordion: true
    tabs:
      - tabButtonText: Entwicklung
        data_ga_name: development
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: KI-gestützter Workflow
              text: Steigere die Effizienz und verkürze die Zykluszeiten aller Benutzer(innen) mit Hilfe von KI in jeder Phase des Software-Entwicklungsprozesses - von der Planung und Erstellung von Code bis hin zu Tests, Sicherheit und Überwachung.
              headerCtas:
                - externalUrl: /gitlab-duo/
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
              ctaHeader: "See it in action:"
              ctas:
                - externalUrl: https://player.vimeo.com/video/855805049
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/894621401
                  text: Codevorschläge
                  data_ga_name: Code Suggestions
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/927753737
                  text: Chat
                  data_ga_name: Chat
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Einzelanwendung
              text: GitLab vereint alle DevSecOps-Funktionen in einer einzigen Anwendung mit einem einheitlichen Datenspeicher, sodass alles an einem Ort ist.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781
                  text: Video über die Verwendung von DORA-Metriken bei GitLab
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062?h=752d064728&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: Video zum Wertstrom-Dashboard von GitLab
                  data_ga_name: GitLab's Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Höhere Entwicklerproduktivität
              text: Die Einzelanwendung von GitLab bietet eine überragende Benutzererfahrung, die die Bearbeitungszeit verkürzt und Kontextwechsel vermeidet.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925629920
                  text: Video zum Portfolio-Management von GitLab
                  data_ga_name: GitLab's Portfolio Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925632272
                  text: Video zum OKR-Management von GitLab
                  data_ga_name: GitLab's OKR Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925633691
                  text: Video über Design-Uploads zu GitLab-Tickets
                  data_ga_name: Design Uploads to GitLab issues
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Bessere Automatisierung
              text: Die Automatisierungstools von GitLab sind zuverlässiger, bieten mehr Funktionen und tragen dazu bei, kognitive Belastungen und unnötige Routinearbeiten zu eliminieren.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023715
                  text: Video zur Übersicht über CD bei GitLab
                  data_ga_name: GitLab's CD Overview
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://docs.gitlab.com/ee/operations/error_tracking.html
                  text: Dokumentation zur Fehlerverfolgung
                  data_ga_name: Error tracking documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Dokumentation zum Vorfallmanagement
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/2DKPuM0RDt2jdfzIaTroKS/1aeee8ed2e7939523ef89e7046d5a468/iron-mountain.svg
                quote: Die Vision von GitLab, die Strategie mit dem Geltungsbereich und dem Code zu verknüpfen, ist sehr überzeugend. Ich schätze die kontinuierlichen Investitionen in die Plattform.
                metrics:
                  - number: 150 Tsd. USD
                    text: ungefähre Kosteneinsparung pro Jahr
                  - number: 20 Stunden
                    text: eingesparte Einarbeitungszeit pro Projekt
                author:
                  headshot: https://images.ctfassets.net/xz1dnu24egyd/1G9lo7UyVxvW6m1CTNND9b/7b8b0449fc8ae1299e3b4fc56f4dd363/JasonManoharan.png
                  name: Jason Monoharan.
                  title: VP of Technology.
                  company: Iron Mountain
                cta: 
                  text: Studie lesen
                  url: '/customers/iron-mountain/'
                  dataGaName: iron mountain case study
                  dataGaLocation: body
          cta:
            title: Nutze die Leistungsfähigkeit von KI mit
            highlight: GitLab Duo
            text: Mehr erfahren
            externalUrl: /gitlab-duo/
            data_ga_name: GitLab Duo
            data_ga_location: body
            additionalIcon: true
      - tabButtonText: Sicherheit
        data_ga_name: security
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Sicherheit ist integriert, nicht aufgesetzt
              text: Die Sicherheitsfunktionen von GitLab, wie DAST, Fuzz-Testing, Container-Scanning und API-Screening, sind durchgängig integriert.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925635707
                  text: Video über dynamische Anwendungssicherheitstests (DAST)
                  data_ga_name: Dynamic Application Security Testing (DAST) video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925676815
                  text: Video zum Container-Scanning
                  data_ga_name: Container scanning video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925677603
                  text: Video über API-Sicherheit und Web-API-Fuzzing
                  data_ga_name: API security and web API Fuzzing video
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Konformität und präzises Richtlinienmanagement
              text: GitLab bietet eine umfassende Governance-Lösung, die die Funktionstrennung zwischen den Teams ermöglicht. Der Richtlinieneditor von GitLab ermöglicht angepasste Approvalregeln, die auf die Konformitätsanforderungen jedes Unternehmens zugeschnitten sind – dies reduziert das Risiko.
              ctas:
                - externalUrl: /solutions/security-compliance/#compliance-management
                  text: Dokumentation zum Konformitätsmanagement
                  data_ga_name: Compliance Management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://player.vimeo.com/video/925679314
                  text: Video zum Compliance Framework von GitLab
                  data_ga_name: GitLab's Compliance Frameworks
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925679982
                  text: Video zum Anforderungsmanagement von GitLab
                  data_ga_name: GitLab's Requirements Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Sicherheitsautomatisierung
              text: Die fortschrittlichen Automatisierungstools von GitLab ermöglichen eine hohe Geschwindigkeit bei gleichzeitigem Einsatz von Schutzmaßnahmen und stellen sicher, dass Code automatisch auf Schwachstellen gescannt wird.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925680640
                  text: Video zum Sicherheits-Dashboard von GitLab
                  data_ga_name: GitLab's Security Dashboard
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/4mEBdmMQrpjOn1VJ2idd9h/39fbbf0fc290dda417a35d1e437de4a3/hackerone.svg
                quote: GitLab hilft uns, Sicherheitsschwachstellen frühzeitig zu erkennen und hat diesen Prozess in den Arbeitsablauf der Entwickler(innen) integriert.** Entwickler(innen) können Code in GitLab CI pushen und erhalten eine sofortige Rückmeldung von einem der vielen kaskadierenden Audit-Schritte. So können sie sofort sehen, ob es eine Sicherheitslücke gibt. Zusätzlich können Entwickler(innen) eigene neue Schritte erstellen, die ganz bestimmte Sicherheitsprobleme testen.
                metrics:
                  - number: 7,5 x
                    text: schnellere Pipeline-Zeit
                  - number: 4 Stunden
                    text: eingesparte Entwicklungszeit pro Ingenieur(in)/Woche
                author:
                  name: Mitch Trale
                  title: Head of Infrastructure
                  company: Hackerone
                cta: 
                  text: Studie lesen
                  url: '/customers/hackerone/'
                  dataGaName: hackerone case study
                  dataGaLocation: body
          cta:
            title: Hier erfährst du, wie du Sicherheitsscans zu deiner
            highlight: CI-Pipeline hinzufügen kannst
            text: Demo starten
            data_ga_name: ci pipeline
            data_ga_location: body
            modal: true
            iconName: slp-laptop-video
            demo:
              subtitle: Füge Sicherheitsscans zu deiner CI-/CD-Pipeline hinzu
              video:
                url: https://capture.navattic.com/clq78b76l001b0gjnbxbd5k1f
              scheduleButton:
                text: Schedule a custom demo
                href: /sales/
                data_ga_name: demo
                data_ga_location: body
      - tabButtonText: Betrieb
        data_ga_name: operations
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Skaliere Unternehmens-Workloads
              text: GitLab unterstützt Unternehmen in jeder Größenordnung und ermöglicht die Verwaltung und Aktualisierung nahezu ohne Ausfallzeiten.
              ctas:
                - externalUrl: https://docs.gitlab.com/ee/user/infrastructure/iac/
                  text: Dokumentation zu Infrastructure as Code (IaC)
                  data_ga_name: Infrastructure as code (IaC) documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Dokumentation zum Vorfallmanagement
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Unvergleichbare Metrik-Sichtbarkeit
              text: Der einheitliche Datenspeicher von GitLab bietet Analysen für den gesamten Lebenszyklus der Softwareentwicklung an einem Ort, sodass keine zusätzlichen Produkte integriert werden müssen.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781
                  text: Video über die Verwendung von DORA-Metriken bei GitLab
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062
                  text: Video zum Wertstrom-Dashboard von GitLab
                  data_ga_name: GitLab's Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Cloud-native-, Multi-Cloud- und Legacy-Unterstützung
              text: GitLab bietet eine komplette DevSecOps-Plattform, die es Teams unabhängig von deinem Infrastrukturmix ermöglicht, die gleichen Produktivitätsmetriken und Governance-Prozesse zu verwenden.
              ctas:
                - externalUrl: /topics/multicloud/
                  text: Multicloud-Dokumentation
                  data_ga_name: Multicloud documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: /solutions/gitops/
                  text: Dokumentation zu GitOps
                  data_ga_name: GitOps documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Niedrigere Gesamtbetriebskosten
              text: ''
              ctas:
                - description: "Hier erfährst du, wie das weltweit größte Verteidigungsunternehmen GitLab einsetzt, um Toolchains zu verkürzen, die Produktion zu beschleunigen und die Sicherheit zu verbessern:"
                  externalUrl: /customers/lockheed-martin/
                  text: Fallstudie Lockheed Martin
                  data_ga_name: Lockheed Martin case study
                  data_ga_location: body
                  iconName: gl-doc-text
                - description: 'Erfahre, wie CARFAX seine DevSecOps-Toolchain optimiert und die Sicherheit mit GitLab verbessert hat:'
                  externalUrl: /customers/carfax/
                  text: Fallstudie CARFAX
                  data_ga_name: CARFAX case study
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/rdWpo3pKJBUFLzDFmTNIE/d69e61550635ebba0556216c44d401a7/forrester-logo.svg
                quote: Verbesserte Entwicklungs- und Liefereffizienz um über 87 %, was zu Einsparungen von über 23 Mio. US-Dollar führte. GitLab ermöglichte es den Unternehmen, den Zeitaufwand für jede Phase des gesamten DevOps-Lebenszyklus drastisch zu reduzieren.“
                metrics:
                  - number: 200,5 Mio. USD
                    text: Gesamtvorteile über drei Jahre
                  - number: 427 %
                    text: Gesamtrendite (ROI)
                cta: 
                  text: Studie lesen
                  url: 'https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html'
                  dataGaName: resources study forrester
                  dataGaLocation: body
          cta:
            title: Wie viel kostet
            highlight: deine Toolchain?
            text: Teste unseren ROI-Rechner
            externalUrl: /calculator/
            data_ga_name: try our roi calculator
            data_ga_location: body
  video_spotlight:
    header: Möchtest du die Geschwindigkeit erhöhen? Konsolidiere noch heute deine Toolchain.
    list:
      - text: Zusammenarbeit verbessern
        iconName: gl-check
      - text: Admin-Belastung reduzieren
        iconName: gl-check
      - text: Sicherheit erhöhen
        iconName: gl-check
      - text: Niedrigere Gesamtbetriebskosten
        iconName: gl-check
      - text: Nahtlos skalieren
        iconName: gl-check
    blurb: |
      **Du weißt nicht, wo du anfangen sollst?**
      Unser Vertriebsteam kann dir helfen.
    video:
      title: Warum GitLab?
      url: https://player.vimeo.com/video/799236905?h=4eee39a447
      text: Erfahre mehr
      data_ga_name: Learn More
      data_ga_location: body
      image: https://images.ctfassets.net/xz1dnu24egyd/7KcXktuJTizzgMwF5o8n66/eddd4708263c41d1afa49399222d8f55/platform-video.png
    button:
      externalUrl: /sales/
      text: Vertrieb kontaktieren
      data_ga_name: sales
      data_ga_location: body
  recognition:
    heading: Branchenführer vertrauen GitLab
    subtitle: GitLab zählt in allen DevOps-Kategorien zu den G2-Leadern.
    badges:
          - src: https://images.ctfassets.net/xz1dnu24egyd/712lPKncEMDkCErGOD91KQ/b3632635c9e3bf13a2d02ada6374029b/DevOpsPlatforms_Leader_Leader.png
            alt: G2 - Winter 2024 - Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/17EE9DHTQpLyGbJAQslSEk/e04bb1e6e71d895af94d10ffb85471e3/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 - Winter 2024 - Easiest to Use
          - src: https://images.ctfassets.net/xz1dnu24egyd/1SNrMzYNTrOvEnOuNucAMD/be22fc71855a96d9c05627c7085148a9/users-love-us.png
            alt: G2 - Winter 2024 - Users love us
          - src: https://images.ctfassets.net/xz1dnu24egyd/3UhXS8fSwKieddbliKZEGn/9bd6e04223769aa0869470befd78dc62/ValueStreamManagement_BestUsability_Total.png
            alt: G2 - Winter 2024 - Best Usability
          - src: https://images.ctfassets.net/xz1dnu24egyd/7AODDnwkskODEsylwXkBys/908eb94a40d8ab2f64bdcacbd18f35a3/DevOpsPlatforms_Leader_Europe_Leader.png
            alt: G2 - Winter 2024 - Leader Europe
          - src: https://images.ctfassets.net/xz1dnu24egyd/4xk6CmKqUndyuir0m4rF5b/b069fcc665b3b11c2a017fb36aa55130/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 - Winter 2024 - Leader Enterprise
          - src: https://images.ctfassets.net/xz1dnu24egyd/7jG9DylTxCnElENurGpocF/4241ca192ba6ec8c1d73d07c5a065d76/DevOpsPlatforms_MomentumLeader_Leader.png
            alt: G2 - Winter 2024 - Momentum Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/I7XhKHEf8rB7o1zYwYyBf/0b62ae05ecb749e83cd453de13972482/DevOps_Leader_Americas_Leader.png
            alt: G2 - Winter 2024 - Leader Americas
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartner logo
        text: 'GitLab ist ein Leader im Gartner® Magic Quadrant™ für DevOps-Plattformen 2024'
        link:
          text: Zum Bericht
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forrester logo
        text: "GitLab ist der einzige Marktführer in The Forrester Wave™: Integrierte Softwarebereitstellungsplattformen, Q2\_2023"
        link:
          text: Zum Bericht
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
  pricing:
    header: Finde heraus, welcher Tarif für dein wachsendes Team am besten geeignet ist
    ctas:
      - button:
          externalUrl: /pricing/premium/
          text: Warum GitLab Premium?
          data_ga_name: why gitlab premium
          data_ga_location: body
      - button:
          externalUrl: /pricing/ultimate/
          text: Warum GitLab Ultimate?
          data_ga_name: why gitlab ultimate
          data_ga_location: body