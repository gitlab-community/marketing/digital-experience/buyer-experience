title: Navigating AI maturity in DevSecOps
og_title: Navigating AI maturity in DevSecOps
description: >-
  AI adoption is ramping up — but our research suggests that most organizations are still evaluating how to incorporate AI into the software development lifecycle. Whether you're an early adopter or you're still exploring, here's a look at what might be ahead on your AI journey.
twitter_description: >-
  AI adoption is ramping up — but our research suggests that most organizations are still evaluating how to incorporate AI into the software development lifecycle. Whether you're an early adopter or you're still exploring, here's a look at what might be ahead on your AI journey.
og_description: >-
  AI adoption is ramping up — but our research suggests that most organizations are still evaluating how to incorporate AI into the software development lifecycle. Whether you're an early adopter or you're still exploring, here's a look at what might be ahead on your AI journey.
og_image: /nuxt-images/developer-survey/2024/2024-devops-survey-ai-meta.jpg
twitter_image: /nuxt-images/developer-survey/2024/2024-devops-survey-ai-meta.jpg
form:
    tag: Free access
    header: Register to read the full report
    text: |
      Get free access to the full report with insights from over 5,000 DevSecOps professionals in 39 countries worldwide.

      **What's in the report**

      - The 4 stages of AI maturity in DevSecOps
      - The AI hype is over — now’s the time for thoughtful adoption
      - AI adoption is a matter of when, not if
      - Organizations are implementing AI across the software development lifecycle
      - Security and skills are barriers to deeper AI adoption
      - Understanding AI impact: The next frontier

    formId: 1002 # Localized versions use a separate form ID
    formDataLayer: resources
    submitted:
      tag: Submission completed
      text: The 2024 Global DevSecOps Report on AI will be sent to your inbox shortly.
      img_src: '/nuxt-images/developer-survey/2024/ai-report-img.png'
hero:
  year: 2024 Global DevSecOps Report
  title: Navigating AI maturity in DevSecOps
  img_src: '/nuxt-images/developer-survey/2024/ai-report-img.png'
  mobile_img_src: '/nuxt-images/developer-survey/2024/ai-mobile-hero-img.svg'
intro:
  survey_header:
    text: >-
      AI adoption is ramping up — but our research suggests that most organizations are still evaluating how to incorporate AI into the software development lifecycle. Whether you're an early adopter or you're still exploring, here's a look at what might be ahead on your AI journey.
    img_variant: 1
  summary:
    title: Report summary
    text: |
      This report analyzes the results of a survey conducted by Omdia and GitLab in which we asked over 5,000 software development, security, and operations professionals worldwide about their organization's position on and adoption of DevSecOps principles and practices.

      This year's survey revealed that while artificial intelligence (AI) is becoming status quo for software development, organizations are at various stages of maturity when it comes to adopting AI in the software development lifecycle. Most organizations have moved beyond the hype and have begun to explore how they might be able to use AI for specific use cases, and the earliest adopters have begun to optimize their approach and demonstrate the impact of AI on their software development processes.

  highlights:
    title: Highlights
    type: barGraph
    buttons:
      previous: Previous
      next: Next
    gaLocation: summary
    blocks:
      - text: >-
          <div class="stat orange"><span class="number">60</span>%</div> of respondents said that it is essential for them to implement AI in their software development processes in order to avoid being left behind
        dataGaName: ai is essential
      - text: >-
          <div class="stat teal"><span class="number">59</span>%</div> of respondents said their organization is prepared to adopt AI
        dataGaName: ai preparedness
      - text: >-
          <div class="stat red"><span class="number">39</span>%</div> of respondents said they are using AI for software development today, up 16 percentage points from 2023
        dataGaName: ai usage
      - text: >-
          <div class="stat purple"><span class="number">55</span>%</div> of respondents said that introducing AI into the software development lifecycle is risky
        dataGaName: ai risk


