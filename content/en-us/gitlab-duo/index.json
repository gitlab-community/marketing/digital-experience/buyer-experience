{
  "metadata": {
    "title": "GitLab Duo",
    "description": "The suite of AI capabilities powering your workflows",
    "og:url": "https://about.gitlab.com/gitlab-duo/"
  },
  "heroCentered": {
    "button": {
      "href": "/solutions/gitlab-duo-pro/sales/",
      "text": "Get Started",
      "dataGaName": "get started",
      "dataGaLocation": "hero",
      "variant": "primary"
    },
    "description": "Ship more secure software faster with AI throughout the entire software development lifecycle",
    "banner": {
        "href": "/solutions/gitlab-duo-pro/sales/",
        "show": true,
        "text": "GitLab Duo Enterprise is now available. Ship more secure software faster with AI throughout the entire SDLC.",
        "data_ga_name": "gitlab duo pill",
        "data_ga_location": "hero"
    },
    "video": {
      "video_url":"https://player.vimeo.com/video/945979565?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479",
      "mp4_url":"/nuxt-images/gitlab-duo/hero.mp4",
      "webm_url":"/nuxt-images/gitlab-duo/hero.webm"
    }
  },
  "sidenav": {
    "hyperlinks": [
      {
        "title":"Benefits",
        "href":"#benefits"
      },
      {
        "title":"Features",
        "href":"#features"
      },
      {
        "title":"Pricing",
        "href":"#pricing"
      },
      {
        "title":"FAQs",
        "href":"#faq"
      },
      {
        "title":"Resources",
        "href":"#resources"
      }
    ]
  },
  "why": {
    "header": "Why GitLab Duo?",
    "card": [
      {
        "title": "Accelerate your path to market",
        "iconName":"speed-alt-2",
        "description":"Develop and deploy secure software faster with AI in every phase of the software development lifecycle — from planning and code creation to testing, security, and monitoring."
      },
      {
        "title":"Adopt AI with guardrails",
        "iconName":"ai-vulnerability-resolution",
        "description":"With GitLab Duo, you control which users, projects, and groups can use AI-powered capabilities. Also, your organization’s proprietary code and data aren’t used to train AI models."
      },
      {
        "title":"Improve developer experience",
        "iconName":"values-three",
        "description":"Give your developers a single platform that integrates the best AI model for each use case across the entire workflow, from understanding code to fixing security vulnerabilities."
      },
      {
        "title":"Committed to transparent AI",
        "iconName":"eye",
        "description":"For organizations and teams to trust AI, it must be transparent. GitLab’s [AI Transparency Center](https://about.gitlab.com/ai-transparency-center/){data-ga-name=\"ai transparency center\" data-ga-location=\"body\"} details how we uphold ethics and transparency in our AI-powered features."
      }
    ],
    "highlight": {
      "eyebrow": "What's next",
      "header": "Meet GitLab Duo Workflow",
      "text": "The next generation of AI-driven development. Workflow is an intelligent, always-on agent that autonomously monitors, optimizes, and secures projects, enabling developers to focus on innovation.",
      "video_src": "https://player.vimeo.com/video/967982166",
      "video_thumbnail": "https://images.ctfassets.net/xz1dnu24egyd/1i5ZSm9ITugyFYHjEBsiar/30f35a754636bdf5a7595b6a91586faa/Image.jpg",
      "cta": {
        "text": "Read the blog post",
        "url": "/blog/2024/06/27/meet-gitlab-duo-workflow-the-future-of-ai-driven-development/",
        "dataGaName": "gitlab duo workflow blog post",
        "dataGaLocation": "body"
      }

    }
  },
  "benefits": {
    "fields": {
      "header": "AI across the software development lifecycle",
      "description": "From planning and coding to securing and deploying, Duo is the only AI solution that supports developers at every stage of their workflow.",
      "card": [
      {
        "title": "AI that is privacy-first",
        "iconName": "ai-vulnerability-resolution",
        "description": "With GitLab Duo, you control which users, projects, and groups can use AI-powered capabilities. Also, your organization’s proprietary code and data aren’t used to train AI models."
      },
      {
        "title": "Improved developer experience",
        "iconName": "values-three",
        "description": "Give your developers a single platform that integrates the best AI model for each use case across the entire workflow, from understanding code to fixing security vulnerabilities."
      },
      {
        "title": "Committed to transparent AI",
        "iconName": "eye",
        "description": "For organizations and teams to trust AI, it must be transparent. GitLab’s [AI Transparency Center](/ai-transparency-center/){data-ga-name=\"ai transparency center\" data-ga-location=\"body\"} details how we uphold ethics and transparency in our AI-powered features. "
      }
    ]
    }
  },
  "features" : {
    "sections": [
      {
        "video" : {
          "title":"Vimeo video 2",
          "internalName":"features-video-1",
          "thumbnail":"/nuxt-images/gitlab-duo/code-suggestions.png",
          "mp4_url":"/nuxt-images/gitlab-duo/code-suggestions.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/code-suggestions.webm"
        },
        "header": "Boost productivity with smart code assistance",
        "text": "Write secure code faster with AI-powered suggestions in over 20 languages, available in your favorite IDE. Automate routine tasks and accelerate development cycles.",
        "pill": "Code",
        "icon": "ai-code-suggestions",
        "left": true
      },
      {
        "video": {
          "title":"Vimeo video 2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/chat.png",
          "mp4_url":"/nuxt-images/gitlab-duo/chat.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/chat.webm"
        },
        "header": "Your AI companion throughout development",
        "text": "Get real-time guidance across the entire software development lifecycle. Generate tests, explain code, refactor efficiently, and chat directly in your IDE or web interface.",
        "pill": "Search",
        "icon": "ai-gitlab-chat",
        "left": false
      },
      {
        "video": {
          "title":"Vimeo video 2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/root-cause-analysis.png",
          "mp4_url":"/nuxt-images/gitlab-duo/root-cause-analysis.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/root-cause-analysis.webm"
        },
        "header": "Quickly resolve CI/CD pipeline issues",
        "text": "Save time troubleshooting with AI-assisted root cause analysis for CI/CD job failures. Get suggested fixes and focus on critical tasks.",
        "pill": "Troubleshoot",
        "icon": "ai-root-cause-analysis",
        "left": true
      },
      {
        "video": {
          "title":"Vimeo video 2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/vulnerability.png",
          "mp4_url":"/nuxt-images/gitlab-duo/vulnerability.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/vulnerability.webm"
        },
        "header": "Fortify your code with AI-powered security",
        "text": "Understand and remediate vulnerabilities more efficiently. Get detailed explanations and auto-generated merge requests to mitigate security risks.",
        "pill": "Secure",
        "icon": "ai-vulnerability-bug",
        "left": false
      },
      {
        "video": {
          "title":"Vimeo video 2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/ai.png",
          "mp4_url":"/nuxt-images/gitlab-duo/ai.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/ai.webm"
        },
        "header": "Measure the ROI of your AI investment",
        "text": "Track AI effectiveness in real-time. See concrete improvements in cycle times and deployment frequencies, quantifying your return on investment.",
        "pill": "Measure",
        "icon": "ai-value-stream-forecast",
        "left": true
      }
    ]
  },
  "promo":{
    "header": "GitLab named a Leader in the 2024 Gartner® Magic Quadrant™ for AI Code Assistants",
    "button": {
      "href": "/gartner-mq-ai-code-assistants/",
      "text": "Read the report",
      "dataGaName": "Read the report",
      "dataGaLocation": "body"
    }
  },
  "categories": {
    "header": "AI-powered features across the software development lifecycle",
    "section": [
      {
        "name":"For Developing Features",
        "data": "developing",
        "icon":"package-alt-2"
      },
      {
        "name":"For Securing Applications",
        "data": "securing",
        "icon":"secure-alt-2"
      },
      {
        "name":"For Facilitating Collaboration",
        "data": "facilitating",
        "icon":"plan-alt-2"
      },
      {
        "name":"For Advanced Troubleshooting",
        "data": "troubleshooting",
        "icon":"monitor-alt-2"
      }
    ],
    "card": [
      {
        "icon":"ai-gitlab-chat",
        "header":"Chat",
        "description":"Processes and generates text and code in a conversational manner. Helps you quickly identify useful information in large volumes of text in issues, epics, code, and GitLab documentation.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo_chat/index.html",
        "category":"developing",
        "text": "Read more"
      },
      {
        "icon":"ai-root-cause-analysis",
        "header":"Code Explanation",
        "description":"Helps you understand code by explaining it in natural language.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/index.html#code-explanation",
        "category":"developing",
        "text": "Read more"
      },
      {
        "icon":"ai-git-suggestions",
        "header":"Code Suggestions",
        "description":"Helps developers write secure code more efficiently and accelerate cycle times by taking care of repetitive, routine coding tasks.",
        "url":"https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html",
        "category":"developing",
        "text": "Read more"
      },
      {
        "icon":"ai-git-suggestions",
        "header":"GitLab Duo for the CLI",
        "description":"Discover or recall Git commands when and where you need them.",
        "url":"https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/index.html#gitlab-duo-commands",
        "category":"developing",
        "text": "Read more"
      },
      {
        "icon":"ai-tests-in-mr",
        "header":"Test Generation",
        "description":"Automates repetitive tasks and helps catch bugs early.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/#test-generation",
        "category":"developing",
        "text": "Read more"
      },
      {
        "icon":"ai-vulnerability-bug",
        "header":"Vulnerability Explanation",
        "description":"Helps you remediate vulnerabilities more efficiently, boost your skills, and write more secure code.",
        "url":"https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#explaining-a-vulnerability",
        "category":"securing",
        "text": "Read more"
      },
      {
        "icon":"ai-vulnerability-resolution",
        "header":"Vulnerability Resolution",
        "description":"Generates a merge request containing the changes required to mitigate a vulnerability.",
        "url":"https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#vulnerability-resolution",
        "category":"securing",
        "text": "Read more"
      },
      {
        "icon":"ai-value-stream-forecast",
        "header":"AI Impact Dashboard",
        "description":"See real-time improvements in cycle times and deployment frequencies",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/#ai-impact-dashboard",
        "category":"facilitating",
        "text": "Read more"
      },
      {
        "icon":"ai-issue",
        "header":"Discussion Summary",
        "description":"Assists with getting everyone up to speed on lengthy conversations to help ensure you are all on the same page.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary",
        "category":"facilitating",
        "text": "Read more"
      },
      {
        "icon":"ai-summarize-mr-review",
        "header":"Code Review Summary",
        "description":"Helps merge request handoff between authors and reviewers and helps reviewers efficiently understand suggestions.",
        "url":"https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html#summarize-a-code-review",
        "category":"facilitating",
        "text": "Read more"
      },
      {
        "icon":"ai-merge-request",
        "header":"Merge Request Summary",
        "description":"Efficiently communicates the impact of your merge request changes.",
        "url":"https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html",
        "category":"facilitating",
        "text": "Read more"
      },
      {
        "icon":"ai-generate-issue-description",
        "header":"Issue Description Generation",
        "description":"Generates issue descriptions.",
        "url":"https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html#summarize-a-code-review",
        "category":"facilitating",
        "text": "Read more"
      },
      {
        "icon":"ai-root-cause-analysis",
        "header":"Root Cause Analysis",
        "description":"Assists you in determining the root cause for a pipeline failure and failed CI/CD build.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/index.html#root-cause-analysis",
        "category":"troubleshooting",
        "text": "Read more"
      }
    ],
    "disclaimer": "An Ultimate license will allow for testing certain capabilities listed as Experiments or in Beta subject to the [GitLab Testing Agreement](https://handbook.gitlab.com/handbook/legal/testing-agreement/). Once an AI feature moves from Beta to General Availability, customers with either a Premium or Ultimate license may continue to use the GitLab Duo capabilities by purchasing the GitLab Duo Pro or GitLab Duo Enterprise add-on."
  },
  "faq": {
    "header":"Frequently Asked Questions",
    "texts":{
      "show":"Show All",
      "hide":"Hide All"
    },
    "questions": [
      {
        "question":"What programming languages are supported in Code Suggestions?",
        "answer":"The best results from Code Suggestions are expected for languages the [Google Vertex AI Codey APIs](https://cloud.google.com/vertex-ai/generative-ai/docs/code/code-models-overview#supported_coding_languages) directly support: C++, C#, Go, Google SQL, Java, JavaScript, Kotlin, PHP, Python, Ruby, Rust, Scala, Swift, and TypeScript. "
      },
      {
        "question":"What language models does GitLab Duo use?",
        "answer":"Our abstraction layer allows us to power AI capabilities with the suitable model for the right use case. Explore the language models used for each GitLab Duo feature [here.](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html)"
      },
      {
        "question":"Will my code be used for training AI models?",
        "answer":"GitLab does not train generative AI models based on private (non-public) data. The vendors we work with also do not train models based on private data. [Learn more here](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html#training-data){data-ga-name=\"learn more about ai training models\" data-ga-location=\"faq\"}."
      },
      {
        "question":"Is GitLab Duo open core?",
        "answer":"Yes. GitLab has an open core business model that enables us to build with our customers, who can contribute new capabilities to our product."
      },
      {
        "question":"How can I use outputs generated by GitLab Duo?",
        "answer":"Output generated by GitLab Duo can be used at your discretion and, if a third-party claim arises from your use of the output generated by GitLab Duo, GitLab will step in and defend you."
      },
      {
        "question":"How do customers control which users can access GitLab Duo Pro features?",
        "answer":"Organizational Controls allows Admins to assign license seats to specific users via a new admin UI. Code Suggestions can be enabled or disabled by the user directly in the IDE extension settings. Experimental/Beta AI Features such as Chat are controlled with a Top-level group setting. [Learn more here](https://docs.gitlab.com/ee/user/gitlab_duo/index.html#experimental-features){data-ga-name=\"learn more about experiemental ai features\" data-ga-location=\"faq\"}."
      },
      {
        "question":"Is GitLab Duo Pro available on non-connected/limited connectivity GitLab instances?",
        "answer":"No, Code Suggestions and Chat require Internet connectivity and Cloud Licensing for self-managed customers to access."
      },
      {
        "question":"When providing the prompt in a comment (in the source file), can the prompt be in a language other than English?",
        "answer":"We don't intentionally restrict natural language, however we do expect English will provide the best results. From experimentation, writing the prompt in a language other than English provides a code suggestion tailored to that language (respecting the specific language syntax and reserved keywords)."
      },
      {
        "question":"How is Code Suggestions ensuring IP protection and Data privacy?",
        "answer":"Read all about Code Suggestions Data privacy and IP protection [here](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html#data-privacy){data-ga-name=\"data privacy and ip protection\" data-ga-location=\"faq\"}. Usage of Code Suggestions and Chat is governed by the [GitLab Testing Agreement](https://about.gitlab.com/handbook/legal/testing-agreement/){data-ga-name=\"gitlab testing agreement\" data-ga-location=\"faq\"} while it is still free to use and while Chat is still in Beta. Learn about [data usage when using Code Suggestions here](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html){data-ga-name=\"coe suggestions data usage\" data-ga-location=\"faq\"} and [Chat data usage here.](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html){data-ga-name=\"chat data usage\" data-ga-location=\"faq\"}"
      }
    ]
  },
  "pricing": {
    "header":"Pricing"
  },
  "resources": {
    "id":"resources",
    "align":"left",
    "grouped": "true",
    "column_size": 4,
    "title":"Learn more about GitLab Duo",
    "header_cta_text":"View all resources",
    "header_cta_href":"/resources/",
    "header_cta_ga_name":"View all resources",
    "header_cta_ga_location":"body",
    "cards":[
      {
        "event_type":"Video",
        "header":"Meet GitLab Duo",
        "link_text":"Watch now",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/wS2NkIeyOYrkiqadM1Jkd/eeabed3c2d08fa50047c70989d70b14d/Meet_GitLab_Duo_Thumbnail.png",
        "href":"https://player.vimeo.com/video/855805049?title=0&byline=0&portrait=0&badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"meet gitlab duo"
      },
      {
        "event_type":"Video",
        "header":"Code Suggestions",
        "link_text":"Watch now",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/7paG5yX4I2FErEKb3022EN/cd480897c76cd1c5dd2a51426c8e0013/Code_Suggestions_Thumbnail.png",
        "href":"https://player.vimeo.com/video/894621401?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479",
        "data_ga_name":"code suggestions"
      },
      {
        "event_type":"Video",
        "header":"Chat",
        "link_text":"Watch now",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/4y29zj5spjIUh2DaGdxk5q/f7d972269fd3b16501ef4d3b013f6c09/Chat_Thumbnail.png",
        "href":"https://player.vimeo.com/video/927753737?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"chat"
      },
      {
        "event_type":"Video",
        "header":"Code Review Summary",
        "link_text":"Watch now",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/kfMhe63t5l6jHIxmmlq5z/a68a12a38b7faf7b37c821c5e3204ec1/Code_Review_Summary_Thumbnail.png",
        "href":"https://player.vimeo.com/video/929891003?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"code review summary"
      },
      {
        "event_type":"Video",
        "header":"Discussion Summary",
        "link_text":"Watch now",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/4gvRwDXZ3zDwKKtCHiYhy6/c26a91ddfa12817c2bcee4e6a991260f/Discussion_Summary_Thumbnail.jpg",
        "href":"https://player.vimeo.com/video/928501915?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"discussion summary"
      },
      {
        "event_type":"Video",
        "header":"Vulnerability Explanation",
        "link_text":"Watch now",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/64mYwKfirCjkHiapVtZlN4/830c78fa1902ed0a61ffdc9da822145c/Vulnerability_Explanation_Thumbnail.jpg",
        "href":"https://player.vimeo.com/video/930066123?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"vulernability explanation"
      },
      {
        "event_type":"Video",
        "header":"Code Explanation",
        "link_text":"Watch now",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/1N6clW3Mq7Fu40uIEt2iav/6b487d1916276934b3641a456211835c/Code_Explanation_Thumbnail.jpg",
        "href":"https://player.vimeo.com/video/930066090?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"code explanation"
      },
      {
        "event_type":"Video",
        "header":"Suggested Reviewers",
        "link_text":"Watch now",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/5L9AwAebsBO8eIQxSH96nz/a388c454b847bc134082a352bea32082/Suggested_Reviewers_Thumbnail.png",
        "href":"https://player.vimeo.com/video/930066108?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"suggested reviewers"
      },
      {
        "icon":{
          "name":"blog",
          "variant":"marketing"
        },
        "event_type":"Blog",
        "header":"Understand and resolve vulnerabilities with AI-powered GitLab Duo",
        "link_text":"Read more",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/1bIUeH2qhLxfgPJIZbBzD9/c916adff20eab529510a0e4eea5d9f4d/GitLab-Duo-Blog-Image-1.png",
        "href":"/blog/2024/02/21/understand-and-resolve-vulnerabilities-with-ai-powered-gitlab-duo/",
        "data_ga_name":"Understand and resolve vulnerabilities with AI-powered GitLab Duo"
      },
      {
        "icon":{
          "name":"blog",
          "variant":"marketing"
        },
        "event_type":"Blog",
        "header":"Measuring AI effectiveness beyond developer productivity metrics",
        "link_text":"Read more",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/2IjSEZ7qEEcaDf2PlvJKRp/bb97ac8129ca9404b74ea0c13b5ed437/GitLab-Duo-Blog-Image-2.png",
        "href":"/blog/2024/02/20/measuring-ai-effectiveness-beyond-developer-productivity-metrics/",
        "data_ga_name":"Measuring AI effectiveness beyond developer productivity metrics"
      },
      {
        "icon":{
          "name":"blog",
          "variant":"marketing"
        },
        "event_type":"Blog",
        "header":"New report on AI-assisted tools points to rising stakes for DevSecOps",
        "link_text":"Read more",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/bH5lKxYxiDkZ4LULySnmO/ab7437e32e77d6b2f39bd70184edf913/GitLab-Duo-Blog-Image-3.png",
        "href":"/blog/2024/02/14/new-report-on-ai-assisted-tools-points-to-rising-stakes-for-devsecops/",
        "data_ga_name":"New report on AI-assisted tools points to rising stakes for DevSecOps"
      }
    ]
  }
}
