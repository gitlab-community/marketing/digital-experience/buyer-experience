---
  title: 'GitLab 17 release event: The future of AI-driven software development'
  description: Join us to explore the new AI-powered features in GitLab 17 that will help you improve collaboration, visibility, security, and cycle times. Register now!
  og_title: 'GitLab 17 release event: The future of AI-driven software development'
  twitter_description: "Join us to explore the new AI-powered features in GitLab 17 that will help you improve collaboration, visibility, security, and cycle times. Register now!"
  og_description: "Join us to explore the new AI-powered features in GitLab 17 that will help you improve collaboration, visibility, security, and cycle times. Register now!"
  og_image: /nuxt-images/seventeen/gl17-open-graph.png
  twitter_image: /nuxt-images/seventeen/gl17-open-graph.png
  hero_image_block:
    seventeen: true # Config boolean value for 'seventeen' page component variations
    title:
      note: GitLab 17 release event
      text: The future of AI-driven software development
    subtitle: Attend our virtual launch event and see what your organization can do with AI throughout the SDLC.
    background: '/nuxt-images/seventeen/gitlab17hero.svg'
    image:
      url: /nuxt-images/seventeen/17.svg
      alt: GitLab 17    
    background_mobile: '/nuxt-images/seventeen/gl17-hero-mobile.svg'
    action:
      url: "#register"
      text: Sign up
      data_ga_name: sign up
      data_ga_location: header
      variant: secondary
      icon:
        name: arrow-down
        variant: product
  register_form_block:
    form:
      formId: 3660
      datalayer: events
      form_header: Register
      all_required: false
      submitted_message:
        body: You will receive a confirmation email shortly
      header_config:
        text_align: left
        text_variant: heading4-bold
        text_tag: h4
    content_aos_animation: zoom-in-right
    content_aos_duration: 800
    form_aos_animation: zoom-in-left
    form_aos_duration: 800
    metadata:
      id_tag: register
    header: |
      Date: Thursday, June 27, 2024 <br />

      Time: 8:30 am PT / 11:30 am EST / 3:30 pm UTC

      Duration: 1 hour
    text_header: 'heading5-bold'
    text_body: 'body2'
    without_bg: true
    content: |
      Today AI can — and should — do much more than assist in code generation. AI should ___transform___ your workflow and make your entire software development lifecycle smarter, faster, and more secure. And with GitLab 17 and GitLab Duo, it does.
      
      Join us for our annual virtual release event to see how your organization can improve collaboration, visibility, security, and cycle times with one platform powered by AI.
      
      You’ll get a front row seat for:
      - Demos of AI throughout the software lifecycle — from identifying and fixing vulnerabilities to troubleshooting CI/CD pipelines
      - What you can expect the future to hold as AI in software development evolves
      - How your organization can adopt AI while maintaining control and data privacy
      - A sneak peek of what our team’s working on for future releases
    gitlabSeventeenRelease:
      title: Speakers
      cards:
        - image: '/nuxt-images/seventeen/david-desanto.png'
          name: 'David DeSanto'
          role: 'Chief Product Officer'
        - image: '/nuxt-images/seventeen/ashley-kramer.png'
          name: 'Ashley Kramer'
          role: 'Chief Marketing and Strategy Officer'
        - image: '/nuxt-images/seventeen/hillary-benson.jpeg'
          name: 'Hillary Benson'
          role: 'Senior Director, Product Management'

