---
  title: Calculadora de retorno de la inversión
  description: Aquí encontrará información sobre la calculadora del ROI de las categorías de GitLab. Vea más aquí y conozca el valor.
  heading: ¿Cuánto le cuesta su cadena de herramientas?
  tabData:
    - tabName: Ahorre dinero
      tabId: cost
    - tabName: Ahorre tiempo
      tabId: time
  cost_steps: 
    - Su personal
    - Gasto actual
    - Sus ahorros
  time_steps:
    - Su personal
    - Hora actual
    - Ahorro de tiempo
  cost_roi:
    people: ¿Cuántas personas utilizan y mantienen su cadena de herramientas?
    users: Cantidad de usuarios
    maintainers: Cantidad de encargados de mantenimiento de las herramientas de DevSecOps
    nextStep: Continuar con el paso siguiente
    spendPerYear: Aproximadamente, ¿cuánto gasta por año (en USD) en estas capacidades?
    sourceCodeManagement: Gestión del código fuente
    agileProjectManagement: Gestión de proyectos Agile
    continuousIntegration: Integración continua
    apm: Gestión de portafolios Agile
    continuousDelivery: Entrega continua (CD)
    applicationSecurity: Seguridad de la aplicación
    registries: Registros
    savings: Consultar sus ahorros
    restart: Reiniciar calculadora
  time_roi:
    people: ¿Cuántas personas utilizan y mantienen su cadena de herramientas?
    users: Cantidad de usuarios
    maintainers: Cantidad de encargados de mantenimiento de las herramientas de DevSecOps
    nextStep: Continuar con el paso siguiente
    howManyDaysToProduction: ¿Cuántos días tarda una actualización de la aplicación (confirmación de código) en llegar a producción?
    daysToProduction: Días para la producción
    hoursPerWeek: Horas por semana
    developmentConfigurationHours : ¿Cuántas horas por semana dedican los desarrolladores a configurar/integrar las diferentes herramientas en su cadena de herramientas de DevOps?
    developmentSecurityHours: ¿Cuántas horas por semana dedican los desarrolladores a resolver las vulnerabilidades de seguridad antes de la producción?
    savings: Consultar sus ahorros
    restart: Reiniciar calculadora
  tooltips:
    numberOfUsers: ¿Cuántos empleados utilizan su conjunto de herramientas actual?
    numberOfMaintainers: |
      ¿Cuántos empleados instalan, integran y gestionan su conjunto de herramientas? Se asume un salario promedio anual de USD $ 97 000 fuente: <a href="https://www.glassdoor.co.in/Salaries/us-software-developer-salary-SRCH_IL.0,2_IN1_KO3,21.htm">glassdoor</a>
    sourceCodeManagement: Gasto anual (en USD) para herramientas de gestión del código fuente, incluido el control de versiones de activos, la ramificación y las revisiones de código. (p. ej., herramientas como GitHub, BitBucket)
    continuousIntegration: Gasto anual (en USD) para herramientas de integración continua que incluyen compilación, prueba y validación. (p. ej., herramientas como Jenkins, CircleCI)
    continuousDelivery: Gasto anual (en USD) para herramientas de entrega continua utilizadas para implementaciones seguras en entornos de producción/etapa de staging. (p. ej., herramientas como Arnés, ArgoCD)
    applicationSecurity: Gasto anual (en USD) para herramientas de seguridad de aplicaciones que incluyen, entre otras, pruebas estáticas y dinámicas. (p. ej., herramientas como Snyk, Veracode o Sonarcube)
    agileProjectManagement: Gasto anual (en USD) para herramientas ágiles de gestión de proyectos utilizadas para la planificación dentro de un equipo/proyecto (p. ej., herramientas como Jira, Trello o Asana)
    agilePortfolioManagement: Gasto anual (en USD) para herramientas ágiles de gestión de portafolios utilizadas para la planificación en múltiples equipos/proyectos. (p. ej., herramientas como Planview Enterprise One, Jira Align)
    registries: Gasto anual (en USD) para registros de contenedores y paquetes. (p. ej., herramientas como Artifactory, Docker Hub)
  currentSpendTooltip: El gasto actual incluye los costos de las herramientas y los encargados de mantenimiento
  cost_results:
    heading: Actualmente, su cadena de herramientas le está costando
    monetary_sign: $
    premium_description: GitLab Premium puede ser una excelente opción para que su empresa mejore la productividad y la colaboración del equipo.
    disclaimer: Los resultados se basan en ahorros informados por organizaciones de tamaño similar. Los resultados son solo una estimación y están sujetos a cambios en función de los diversos factores contemplados en el cálculo.
    youCouldSave: Podría ahorrar
    ultimateSwitch: por año al cambiar a GitLab Ultimate.
    ultimate_description: GitLab Ultimate es la mejor opción para que su empresa alcance la seguridad, el cumplimiento y la planificación en toda la organización.
    current_spend: Su gasto actual
    vs: frente a
    gitlab_ultimate: GitLab Ultimate
    free_trial: Obtener prueba gratuita
    buy_ultimate: Comprar Ultimate ahora
    results_disclaimer: Los resultados son solo una estimación y están sujetos a cambios en función de los diversos factores contemplados en el cálculo.
    premiumSwitch: por año al cambiar a GitLab Premium.
    gitlab_premium: GitLab Premium
    buy_premium: Comprar Premium ahora
  time_results: 
    toolchain_cost: Hoy dedica
    hoursPerProject: horas por proyecto
    hours: horas
    save: Guardar
    switching: Al cambiar a
    efficiency: Aumentar la eficiencia mediante
    premium: GitLab Premium
    ultimate: GitLab Ultimate
    results: Los resultados se basan en ahorros informados por organizaciones de tamaño similar. Los resultados son solo una estimación y están sujetos a cambios en función de los diversos factores contemplados en el cálculo.
    tooltip: Calculado según el estudio Forrester Consulting Total Economic Impact™ encargado por GitLab en 2022.
    ultimateButton: 
      text: Más información sobre Ultimate
      href: /pricing/ultimate/
      variant: primary
      data_ga_name: ultimate learn more
      data_ga_location: calculator body with time calculator selected
    premiumButton: 
      text: Más información sobre Premium
      href: /pricing/premium/
      variant: primary
      data_ga_name: premium learn more
      data_ga_location: calculator body with time calculator selected
    salesButton: 
      text: Comuníquese con Ventas
      href: /sales/
      variant: tertiary
      data_ga_name: sales
      data_ga_location: calculator body with time calculator selected
  marketoForm:
    contactUs: Comuníquese con nosotros para un análisis más profundo de sus ahorros
    requiredFields: Todos los campos son obligatorios
    submissionReceived: ¡Solicitud recibida!
  premium_features:
    header: 'GitLab Premium incluye:'
    button:
      text: Más información sobre Premium
      url: '/pricing/premium/'
      data_ga_location: premium roi results
      data_ga_name: learn more about premium
      listHeading: "GitLab Premium incluye:"
      freeTrial: Obtener prueba gratuita
      learnMore: Más información sobre Premium
    list:
      - text: Las revisiones de código más rápidas
        information:
          - text: Múltiples aprobadores en la revisión de código
          - text: Propietarios del código GitLab
          - text: Análisis de revisión de código
          - text: Revisiones de solicitudes de fusión
          - text: Informes de calidad del código
          - text: Pipelines de resultados fusionados
          - text: Comentarios en Review Apps
      - text: CI/CD avanzadas
        information:
          - text: Panel de pipelines de CI/CD
          - text: Trenes de fusión
          - text: Gráficos de pipeline de varios proyectos
          - text: CI/CD para repositorio externo
          - text: Gestión de la implementación de GitOps
          - text: Panel de entornos
          - text: Plantillas de archivos de grupo
          - text: Paquete de reversiones e implementación robusto
      - text: Entrega ágil en empresas
        information:
          - text: Planes de desarrollo
          - text: Múltiples personas asignadas
          - text: Épicas de un solo nivel
          - text: Listas de hitos del panel de tickets
          - text: Etiquetas con alcance
          - text: Promover el ticket a épica
          - text: Panel de tickets múltiples de grupo
          - text: Dependencias de tickets
      - text: Controles de lanzamientos
        information:
          - text: Reglas de aprobación para la revisión de código
          - text: Aprobaciones de solicitudes de fusión requeridas
          - text: Dependencias de solicitudes de fusión
          - text: Reglas de push
          - text: Restringir el acceso push y fusionar
          - text: Entornos protegidos
          - text: Bloquear la membresía del proyecto al grupo
          - text: DNS con reconocimiento de geolocalización
      - text: Confiabilidad autogestionada
        information:
          - text: Recuperación ante desastres
          - text: Modo de mantenimiento
          - text: Clonación distribuida con GitLab Geo
          - text: Almacenamiento Git tolerante a fallos con Gitaly
          - text: Soporte para arquitecturas a escala
          - text: Replicación geográfica del registro de contenedores
          - text: Equilibrio de carga de base de datos para PostgreSQL
          - text: Reenvío de registros
      - text: 10 000 minutos de cálculo por mes
      - text: Soporte
  ultimate_features:
    header: 'GitLab Ultimate incluye:'
    button:
      text: Más información sobre Ultimate
      url: '/pricing/ultimate/'
      data_ga_location: premium roi results
      data_ga_name: learn more about ultimate
    list:
      - text: Pruebas de seguridad avanzadas
        information:
          - text: Pruebas dinámicas de seguridad de las aplicaciones
          - text: Paneles de seguridad
          - text: Gestión de vulnerabilidades
          - text: Análisis de contenedores
          - text: Análisis de dependencias
          - text: Informes de vulnerabilidades
          - text: Fuzzing de la API
          - text: Base de datos de vulnerabilidades
      - text: Gestión de vulnerabilidades
        information:
          - text: Aprobaciones de seguridad
          - text: Políticas de seguridad
      - text: Pipelines de cumplimiento
        information:
          - text: Informe de eventos de auditoría
          - text: Interfaz para eventos de auditoría
          - text: Cumplimiento de licencia
          - text: Informe de cumplimiento
          - text: Gestión de calidad
          - text: Gestión de requisitos
          - text: Requerir un ticket de Jira antes de fusionar el código
          - text: Marcos de cumplimiento personalizados
      - text: Gestión de portafolios
        information:
          - text: Paneles de las épicas
          - text: Épicas multinivel
          - text: Informes de estado de épicas y tickets
          - text: Jerarquía de planificación
          - text: Planes de desarrollo a nivel de portafolio
          - text: Editar épicas masivas
      - text: Gestión del flujo de valor
        information:
          - text: Insights
          - text: Métrica DORA-4 - Frecuencia de implementación
          - text: Métrica DORA-4 - Plazo de realización de los cambios
      - text: 50 000 minutos de cálculo por mes
      - text: Soporte
      - text: Usuarios de la versión gratuita
  time_card:
    img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
    text: Vea cómo HackerOne logra implementaciones 5 veces más rápidas al cambiar a la seguridad integrada de GitLab
    cta:
      text: Leer el estudio de caso del cliente
      url: /customers/hackerone/
      data_ga_name: HackerOne case study
      data_ga_location: calculator body with time calculator selected
  cards:
  - icon:
      name: money
      alt: Ícono de dinero
      variant: marketing
    title: Más información sobre los planes de precios de GitLab
    link_text: Ir a la página de precios
    link_url: /pricing/
    data_ga_name: learn more about gitlab pricing plans
    data_ga_location: roi calculator
  - icon:
      name: cloud-thin
      alt: Ícono de nube delgada
      variant: marketing
    title: Compre GitLab a través de Cloud Marketplaces
    link_text: Comuníquese con Ventas
    link_url: /sales/
    data_ga_name: purchase gitlab through cloud marketplaces
    data_ga_location: roi calculator