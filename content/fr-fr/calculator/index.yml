---
  title: Calculateur de retour sur investissement
  description: Vous trouverez ici des informations sur le calculateur de retour sur investissement pour les fonctionnalités de GitLab. Cliquez ici pour en savoir plus et en découvrir la valeur ajoutée !
  heading: Combien vous coûte votre chaîne d'outils ?
  tabData:
    - tabName: Économisez de l'argent
      tabId: cost
    - tabName: Gagnez du temps
      tabId: time
  cost_steps: 
    - Vos collaborateurs
    - Dépenses actuelles
    - Vos économies
  time_steps:
    - Vos collaborateurs
    - Temps actuel
    - Gains de temps
  cost_roi:
    people: Combien de personnes utilisent et tiennent à jour votre chaîne d'outils ?
    users: Nombre d'utilisateurs
    maintainers: Nombre de chargés de maintenance d'outils DevSecOps
    nextStep: Passer à l'étape suivante
    spendPerYear: Quelles sont approximativement vos dépenses annuelles (en USD) pour ces fonctionnalités ?
    sourceCodeManagement: Gestion du code source
    agileProjectManagement: Gestion de projet Agile
    continuousIntegration: Intégration continue
    apm: Gestion de portefeuille Agile
    continuousDelivery: Livraison continue
    applicationSecurity: Sécurité des applications
    registries: Registres
    savings: Découvrez les économies que vous pouvez réaliser
    restart: Redémarrer le Calculateur
  time_roi:
    people: Combien de personnes utilisent et maintiennent votre chaîne d'outils ?
    users: Nombre d'utilisateurs
    maintainers: Nombre de chargés de maintenance d'outils DevSecOps
    nextStep: Passer à l'étape suivante
    howManyDaysToProduction: Combien de jours faut-il pour qu'une mise à jour de l'application (validation du code) passe en production ?
    daysToProduction: Nombre de jours pour arriver en production
    hoursPerWeek: Heures par semaine
    developmentConfigurationHours : Combien d'heures par semaine vos développeurs passent-ils à configurer/intégrer différents outils dans votre chaîne d'outils DevOps ?
    developmentSecurityHours: Combien d'heures par semaine vos développeurs passent-ils à corriger les failles de sécurité avant la mise en production ?
    savings: Découvrez les économies que vous pouvez réaliser
    restart: Redémarrer le Calculateur
  tooltips:
    numberOfUsers: Combien d'employés utilisent vos outils actuels ?
    numberOfMaintainers: |
      Combien d'employés installent, intègrent et gèrent votre ensemble d'outils ? On suppose un salaire annuel moyen de 97 000  USD, source : <a href="https://www.glassdoor.co.in/Salaries/us-software-developer-salary-SRCH_IL.0,2_IN1_KO3,21.htm">Glassdoor</a>
    sourceCodeManagement: Dépenses annuelles (en USD) pour les outils de gestion du code source, y compris le contrôle de version des actifs, la gestion des branches et les revues de code. (Par ex., des outils comme GitHub, BitBucket)
    continuousIntegration: Dépenses annuelles (en USD) pour les outils d'intégration continue, y compris la compilation, les tests et la validation. (Par ex., des outils comme Jenkins, CircleCI)
    continuousDelivery: Dépenses annuelles (en USD) pour les outils de livraison continue utilisés pour des déploiements sûrs dans des environnements de staging (préproduction) ou de production. (Par ex., des outils comme Harness, ArgoCD)
    applicationSecurity: Dépenses annuelles (en USD) pour les outils de sécurité des applications, y compris les tests statiques et dynamiques de sécurité, entre autres. (Par ex., des outils comme Snyk, Veracode ou Sonarcube)
    agileProjectManagement: Dépenses annuelles (en USD) pour des outils de gestion de projets Agile utilisés pour la planification au sein d'une équipe ou d'un projet (par ex., des outils comme Jira, Trello ou Asana)
    agilePortfolioManagement: Dépenses annuelles (en USD) pour les outils de gestion de portefeuille Agile utilisés pour la planification entre plusieurs équipes/projets. (Par ex., des outils comme Planview Enterprise One, Jira Align)
    registries: Dépenses annuelles (en USD) pour les registres de conteneurs et de paquets. (Par ex., des outils comme Artifactory, Docker Hub)
  currentSpendTooltip: Les dépenses actuelles comprennent les coûts des outils et des chargés de maintenance
  cost_results:
    heading: Votre chaîne d'outils vous coûte actuellement
    monetary_sign: $
    premium_description: GitLab Premium peut être un excellent choix pour votre entreprise afin d'améliorer la productivité et la collaboration des équipes.
    disclaimer: Les résultats sont basés sur les économies déclarées par des entreprises de taille similaire. Les résultats ne sont qu'une estimation et sont sujets à une réévaluation en fonction de divers facteurs pris en compte dans le calcul.
    youCouldSave: Vous pourriez économiser
    ultimateSwitch: chaque année en passant à GitLab Ultimate !
    ultimate_description: GitLab Ultimate est le meilleur choix afin d'atteindre le niveau de sécurité, de conformité et de planification que vous désirez à l'échelle de votre entreprise.
    current_spend: Vos dépenses actuelles
    vs: par rapport à
    gitlab_ultimate: GitLab Ultimate
    free_trial: Commencer un essai gratuit
    buy_ultimate: Acheter GitLab Ultimate maintenant
    results_disclaimer: Les résultats ne sont qu'une estimation et sont sujets à une réévaluation en fonction de divers facteurs pris en compte dans le calcul.
    premiumSwitch: chaque année en passant à GitLab Premium !
    gitlab_premium: GitLab Premium
    buy_premium: Acheter GitLab Premium maintenant
  time_results: 
    toolchain_cost: Aujourd'hui, vous dépensez
    hoursPerProject: heures par projet
    hours: heures
    save: Enregistrer
    switching: En passant à
    efficiency: Augmentez la réactivité et le rendement de
    premium: GitLab Premium
    ultimate: GitLab Ultimate
    results: Les résultats sont basés sur les économies déclarées par des entreprises de taille similaire. Les résultats ne sont qu'une estimation et sont sujets à une réévaluation en fonction de divers facteurs pris en compte dans le calcul.
    tooltip: Calculés sur la base de l'étude Forrester Consulting Total Economic Impact™ commandée par GitLab en 2022.
    ultimateButton: 
      text: En savoir plus sur GitLab Ultimate
      href: /pricing/ultimate/
      variant: primary
      data_ga_name: ultimate learn more
      data_ga_location: calculator body with time calculator selected
    premiumButton: 
      text: Découvrir l'édition Premium
      href: /pricing/premium/
      variant: primary
      data_ga_name: premium learn more
      data_ga_location: calculator body with time calculator selected
    salesButton: 
      text: Contacter notre équipe commerciale
      href: /sales/
      variant: tertiary
      data_ga_name: sales
      data_ga_location: calculator body with time calculator selected
  marketoForm:
    contactUs: Contactez-nous pour en savoir plus sur vos économies potentielles
    requiredFields: Tous les champs sont obligatoires
    submissionReceived: Merci pour votre inscription !
  premium_features:
    header: 'GitLab Premium comprend :'
    button:
      text: En savoir plus sur GitLab Premium
      url: '/pricing/premium/'
      data_ga_location: premium roi results
      data_ga_name: learn more about premium
      listHeading: "GitLab\_Premium comprend\_:"
      freeTrial: Commencer un essai gratuit
      learnMore: En savoir plus sur GitLab Premium
    list:
      - text: Revues de code plus rapides
        information:
          - text: Plusieurs approbateurs lors de la revue de code
          - text: Fonctionnalité Code Owners
          - text: Analyse des revues de code
          - text: Révisions des merge requests
          - text: Rapports Code Quality
          - text: Pipelines de résultats de merge
          - text: Commentaires dans Review Apps
      - text: Pipeline CI/CD avancé
        information:
          - text: Tableau de bord des pipelines CI/CD
          - text: Merge trains
          - text: Graphiques de pipeline multiprojets
          - text: Pipeline CI/CD pour dépôt externe
          - text: Gestion du déploiement GitOps
          - text: Tableau de bord des environnements
          - text: Modèles de fichiers de groupe
          - text: Ensemble d'outils intégrés de déploiement et de retour en arrière robuste
      - text: La livraison Agile intégrée
        information:
          - text: Feuilles de route
          - text: Personnes assignées à plusieurs tickets
          - text: Epic à un seul niveau
          - text: Listes de jalons du tableau des tickets
          - text: Labels à portée limitée
          - text: Évolution du statut de ticket en epic
          - text: Tableaux de tickets multiples de groupe
          - text: Dépendances des tickets
      - text: Contrôles des nouvelles de sortie
        information:
          - text: Règles d'approbation pour la revue de code
          - text: Approbations requises pour les merge requests
          - text: Dépendances de la merge request
          - text: Règles de push
          - text: Restriction de l'accès au push et au merge
          - text: Environnements protégés
          - text: Verrouillage de l'appartenance du projet au groupe
          - text: DNS prenant en charge la géolocalisation
      - text: Fiabilité de la version auto-gérée
        information:
          - text: Reprise après sinistre
          - text: Mode maintenance
          - text: Clonage distribué avec GitLab Geo
          - text: Stockage Git tolérant aux pannes avec Gitaly
          - text: Prise en charge des architectures de déploiment à grande échelle
          - text: Réplication géographique du registre de conteneurs
          - text: Répartition de charge de base de données pour PostgreSQL
          - text: Transfert des journaux
      - text: 10 000 minutes de calcul par mois
      - text: Assistance
  ultimate_features:
    header: 'GitLab Ultimate comprend :'
    button:
      text: En savoir plus sur Ultimate
      url: '/pricing/ultimate/'
      data_ga_location: premium roi results
      data_ga_name: learn more about ultimate
    list:
      - text: Tests de sécurité avancés
        information:
          - text: Test dynamique de sécurité des applications (DAST)
          - text: Tableaux de bord relatif à la sécurité
          - text: Gestion des vulnérabilités
          - text: Analyse des conteneurs
          - text: Analyse des dépendances
          - text: Rapports sur les vulnérabilités
          - text: Test de l'API par injection de données aléatoires
          - text: Base de données des vulnérabilités
      - text: Gestion des vulnérabilités
        information:
          - text: Approbations de sécurité
          - text: Stratégies de sécurité
      - text: Pipelines de conformité
        information:
          - text: Rapport d'événements d'audit
          - text: Interface pour les événements d'audit
          - text: Conformité des licences logicielles
          - text: Rapport de conformité
          - text: Gestion de la qualité
          - text: Gestion des exigences
          - text: Ticket Jira obligatoire avant de fusionner le code
          - text: Frameworks de conformité personnalisés
      - text: Gestion de portefeuilles
        information:
          - text: Tableaux des epics
          - text: Epics multi-niveaux
          - text: Rapport de progression des epics et des tickets
          - text: Hiérarchie de planification
          - text: Feuilles de route au niveau du portefeuille
          - text: Modification groupée d'epics
      - text: Gestion de la chaîne de valeur
        information:
          - text: Informations clés
          - text: Métrique DORA-4 - Fréquence de déploiement
          - text: Métrique Dora-4 - Délai d'exécution des modifications
      - text: 50 000 minutes de calcul par mois
      - text: Assistance
      - text: Utilisateurs de la version gratuite de GitLab avec un niveau d'accès Invité
  time_card:
    img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
    text: Découvrez comment HackerOne réalise des déploiements 5 fois plus rapides en adoptant la sécurité intégrée de GitLab
    cta:
      text: Lire l'étude de cas
      url: /customers/hackerone/
      data_ga_name: HackerOne case study
      data_ga_location: calculator body with time calculator selected
  cards:
  - icon:
      name: money
      alt: Icône d'argent
      variant: marketing
    title: En savoir plus sur les tarifs des forfaits GitLab
    link_text: Consulter la page des tarifs
    link_url: /pricing/
    data_ga_name: learn more about gitlab pricing plans
    data_ga_location: roi calculator
  - icon:
      name: cloud-thin
      alt: Icône Cloud Thin
      variant: marketing
    title: Acheter GitLab auprès d'une marketplace Cloud
    link_text: Contacter l'équipe commerciale
    link_url: /sales/
    data_ga_name: purchase gitlab through cloud marketplaces
    data_ga_location: roi calculator