title: Rapport GitLab Global DevSecOps 2024
og_title: Rapport GitLab Global DevSecOps 2024
description: >-
  GitLab a interrogé plus de 5 000 professionnels du développement, de la sécurité et des opérations sur l’avenir de l’approche DevSecOps. Consultez le rapport pour en savoir plus.
twitter_description: >-
  GitLab a interrogé plus de 5 000 professionnels du développement, de la sécurité et des opérations sur l’avenir de l’approche DevSecOps. Consultez le rapport pour en savoir plus.
og_description: >-
  GitLab a interrogé plus de 5 000 professionnels du développement, de la sécurité et des opérations sur l’avenir de l’approche DevSecOps. Consultez le rapport pour en savoir plus.
og_image: /nuxt-images/developer-survey/2023-devsecops-survey-landing-page-opengraph.jpg
twitter_image: /nuxt-images/developer-survey/2024/2024-devsecops-survey-landing-page-opengraph.jpg
hero:
  year: 2024
  title: Rapport Global DevSecOps
intro:
  survey_header:
    text: >-
      L’enquête de cette année met en évidence l’évolution des comportements à l’égard de la sécurité, de l’IA et de l’expérience des développeurs.
    img_variant: 1
  summary:
    title: Executive summary
    text: >
      Notre enquête, réalisée auprès de plus de 5 000 professionnels DevSecOps dans le monde entier, révèle que les entreprises privilégient les investissements dans les domaines de la sécurité, de l’IA et de l’automatisation. Cette approche a des effets positifs sur l’expérience des développeurs et des équipes de développement logiciel. Cependant, l’enquête a également mis en lumière certains domaines spécifiques, tels que la sécurité de la chaîne d’approvisionnement logicielle, qui nécessitent une attention particulière lors de l’élaboration de stratégies DevSecOps.
  highlights:
    title: Points forts
    type: text
    buttons:
      previous: Précédent
      next: Suivant
    gaLocation: summary
    blocks:
      - header: L’IA est un élément central du développement logiciel
        text: >-
          78 % des répondants déclarent utiliser l’IA pour développer des logiciels ou prévoient de le faire sous 2 ans, contre 64 % en 2023
      - header: Les entreprises prennent l’automatisation au sérieux
        text: >-
          67 % des répondants déclarent que leur cycle de développement logiciel est entièrement ou très largement automatisé
      - header: Les difficultés liées à la chaîne d’outils sont bien réelles
        text: >-
          64 % des répondants ont l'intention de consolider leur chaîne d’outils
      - header: La sécurité de la chaîne d’approvisionnement logicielle est fondamentale
        text: >-
          67 % des développeurs déclarent qu’un quart ou plus de leur code provient de bibliothèques open source. Seulement 21 % des entreprises utilisent actuellement une nomenclature logicielle (SBOM) pour documenter les composants des logiciels qu'elles développent
  list:
    title: Priorités d’investissement informatique en 2024
    items:
      - text: Sécurité
      - text: IA
      - text: Plateforme DevSecOps
      - text: Automatisation
      - text: Cloud computing
form:
    tag: Accès gratuit
    header: Inscrivez-vous pour lire le rapport complet
    text: |
      Accédez gratuitement à l'intégralité du rapport contenant les observations de plus de 5 000 professionnels DevSecOps dans 39 pays à travers le monde.

      **Contenu du rapport**

      - Partie 1 : les principaux domaines d’investissement en 2024
      - Partie 2 : l’IA
      - Partie 3 : l’expérience des développeurs
      - Partie 4 : la sécurité
    formId: 2562 # This has been localized. Reminder to update this ID in localized versions! You can find these here: https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/3789#note_1945222165
    dataLayer: resources
sections:
  - title: 'Sécurité, IA et automatisation : les principaux domaines d''investissement en 2024'
    mobile_subtitle: 'Partie 1 : Tendances en matière d''investissement informatique'
    img_src: '/nuxt-images/developer-survey/2024/investment-trends-hero.png'
    text: >-
      Cette année, nous avons constaté une réorganisation des priorités. Les entreprises cherchent en effet à concilier les priorités traditionnelles à long terme, telles que la sécurité et le cloud, avec des technologies émergentes comme l’IA et l’automatisation.
    subsections:
      - subtext: Priorités d’investissement informatique en 2024
        img_variant: 2
        results_file: 2024_it_investment
        text_blocks:
          - title: Analyse
            text: >
              La sécurité reste une priorité absolue. Elle occupe la première place cette année, même si le pourcentage de répondants citant la sécurité comme priorité a légèrement diminué. Par rapport à 2023, l’adoption de l’IA a réellement progressé, même si l’ampleur de cette progression peut être relativement modeste. Cela révèle néanmoins un changement réel et mesurable. 


              Le nombre de répondants pour lesquels une plateforme DevSecOps est une priorité d’investissement et le nombre de répondants indiquant que leur entreprise utilisera une plateforme DevSecOps cette année sont comparables à ceux de 2023. Cela indique que l’intérêt et l’investissement dans les plateformes DevSecOps demeurent stables. Parallèlement, l’intérêt pour l’automatisation a nettement augmenté en 2024, passant de la sixième place en 2023 à la quatrième cette année.


              L’un des signes les plus marquants de la réorganisation des priorités est le sort réservé au cloud computing : priorité absolue en 2023, il est relégué à la cinquième position cette année. Il est toutefois clair que le cloud n'a pas perdu toute son importance. Le nombre de répondants déclarant que moins de la moitié de leurs applications sont hébergées dans le cloud a considérablement diminué par rapport à l’année dernière (de 68 % en 2023 à 43 % en 2024) et le nombre de répondants déclarant que 50 % ou plus de leurs applications sont exécutées dans le cloud a augmenté en conséquence (de 32 % en 2023 à 55 % en 2024). Cela indique que, bien que le cloud soit toujours critique pour l'entreprise, il est devenu la norme pour de nombreuses entreprises, alors même que la liste des priorités des équipes techniques et des responsables informatiques continue de s'allonger.


              Même si les métriques et l'analyse ne font pas encore partie des cinq principales priorités d'investissement cette année, nous avons observé une nette augmentation par rapport à l'année dernière (de 9 % en 2023 à 12 % en 2024). Cette augmentation laisse entendre qu'il s'agit d'une priorité croissante pour les entreprises.
          - title: Tendances mondiales et perspectives du secteur
            text: >
              ### Les secteurs hautement réglementés redoublent leurs efforts en matière de sécurité

              Sans surprise, la sécurité est restée la première priorité d’investissement dans des secteurs hautement réglementés tels que le secteur public (défini dans notre enquête comme les organisations gouvernementales, aérospatiales et de défense), les services financiers et les télécommunications.


              ### Le secteur technologique ouvre la voie à l'IA

              Les entreprises de logiciels, de SaaS et de matériel informatique se sont montrées particulièrement enthousiastes à propos de l'IA, avec un quart des répondants identifiant l’IA comme une priorité absolue.
  - title: L’IA domine actuellement le secteur, mais son adoption doit être éclairée et réfléchie
    mobile_subtitle: 'Partie 2 : IA'
    img_src: '/nuxt-images/developer-survey/2024/ai-hero.png'
    text: >-
      Comme nous l’avons noté l’année dernière, les professionnels DevSecOps ont une perception positive de l’IA et son intégration dans le cycle du développement logiciel s’accélère. Les données de cette année permettent toutefois d’identifier certains domaines auxquels les entreprises devraient prêter attention afin de s’assurer que l’adoption de l’IA soit aussi productive que possible.
    subsections:
      - subtext: L’IA est désormais incontournable
        img_variant: 3
        inverse: true
        results_file: 2024_ai
        text_blocks:
          - title: Analyse
            text: >
              Cette année, 78 % des répondants déclarent utiliser désormais l’IA pour développer des logiciels ou prévoient de le faire sous 2 ans, contre 64 % l’année dernière. En outre, 39 % des personnes interrogées ont déclaré qu’elles utilisaient déjà l’IA (contre 23 % l’année dernière). En conséquence, le nombre de répondants indiquant que leurs entreprises n'ont pas de projet d'utilisation de l'IA a également diminué.


              Ces résultats soulignent que l’utilisation de l’IA dans le développement logiciel est maintenant acceptée. Un nombre croissant d'entreprises estiment qu'il est indispensable d'intégrer l'IA dans leurs processus, même celles qui avaient initialement des réserves à son égard.
          - title: Tendances mondiales et perspectives du secteur
            text: >
              ### Les États-Unis et le Royaume-Uni sont-ils à la traîne ?

              Près des deux tiers des répondants aux États-Unis et au Royaume-Uni ont convenu qu’il était essentiel d’intégrer l’IA dans le développement logiciel. Cependant, les taux d’adoption de l’IA aux États-Unis (34 %) et au Royaume-Uni (31 %) sont nettement inférieurs à ceux de l’Australie (62 %), de l’Allemagne (53 %), de la France (48 %) et du Japon (48 %).


              ### L'automobile et le secteur public accélèrent l'adoption de l'IA

              Parmi les secteurs inclus dans notre enquête, l’industrie automobile s’est avérée la plus avancée dans l’adoption de l’IA. 66 % des répondants ont déclaré déjà utiliser l’IA pour le développement logiciel. Le secteur public a également témoigné d’une adoption notable de l’IA, avec près de la moitié (47 %) des répondants du secteur public déclarant l’utiliser actuellement.
      - subtext: Les chaînes d’outils à l’ère de l’IA
        results_file: 2024_toolchains
        img_variant: 4
        text_blocks:
          - title: Analyse
            text: >
              Quel est l’impact de l’introduction rapide des outils d’IA sur le quotidien des équipes DevSecOps ? Pour évaluer l'opinion des équipes DevSecOps concernant leurs outils actuels, nous leur avons demandé si elles souhaitaient consolider leur chaîne d'outils.


              Près des trois quarts (74 %) des répondants dont les entreprises utilisent actuellement l’IA pour le développement logiciel ont exprimé leur intention de consolider leur chaîne d’outils, contre 57 % de ceux qui ne l'utilisent pas. Cependant, il n’y a pas de différence significative entre les deux groupes en ce qui concerne le nombre d’outils que les répondants déclarent utiliser.


              Pourquoi l’IA accélérerait-elle le désir de consolidation ? Ce phénomène pourrait s’expliquer par le fait que l’adoption rapide de l’IA met en lumière la lourdeur des chaînes d’outils contre-productives. Dans de nombreux cas, la valeur de l’IA dépend des données dont elle dispose. Par conséquent, plus il y a d’outils dans la chaîne d’outils, plus il y a d’outils avec lesquels une solution d’IA devra s’intégrer. Les équipes tirent davantage de valeur de l’IA lorsque les chaînes d’outils sont plus petites, ce qui facilite l’intégration de l’IA tout au long du cycle du développement logiciel.
      - subtext: Les équipes veulent que l’IA soit intégrée dans tous les aspects du développement logiciel
        inverse: true
        img_variant: 5
        results_file: 2024_ai_integration
        text_blocks:
          - title: Analyse
            text: >
              Comme nous l’avons observé l’année dernière, les équipes d’ingénierie logicielle souhaitent adopter l’IA générative pour les aider à accélérer la création de code. Cette année encore, la génération de code et les suggestions de code (47 %) se placent en tête des cas d’utilisation dans le développement logiciel pour lesquels les répondants montrent un intérêt pour l’application de l’IA, suivis par les explications sur le fonctionnement du code (40 %) et les résumés des modifications du code (38 %).


              Interrogés sur leur utilisation prévue de l’IA ou leur intérêt pour celle-ci, les répondants, cette année, ont identifié un ensemble différent de cas d’utilisation, notamment la prévision des mesures de productivité et l’identification des anomalies (38 %), l’explication sur la manière dont une vulnérabilité peut être exploitée et les remédiations possibles (37 %), ainsi que l’utilisation de chatbots pour permettre aux utilisateurs de poser des questions en langage naturel (36 %).


              Les chatbots apparaissent à la fois dans les cinq principaux cas d’utilisation actuels et dans les cinq principaux cas d’utilisation qui intéressent les répondants. Ce classement suggère que les interfaces de chat en langage naturel sont un moyen attrayant pour les équipes DevSecOps d’interagir avec les outils d’IA.
      - subtext: Exploiter pleinement le potentiel de l’IA
        results_file: 2024_full_potential
        img_variant: 1
        text_blocks:
          - title: Analyse
            text: >
              Bien que la génération de code ait obtenu le taux d'adoption le plus élevé dans notre enquête (probablement car elle a été introduite en premier), les conclusions de notre enquête suggèrent que la véritable opportunité de l'IA ne réside pas dans des tâches ponctuelles, mais plutôt dans l'ensemble du workflow des développeurs.


              Cette année tout comme en 2023, les développeurs ont déclaré passer moins d’un quart de leur temps à écrire du nouveau code. Le reste est consacré aux réunions et aux tâches administratives (15 %), à l’amélioration du code existant (15 %), à la compréhension du code (13 %), aux tests (12 %), à la maintenance du code (11 %), ainsi qu’à l’identification et à l’atténuation des failles de sécurité (10 %). Cela représente plus de 75 % du travail quotidien des développeurs, pour lesquels l’IA peut contribuer à accroître l’efficacité et à améliorer la productivité et la collaboration en expliquant les failles de sécurité, en résumant les modifications de code, en automatisant les tests et bien plus encore.
      - subtext: >-
          Les chefs d’entreprise ont une vision positive de l’IA, mais ont besoin de plus de visibilité sur les expériences de leurs équipes
        inverse: true
        img_variant: 2
        results_file: 2024_ai_training
        text_blocks:
          - title: Analyse
            text: >
              Les dirigeants d'entreprise ont exprimé un optimisme prudent à l’égard de l’IA cette année, tout en se montrant conscients des risques posés par son adoption : 62 % des dirigeants interrogés ont convenu qu'il est indispensable d'utiliser l'IA dans le développement logiciel pour éviter de prendre du retard sur les objectifs fixés, tandis que 56 % s’accordent sur le fait que l’introduction de l’IA dans le cycle du développement logiciel est risquée.


              Cependant, les contributeurs individuels et les managers sont beaucoup plus enclins que les dirigeants d'entreprise à considérer que leur entreprise n'est pas prête à adopter l'IA. En outre, les contributeurs individuels sont bien plus enclins que les dirigeants d'entreprise à affirmer que leur entreprise ne fournit pas la formation et les ressources adéquates pour l’utilisation de l’IA. Cette divergence pourrait indiquer que les dirigeants d’entreprises manquent de visibilité sur la réalité de l’adoption de l’IA dans la pratique. Il est également possible que ces dirigeants tentent de mettre des ressources d’IA à la disposition des employés selon une approche descendante, sans que celles-ci soient adéquates ou que les employés en aient connaissance.


              Comment les dirigeants d'entreprise peuvent-ils bénéficier d'une meilleure visibilité sur la façon dont leurs équipes s’adaptent à l’utilisation de l’IA ? La solution consiste à intégrer des outils d’IA dans un magasin de données unique couvrant toutes les étapes du développement logiciel, notamment via une plateforme DevSecOps. Cette approche permet aux dirigeants d’entreprise de tirer parti de fonctionnalités avancées telles que les rapports sur l’impact de l’IA, pour mieux comprendre l’utilisation de l’IA par leurs équipes, les gains d’efficacité réalisés, ainsi que les éventuels défis qu’elles rencontrent. Ces données peuvent aider les dirigeants à surveiller l’adoption de l’IA et à évaluer les avantages et la valeur commerciale des fonctionnalités d’IA.
  - title: Les chaînes d’outils restent un obstacle à l’expérience des développeurs
    mobile_subtitle: 'Partie 3 : Expérience des développeurs'
    img_src: '/nuxt-images/developer-survey/2024/developer-experience-hero.png'
    text: >-
      Une expérience optimale pour les développeurs implique de supprimer les obstacles afin qu’ils puissent s'intégrer rapidement et commencer à créer de la valeur immédiatement. Il a été démontré que l’expérience des développeurs aide les entreprises à accélérer l’innovation, à gagner en efficacité et à attirer les meilleurs talents. Mais la voie vers une meilleure expérience des développeurs n’est pas toujours un long fleuve tranquille : les nouvelles technologies et tendances peuvent apporter des gains d’efficacité dans certains domaines et des complications dans d’autres. Alors que les entreprises s’efforcent d’améliorer l’expérience des développeurs dans tous les domaines, l’enquête de cette année a révélé un certain nombre d’obstacles potentiels.
    subsections:
      - subtext: "Les développeurs plébiscitent l’automatisation, la collaboration et l’IA"
        results_file: 2024_dev_satisfaction
        img_variant: 3
        call_out_box:
          text: 67 % des répondants déclarent que leur cycle de développement logiciel est entièrement ou très largement automatisé.
          stat:
            number: 67
            color: '#FCA326'
        text_blocks:          
          - title: Analyse
            text: >
              Interrogés sur la manière dont leur entreprise peut améliorer leur expérience, les développeurs sont unanimes : l’automatisation, la collaboration et l’utilisation d’outils d’IA figurent en tête de liste, en plus de considérations professionnelles telles qu’une meilleure rémunération et des conditions de travail plus flexibles.


              Comme nous l’avons vu dans la liste des priorités d’investissement, les entreprises semblent être à l’écoute, car elles investissent de plus en plus dans l’automatisation, la collaboration (sous la forme de plateformes DevSecOps) et l’IA. En effet, plus de la moitié (67 %) des répondants cette année ont déclaré que les processus de développement logiciel sont entièrement ou très largement automatisés. En Europe, où notre enquête a montré une adoption particulièrement rapide de l’IA, ce chiffre était nettement plus élevé (72 %).
      - subtext: Les équipes sont confrontées à une surcharge d’outils
        inverse: true
        img_variant: 4
        results_file: 2024_number_of_tools
        text_blocks:
          - title: Analyse
            text: >
              Malgré un investissement accru dans de nombreux aspects favorisant une expérience de développement positive, un indicateur clé de notre enquête a clairement révélé un signe inquiétant : le nombre d’outils que les équipes utilisent quotidiennement. Au total, 64 % des répondants cette année nous ont fait part de leur intention de consolider leur chaîne d’outils. Plus d’un quart (27 %) des répondants qui nous ont révélé avoir commencé la consolidation ou avoir l'intention de le faire ont déclaré qu'un trop grand nombre d'outils avait un impact négatif sur l'expérience des développeurs, car ils devaient passer trop souvent d'un outil à l'autre.


              Parallèlement à la majorité des répondants qui souhaitent consolider leur chaîne d’outils, nous avons également observé cette année, par rapport à l'année dernière, une augmentation significative du nombre de répondants qui ont déclaré utiliser 6 à 10, 11 à 14, voire plus de 15 outils. En conséquence, le nombre de répondants utilisant 5 outils ou moins a diminué proportionnellement.
          - title: Tendances mondiales et perspectives du secteur
            text: |
              ### Les entreprises technologiques ressentent la pression de la chaîne d'outils
              Ce sont les personnes interrogées dans les entreprises de logiciels, de SaaS et de matériel informatique qui semblent ressentir le plus cette pression : un quart (25 %) des personnes interrogées ont déclaré utiliser 11 outils ou plus. En revanche, dans l'industrie automobile, ils ne sont que 9 % à utiliser 11 outils ou plus.
              
              ### En France, les chaînes d'outils sont plus légères, mais les entreprises souhaitent poursuivre la consolidation

              Bien que moins de la moitié (47 %) des répondants en France déclarent utiliser 6 outils ou plus, contre 70 % en Allemagne et 63 % aux États-Unis et au Canada, la majorité des répondants en France (66 %) souhaitent poursuivre la consolidation de leur chaîne d’outils.
      - subtext: L’intégration ralentit les développeurs
        img_variant: 5
        results_file: 2024_onboarding_time
        text_blocks:
          - title: Analyse
            text: >
              L’intégration des développeurs est un autre indicateur clé de l’expérience des développeurs qui a montré des signes de déclin en 2024, peut-être en partie en raison de la multiplication continue du nombre d'outils.


              Cette année, le nombre de répondants indiquant qu’il faut entre 1 et 2 mois aux développeurs pour être intégrés et devenir productifs a considérablement augmenté par rapport à l’année dernière, et un nombre inférieur de répondants ont déclaré qu’il fallait un mois ou moins.
          - title: Tendances mondiales et perspectives du secteur
            text: >
              ### Les entreprises technologiques et automobiles sont en tête du peloton

              Les entreprises de logiciels, de SaaS et de matériel informatique, ainsi que celles du secteur automobile, ont observé des périodes d’intégration des développeurs relativement courtes. Seulement 16 % et 9 % des répondants, respectivement, rapportent que le processus d’intégration des développeurs dépasse 3 mois. Plus d’un tiers (39 %) des répondants du secteur de l’automobile déclarent que la période d'intégration des développeurs est inférieure à 2 semaines.

              ### Les investissements technologiques portent-ils leurs fruits en Allemagne ?
              
              L’Allemagne a enregistré des taux d’adoption de l’IA (53 %) et de l’automatisation (74 %) plus élevés que d’autres pays dans notre enquête, et nos résultats suggèrent que ces investissements pourraient porter leurs fruits au niveau de l’intégration des développeurs. Parmi les pays inclus dans l’enquête, l’Allemagne compte le plus grand nombre de répondants (18 %) déclarant que l’intégration des développeurs prend moins de 2 semaines.
      - subtext: L’utilisation de l’IA et d'une plateforme DevSecOps accélère l’intégration des développeurs
        inverse: true
        img_variant: 1
        results_file: 2024_ai_onboarding
        text_blocks:
          - title: Analyse
            text: >
              Cette année, nous avons observé que l’utilisation de l’IA et d’une plateforme DevSecOps a un effet positif significatif sur l’intégration des développeurs. Les répondants qui ont déclaré utiliser l’IA pour le développement de logiciels (43 %) sont beaucoup plus enclins que ceux qui n’utilisent pas l’IA (20 %) à affirmer que l’intégration des développeurs prend généralement moins d’un mois. Le même effet a été observé pour l’utilisation d'une plateforme DevSecOps : 44 % de ceux qui utilisent actuellement une plateforme déclarent que l’intégration des développeurs prend moins d’un mois, contre 20 % de ceux qui n’utilisent pas de plateforme.
      - subtext: >-
          Les dirigeants d'entreprise reconnaissent l’importance de la productivité des développeurs, mais la mesurer est difficile
        img_variant: 2
        results_file: 2024_developer_productivity
        text_blocks:
          - title: Analyse
            text: >
              Lorsque les entreprises éliminent les obstacles pour intégrer rapidement les développeurs et leur permettre de commencer à créer de la valeur immédiatement, l'un des résultats les plus significatifs, et le plus mesurable, est la productivité des développeurs.


              Dans notre enquête, plus de la moitié (55 %) des chefs d’entreprise reconnaissent l'importance de la productivité des développeurs pour le succès de leur entreprise, et 57 % sont d'accord sur le fait que la mesure de la productivité des développeurs est essentielle à la croissance de l'entreprise.


              Cependant, seuls 42 % des chefs d'entreprise mesurent actuellement la productivité de leurs développeurs et se montrent satisfaits de leur approche. Plus d’un tiers (36 %) estiment que leurs méthodes de mesure de la productivité des développeurs sont imparfaites, tandis que 15 % souhaitent mesurer la productivité des développeurs, mais ne savent pas comment s'y prendre.


              Tirer parti d’une plateforme DevSecOps fournit une base pour mesurer la productivité des développeurs en rassemblant les métriques de toutes les étapes du processus de développement dans un seul magasin de données. Dans notre enquête, les cadres dirigeants interrogés dont les entreprises utilisent une plateforme DevSecOps (56 %) sont beaucoup plus satisfaits de leur approche actuelle de la mesure de la productivité des développeurs que ceux qui n’en utilisent pas (33 %).
  - title: 'Les responsabilités en matière de sécurité évoluent, mais il reste encore des défis à surmonter'
    mobile_subtitle: 'Partie 4 : Sécurité'
    img_src: '/nuxt-images/developer-survey/2024/security-hero.png'
    text: >-
      La sécurité reste une priorité absolue pour les entreprises et l’enquête de cette année montre que les investissements dans la sécurité sont rentables à bien des égards. Les personnes ayant répondu à l'enquête ont néanmoins signalé plusieurs domaines dans lesquels le changement de mentalité ne s'est pas encore totalement traduit dans la pratique. Elles soulignent également des lacunes que les entreprises devraient prioriser pour intégrer davantage la sécurité dans les workflows de développement logiciel.
    subsections:
      - subtext: La nécessité de la responsabilité partagée en matière de sécurité s’impose
        inverse: true
        img_variant: 3
        results_file: 2024_responsibility_role
        text_blocks:
          - title: Analyse
            text: >
              Cette année, les développeurs sont nettement plus enclins que les équipes de sécurité interrogées à affirmer que la responsabilité de la sécurité incombe aux développeurs. Cette évolution marque un changement par rapport à l'année 2023, où les développeurs avaient plutôt tendance à considérer que la responsabilité de la sécurité revenait aux équipes spécialisées dans ce domaine.


              En outre, un quart (25 %) de tous les répondants sont tout à fait d’accord pour dire qu’ils sont principalement responsables de la sécurité des applications, tandis que 33 % sont d’accord avec cette affirmation. Ainsi, plus de la moitié (58 %) de l'ensemble des répondants se sentent responsables de la sécurité des applications dans une certaine mesure. En d’autres termes, la sécurité n'est pas seulement l'affaire de l'équipe dédiée.


              Malgré l'évolution vers une approche plus collaborative en la matière, les équipes de sécurité ont encore exprimé plusieurs préoccupations lors de notre enquête. Plus de la moitié (58 %) des responsables de la sécurité interrogés ont déclaré qu'il leur est difficile de faire en sorte que le développement priorise la remédiation des vulnérabilités, contre 42 % l’année dernière. D'autre part, 55 % des responsables de la sécurité interrogés ont indiqué que les failles de sécurité sont généralement découvertes par l'équipe de sécurité après la fusion du code dans un environnement de test, contre 46 % l’année dernière.


              Les équipes de sécurité et des opérations sont également plus frustrées à l'égard des chaînes d'outils : 86 % des équipes des opérations interrogées et 84 % des équipes de sécurité interrogées ont déclaré qu’elles consacrent un quart ou plus de leur temps à la maintenance et à l’intégration de la chaîne d’outils, contre 68 % des développeurs.
      - subtext: La sécurité de la chaîne d’approvisionnement logicielle est un point faible potentiel
        results_file: 2024_security
        img_variant: 4
        text_blocks:
          - title: Analyse
            text: >
              Au total, 67 % des développeurs déclarent qu’un quart ou plus de leur code provient de bibliothèques open source. Ce pourcentage est encore plus élevé dans les entreprises de logiciels, de SaaS et de matériel informatique (85 % chez les développeurs) et dans la région Asie-Pacifique (76 %).


              Des fonctionnalités telles qu’une nomenclature logicielle (SBOM) (une liste de tous les composants, bibliothèques et modules qui composent une application) sont essentielles au maintien de la sécurité de la chaîne d’approvisionnement logicielle, d’autant plus que la quantité de code tirée des bibliothèques open source augmente. Cependant, dans notre enquête, seulement 21 % des répondants déclarent que leur entreprise utilise actuellement des SBOM pour sécuriser le cycle du développement logiciel. Dans la région Asie-Pacifique, ce pourcentage est particulièrement bas, à seulement 15 %.


              Les entreprises sont confrontées à un impératif croissant de connaître la composition des logiciels qu’elles produisent. Elles devraient envisager d’utiliser des outils tels que les SBOM en tandem avec les autres fonctionnalités qu’offre une plateforme DevSecOps, telles que l’analyse de la composition logicielle (SCA). Cela leur permettrait de prévenir les failles de sécurité dans la chaîne d’approvisionnement logicielle et de garantir que les composants open source non sécurisés ne génèrent pas de charges de travail supplémentaires pour les équipes de sécurité.
      - subtext: Combler l’écart de perception en matière de sécurité
        inverse: true
        img_variant: 5
        results_file: 2024_security_perception
        text_blocks:
          - title: Analyse
            text: >
              Les résultats de notre enquête ont révélé que les chefs d'entreprise affichent un optimisme quant à l’approche DevSecOps et aux avantages qu'elle apporte en termes de sécurité. Toutefois, comme cela a été le cas avec l'IA, les dirigeants doivent veiller à superviser la mise en œuvre dans la pratique des outils et méthodologies par leurs équipes.


              La sécurité est clairement une priorité pour les chefs d'entreprise et les responsables d'équipe constatent les résultats positifs de ces investissements dans le DevSecOps. Ils identifient le développement d’applications plus sécurisées comme principal avantage du DevOps et DevSecOps, suivi par des cycles d’itération plus rapides et une meilleure qualité du code. Les dirigeants que nous avons interrogés et dont les équipes utilisent actuellement une plateforme DevSecOps ont également cité la sécurité comme le deuxième avantage le plus important de l’adoption d’une plateforme, après l’amélioration de l’efficacité opérationnelle.


              Cependant, les divergences de comportement entre les dirigeants et les contributeurs individuels pourraient suggérer que les dirigeants ne sont pas conscients de certains des défis en matière de sécurité auxquels les équipes sont confrontées. Plus de 60 % des cadres dirigeants nous ont déclaré qu’ils estiment que les pratiques DevSecOps sont bien implantées au sein de leur entreprise, mais beaucoup moins de contributeurs individuels (52 %) ont exprimé le même ressenti. De même, les cadres dirigeants (64 %) sont beaucoup plus enclins que les managers (59 %) et les contributeurs individuels (58 %) à affirmer qu’ils sont confiants dans l’approche de leur entreprise en matière de sécurité des applications.


              Quelle est la raison de cette divergence ? Les équipes de sécurité elles-mêmes ont souligné les frustrations liées aux processus organisationnels qui pourraient avoir un impact négatif sur les pratiques de sécurité. Plus de la moitié (52 %) des experts de la sécurité interrogés ont déclaré que leurs efforts pour corriger rapidement les vulnérabilités sont souvent ralentis par la bureaucratie au sein de leur entreprise, contre 43 % en 2023.


              L’écart de perception entre les dirigeants et les contributeurs individuels pourrait potentiellement empêcher les entreprises de profiter pleinement des avantages de l’approche DevSecOps. Notre enquête révèle qu’investir dans des outils qui favorisent la collaboration et réduisent la bureaucratie, comme une plateforme DevSecOps, est un moyen de combler l’écart : les répondants qui utilisent une plateforme DevSecOps (68 %) étaient plus confiants que ceux qui n’en utilisent pas (56 %) dans la capacité de leur entreprise à maîtriser la sécurité des applications.
  - title: Qui a répondu à l’enquête ?
    demographics_section: true
    img_src: '/nuxt-images/developer-survey/2024/2024-survey-hero.png'
    mobile_subtitle: Données démographiques
    text: >-
      Nous avons recueilli un total de 5 315 réponses à l'enquête en avril 2024 auprès de contributeurs individuels et de leaders du développement, des opérations informatiques et de la sécurité au sein de tout un éventail de secteurs et de tailles d'entreprise dans le monde.

      Nous avons utilisé deux méthodes d'échantillonnage pour la collecte de données :

      - Nous avons distribué l'enquête via les réseaux sociaux et les listes de diffusion de GitLab.

      - Un partenaire de recherche tiers a effectué un échantillonnage de panel, ce qui réduit les biais dans l'échantillon. Notre partenaire de recherche a utilisé son accès exclusif à des listes, panels et bases de données pour recueillir des réponses de qualité et a nettoyé les données dès la collecte pour assurer la qualité des données.
    subsections:
      - subtitle: null
        results_file: 2024_demographics
        text_blocks:
          - title: Méthodologie
            mobile_only: true
            text: >
              Nous avons recueilli un total de 5 315 réponses à l’enquête en avril 2024 auprès de contributeurs individuels et de leaders du développement, des opérations informatiques et de la sécurité au sein de tout un éventail de secteurs et de tailles d’entreprise dans le monde.


              **Nous avons utilisé deux méthodes d’échantillonnage pour la collecte de données :**

               - Nous avons distribué l’enquête via les réseaux sociaux et les listes de diffusion de GitLab.

              - Un partenaire de recherche tiers a effectué un échantillonnage par panel, ce qui réduit les biais dans l’échantillon. Notre partenaire de recherche a utilisé son accès exclusif à des listes, panels et bases de données pour recueillir des réponses de qualité et a nettoyé les données dès la collecte pour assurer la qualité des données. 
all_reports:
  header: Consulter les rapports Global DevSecOps précédents
  reports:
    - label: 2023
      link: '/developer-survey/previous/2023/'
    - label: 2022
      link: '/developer-survey/previous/2022/'
    - label: 2021
      link: '/developer-survey/previous/2021/'
    - label: 2020
      link: '/developer-survey/previous/2020/'
    - label: 2019
      link: '/developer-survey/previous/2019/'
    - label: 2018
      link: '/developer-survey/previous/2018/'
