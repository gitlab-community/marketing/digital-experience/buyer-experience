---
  title: Pourquoi choisir GitLab Premium ?
  description: Améliorez la productivité et la coordination de vos équipes avec GitLab Premium.
  side_menu:
    anchors:
      text: Sur cette page
      data:
      - text: Présentation
        href: "#overview"
        data_ga_name: overview
        data_ga_location: side-navigation
        nodes:
        - text: Récapitulatif
          href: "#wp-summary"
          data_ga_name: summary
          data_ga_location: side-navigation
        - text: Principales solutions
          href: "#wp-key-solutions"
          data_ga_name: key-solutions
          data_ga_location: side-navigation
        - text: Études de cas clients
          href: "#wp-customer-case-studies"
          data_ga_name: customer-case-studies
          data_ga_location: side-navigation
      - text: Calculer votre retour sur investissement
        href: "#wp-roi-calculator"
        data_ga_name: roi-calculator
        data_ga_location: side-navigation
      - text: Fonctionnalités de GitLab Premium
        href: "#wp-premium-features"
        data_ga_name: premium-features
        data_ga_location: side-navigation
        nodes:
        - text: Revues de code plus rapides
          href: "#wp-faster-code-reviews"
          data_ga_name: faster-code-reviews
          data_ga_location: side-navigation
        - text: Processus CI/CD avancé
          href: "#wp-advanced-ci-cd"
          data_ga_name: advanced-ci-cd
          data_ga_location: side-navigation
        - text: Livraison Agile en entreprise
          href: "#wp-enterprise-agile-planning"
          data_ga_name: nterprise-agile-planning
          data_ga_location: side-navigation
        - text: Contrôle des releases
          href: "#wp-release-controls"
          data_ga_name: release-controls
          data_ga_location: side-navigation
        - text: Fiabilité de l'environnement auto-géré
          href: "#wp-self-managed-reliability"
          data_ga_name: self-managed-reliability
          data_ga_location: side-navigation
        - text: Autres fonctionnalités de GitLab Premium
          href: "#wp-other-premium-features"
          data_ga_name: other-premium-features
          data_ga_location: side-navigation
      - text: Nous contacter
        href: "#get-in-touch"
        data_ga_name: get-in-touch
        data_ga_location: side-navigation
    hyperlinks:
      text: Étapes suivantes
      data:
      - text: Acheter GitLab Premium maintenant
        href: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        variant: primary
        icon: false
        data_ga_name: buy-premium
        data_ga_location: side-navigation
      - text: En savoir plus sur GitLab Ultimate
        href: /pricing/ultimate/
        variant: secondary
        icon: true
        data_ga_name: learn-about-ultimate
        data_ga_location: side-navigation
  components:
  - name: plan-summary
    data:
      id: wp-summary
      title: Les avantages de Premium
      subtitle: C'est la solution idéale pour renforcer la productivité et la coordination au sein de votre équipe
      text: "Disponible sous deux options de déploiement (SaaS et Auto-géré), GitLab\_Premium permet d'améliorer la productivité et la collaboration des équipes grâce aux avantages suivants\_:"
      list: 
        - Revues de code plus rapides
        - Contrôle des releases
        - Assistance prioritaire
        - CI/CD avancé
        - Assistance à la mise à niveau en direct
        - Livraison Agile en entreprise
        - Gestionnaire de compte technique pour les clients éligibles
        - Haute disponibilité et reprise après sinistre pour les instances auto-gérées
      cta: 
        text: Comparer toutes les fonctionnalités
        url: /pricing/feature-comparison/
      buttons:
        - variant: primary
          icon: false
          text: Acheter GitLab Premium maintenant
          url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
          data_ga_name: buy-premium-now
          data_ga_location: body
        - variant: secondary
          icon: true
          text: En savoir plus sur GitLab Ultimate
          url: /pricing/ultimate/
          data_ga_name: learn-about-ultimate
          data_ga_location: body
  - name: guest-calculator
    data:
      id: wp-guest-calculator
      title: Calculez le coût pour votre organisation
      input_title: GitLab offre un nombre illimité d'utilisateurs invités gratuits sur les forfaits Ultimate
      caption: |
        *Tous les forfaits sont facturés annuellement. Les prix indiqués peuvent être soumis à des taxes locales et à des retenues à la source. Les prix peuvent varier si les licences sont achetées auprès d'un partenaire ou d'un revendeur. Pour en savoir plus, consultez nos [tarifs](/pricing/){data-ga-name ="pricing" data-ga-location = "guest calculator"}.
      seats:
        title: Nombre de sièges GitLab
        tooltip: Les sièges GitLab correspondent aux utilisateurs dotés d'un accès de rapporteur, de développeur, de chargé de maintenance ou de propriétaire. Les utilisateurs invités ne consomment pas de siège dans GitLab Ultimate. En savoir plus sur l'utilisation des sièges <a href="https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined" data-ga-name="seat usage documentation" data-ga-location="user calculator tooltip">ici</a>.
      guests:
        title: Nombre d'utilisateurs invités
        tooltip: Les utilisateurs invités peuvent créer et assigner des tickets, afficher certaines analyses, éventuellement afficher du code et bien plus encore. En savoir plus <a href="https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions" data-ga-name="project members documentation" data-ga-location="user calculator tooltip">ici</a>.
      premium:
        title: Coût mensuel de GitLab Premium*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        data_ga_name: buy premium
        data_ga_location: guest calculator
        button: Acheter GitLab Premium
      ultimate:
        title: Coût mensuel de GitLab Ultimate*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        data_ga_name: buy ultimate
        data_ga_location: guest calculator
        button: Acheter GitLab Ultimate
  - name: 'by-solution-value-prop'
    data:
      id: wp-key-solutions
      light_background: true
      small_margin: true
      large_card_on_bottom: true
      title: GitLab Premium est là pour vous aider
      cards:
        - icon:
            name: increase
            alt: Icône d'augmentation
            variant: marketing
          title: Gagner en efficacité opérationnelle
          description: GitLab Premium introduit des fonctionnalités qui permettent aux entreprises d'analyser les tendances des équipes, des projets et des groupes afin de découvrir des modèles et de mettre en place des normes cohérentes pour améliorer la productivité globale.
        - icon:
            name: speed-alt
            variant: marketing
            alt: Icône de compteur de vitesse
          title: Développer plus rapidement des produits de meilleure qualité
          description: Avec un processus CI/CD avancé et des revues de code plus rapides, GitLab Premium vous aide à créer, à maintenir, à déployer et à surveiller des pipelines d'applications complexes afin de fournir des produits plus rapidement.
        - icon:
            name: lock-alt-5
            alt: Icône de cadenas
            variant: marketing
          title: Réduire les risques liés à la sécurité et à la conformité
          description: Les contrôles de releases dans GitLab Premium garantissent que les équipes livrent un code de haute qualité et sécurisé.
  - name: 'education-case-study-carousel'
    data:
      id: 'wp-customer-case-studies'
      case_studies:
        - logo_url: /nuxt-images/logos/nvidia-logo.svg
          text: Comment GitLab Geo soutient l'innovation de NVIDIA
          img_url: /nuxt-images/blogimages/nvidia.jpg
          case_study_url: /customers/nvidia/
          data_ga_name: Nvidia case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/case-study-logos/FujitsuCTL_Logo.png
          img_url: /nuxt-images/blogimages/fujitsu.png
          text: Fujitsu Cloud Technologies améliore la vélocité de déploiement et les workflows interfonctionnels avec GitLab
          case_study_url: /customers/fujitsu/
          data_ga_name: fujitsu case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/customers/credit-agricole-logo-1.png
          img_url: /nuxt-images/blogimages/creditagricole-cover-image.jpg
          text: Comment Crédit Agricole Corporate & Investment Bank (CACIB) a transformé son workflow mondial grâce à GitLab
          case_study_url: /customers/credit-agricole
          data_ga_name: credit-agricole case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/esa-logo.svg
          img_url: /nuxt-images/blogimages/ESA_case_study_image.jpg
          text: Comment l'Agence spatiale européenne utilise GitLab pour se concentrer sur les missions spatiales
          case_study_url: /customers/european-space-agency
          data_ga_name: european-space-agency case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/sopra_steria-logo.wine.svg
          img_url: /nuxt-images/blogimages/soprasteria.jpg
          text: Comment GitLab est devenue la pierre angulaire de l'activation numérique pour Sopra Steria
          case_study_url: /customers/sopra_steria
          data_ga_name: sopra_steria case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/customers/hemmersbach_logo.svg
          img_url: /nuxt-images/blogimages/hemmersbach_case_study.jpg
          text: Hemmersbach a réorganisé sa chaîne de compilation et augmenté sa vitesse de 59x
          case_study_url: /customers/hemmersbach
          data_ga_name: hemmersbach case study
          data_ga_location: case study carousel
  - name: roi-calculator-block
    data:
      id: wp-roi-calculator
      header:
        gradient_line: true
        title:
          text: Calculer votre retour sur investissement
          anchor: wp-roi-calculator
        subtitle: Combien coûte votre chaîne d'outils ?
      calc_data_src: fr-fr/calculator/index
  features_block:
    id: wp-premium-features
    tier: premium
    header:
      gradient_line: true
      title:
        text: Fonctionnalités de GitLab Premium
        anchor: wp-premium-features
        button:
          text: Comparer toutes les fonctionnalités
          data_ga_name: Compare all features
          data_ga_location: body
          url: /fr-fr/pricing/feature-comparison/
    pricing_themes:
      - id: wp-faster-code-reviews
        theme: Revues de code plus rapides
        text: assurent une qualité de code élevée au sein des différentes équipes grâce à des workflows de revue de code transparents.
        link:
          text: En savoir plus
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/development/code_review.html
      - id: wp-advanced-ci-cd
        theme: CI/CD avancé
        text: vous permet de créer, de maintenir, de déployer et de surveiller des pipelines complexes.
        link:
          text: En savoir plus
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: /solutions/continuous-integration/
      - id: wp-enterprise-agile-planning
        theme: La livraison Agile en entreprise
        text: vous aide à planifier et à gérer vos projets, programmes et produits avec un support Agile intégré.
        link:
          text: En savoir plus
          data_ga_name: Agile Planning learn more
          data_ga_location: body
          url: /solutions/agile-delivery/
      - id: wp-release-controls
        theme: Les contrôles des releases
        text: garantissent que les équipes expédient un code de haute qualité et sécurisé.
        link:
          text: En savoir plus
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
      - id: wp-self-managed-reliability
        theme: La fiabilité de la version auto-gérée
        text: assure la reprise après sinistre, la haute disponibilité et l'équilibrage de charge de votre déploiement auto-géré.
        link:
          text: En savoir plus
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/administration/reference_architectures/#traffic-load-balancer
      - id: wp-other-premium-features
        text: Autres fonctionnalités de GitLab Premium