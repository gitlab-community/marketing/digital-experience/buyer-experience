---
  title: Piattaforma
  description: Scopri di più su come la piattaforma GitLab può aiutare i team a collaborare e a sviluppare software più velocemente.
  hero:
    note: La più completa
    header: piattaforma DevSecOps
    sub_header: basata su IA
    description: Distribuisci software migliori più velocemente con un'unica piattaforma per l'intero ciclo di vita della distribuzione.
    image:
      src: /nuxt-images/platform/loop-shield-duo.svg
      alt: The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).
    button:
      text: Fai una prova gratuita
      href: https://gitlab.com/-/trial_registrations/new?glm_source=/solutions/ai/&glm_content=default-saas-trial
      variant: secondary
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      text: Scopri i prezzi
      href: /pricing/
      variant: tertiary
      icon: chevron-lg-right
      data_ga_name: pricing
      data_ga_location: hero
  table_data:
    - name: "Pianificazione"
      link: "/solutions/agile-delivery/"
      icon: "plan-alt-2"
      features:
        - name: "Report DevOps"
          link: "https://docs.gitlab.com/ee/administration/analytics/dev_ops_reports"
        - name: "Metriche DORA"
          link: "/solutions/value-stream-management/dora/"
        - name: "Gestione del flusso del valore"
          link: "/solutions/value-stream-management/"
        - name: "Pages"
          link: "https://docs.gitlab.com/ee/user/project/pages/"
        - name: "Wiki"
          link: "https://docs.gitlab.com/ee/user/project/wiki/"
        - name: "Gestione del portfolio"
          link: "https://docs.gitlab.com/ee/topics/plan_and_track.html#portfolio-management"
        - name: "Pianificazione del team"
          link: "https://docs.gitlab.com/ee/topics/plan_and_track.html#team-planning"
        - name: "Riepilogo dell'epic"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
        - name: "Riepilogo del ticket"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
        - name: "Genera descrizione del ticket"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#issue-description-generation"
        - name: "Riepilogo delle discussioni"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
      replacement: Sentry
    - name: "Gestione del codice sorgente"
      link: "/solutions/source-code-management/"
      icon: "cog-code"
      features:
        - name: "Sviluppo da remoto"
          link: "https://docs.gitlab.com/ee/user/project/remote_development/"
        - name: "Web IDE"
          link: "https://docs.gitlab.com/ee/user/project/web_ide/"
        - name: "CLI di GitLab"
          link: "https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/index.html"
        - name: "Flusso di lavoro di revisione del codice"
          link: "https://docs.gitlab.com/ee/user/project/merge_requests/"
        - name: "Suggerimenti di codice"
          link: "https://docs.gitlab.com/ee/user/project/repository/code_suggestions/"
        - name: "Spiegazione del codice"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#code-explanation-in-the-ide"
        - name: "Riepilogo delle revisioni del codice"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#code-review-summary"
        - name: "Generazione di test"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#test-generation"
      replacement: GitHub
    - name: "Integrazione continua"
      link: "/solutions/continuous-integration/"
      icon: "automated-code-alt"
      features:
        - name: "Gestione dei segreti"
          link: "https://docs.gitlab.com/ee/ci/secrets/"
        - name: "App di revisione"
          link: "https://docs.gitlab.com/ee/ci/review_apps/"
        - name: "Test e copertura del codice"
          link: "https://docs.gitlab.com/ee/ci/testing/"
        - name: "Coda di merge"
          link: "https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html"
        - name: "Revisori suggeriti"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#suggested-reviewers"
        - name: "Riepilogo delle richieste di merge"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#merge-request-summary"
        - name: "Analisi delle cause principali"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#root-cause-analysis"
        - name: "Riepilogo delle discussioni"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
      replacement: Jenkins
    - name: "Sicurezza"
      link: "/solutions/security-compliance/"
      icon: "secure-alt-2"
      features:
        - name: "Scansione dei container"
          link: "https://docs.gitlab.com/ee/user/application_security/container_scanning/"
        - name: "Analisi della composizione del software"
          link: "https://docs.gitlab.com/ee/user/application_security/dependency_scanning/"
        - name: "Sicurezza delle API"
          link: "https://docs.gitlab.com/ee/user/application_security/api_security/"
        - name: "Fuzzing basato sulla copertura del codice"
          link: "https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/"
        - name: "DAST"
          link: "https://docs.gitlab.com/ee/user/application_security/dast/"
        - name: "Code Quality"
          link: "https://docs.gitlab.com/ee/ci/testing/code_quality"
        - name: "Rilevamento dei segreti"
          link: "https://docs.gitlab.com/ee/user/application_security/secret_detection/"
        - name: "SAST"
          link: "https://docs.gitlab.com/ee/user/application_security/sast/"
        - name: "Spiegazione delle vulnerabilità"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#vulnerability-explanation"
        - name: "Risoluzione delle vulnerabilità"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#vulnerability-resolution"
      replacement: Snyk
    - name: "Conformità "
      icon: "protect-alt-2"
      link: "/solutions/continuous-software-compliance/"
      features:
        - name: "Informazioni sulla release"
          link: "https://docs.gitlab.com/ee/user/project/releases/release_evidence"
        - name: "Gestione della conformità"
          link: "https://docs.gitlab.com/ee/administration/compliance.html"
        - name: "Eventi di audit"
          link: "https://docs.gitlab.com/ee/administration/audit_events.html"
        - name: "Distinta base del software"
          link: "/solutions/supply-chain/"
        - name: "Gestione delle dipendenze"
          link: "https://docs.gitlab.com/ee/user/application_security/dependency_list/"
        - name: "Gestione delle vulnerabilità"
          link: "https://docs.gitlab.com/ee/user/application_security/security_dashboard/"
        - name: "Gestione delle politiche di sicurezza"
          link: "https://docs.gitlab.com/ee/user/application_security/policies/"
    - name: "Registro degli artefatti"
      icon: "package-alt-2"
      features:
        - name: "Registro virtuale"
          link: "https://docs.gitlab.com/ee/user/packages/dependency_proxy/"
        - name: "Registro dei container"
          link: "https://docs.gitlab.com/ee/user/packages/container_registry/"
        - name: "Registro dei grafici Helm"
          link: "https://docs.gitlab.com/ee/user/packages/helm_repository/#helm-charts-in-the-package-registry"
        - name: "Registro dei pacchetti"
          link: "https://docs.gitlab.com/ee/user/packages/"
      replacement: JFrog
    - name: "Distribuzione continua"
      icon: "continuous-delivery-alt"
      link: "/solutions/continuous-integration/"
      features:
        - name: "Orchestrazione delle release"
          link: "https://docs.gitlab.com/ee/user/project/releases/"
        - name: "Infrastructure as Code"
          link: "https://docs.gitlab.com/ee/user/infrastructure/iac/index.html"
        - name: "Flag funzionalità"
          link: "https://docs.gitlab.com/ee/operations/feature_flags.html"
        - name: "Gestione dell'ambiente"
          link: "https://docs.gitlab.com/ee/ci/environments/"
        - name: "Gestione del deployment"
          link: "https://docs.gitlab.com/ee/topics/release_your_application.html"
        - name: "Auto DevOps"
          link: "/solutions/delivery-automation/"
      replacement: Harness
    - name: "Osservabilità"
      icon: "monitor-alt-2"
      link: "/solutions/analytics-and-insights/"
      features:
        - name: "Service Desk"
          link: "https://docs.gitlab.com/ee/user/project/service_desk/"
        - name: "Gestione della pianificazione della reperibilità"
          link: "https://docs.gitlab.com/ee/operations/incident_management/oncall_schedules.html"
        - name: "Gestione degli incidenti"
          link: "https://docs.gitlab.com/ee/operations/incident_management/"
        - name: "Monitoraggio degli errori"
          link: "https://docs.gitlab.com/ee/operations/error_tracking.html"
        - name: "Visualizzazione dell'analisi del prodotto"
          link: "https://docs.gitlab.com/ee/user/analytics/"
        - name: "Previsione del flusso del valore"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#value-stream-forecasting"
        - name: "Dati sul prodotto basati sull'IA"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#product-analytics"
      replacement: Sentry
  benefits:
    title: Un’unica piattaforma
    subtitle: a supporto dei team Dev, Sec e Ops
    image:
      image_url: "/nuxt-images/platform/one-platform.svg"
      alt: code source image
    is_accordion: true
    tabs:
      - tabButtonText: Sviluppo
        data_ga_name: development
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Flusso di lavoro basato sull'IA
              text: Aumenta l'efficienza e riduci la durata del ciclo di ogni utente con l'aiuto dell'IA, in ogni fase dello sviluppo software, dalla pianificazione e creazione del codice fino ai test, alla sicurezza e al monitoraggio.
              headerCtas:
                - externalUrl: /gitlab-duo/
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
              ctaHeader: "Guardalo in azione:"
              ctas:
                - externalUrl: https://player.vimeo.com/video/855805049
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/894621401
                  text: Suggerimenti di codice
                  data_ga_name: Code Suggestions
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/927753737
                  text: Chat
                  data_ga_name: Chat
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Applicazione singola
              text: GitLab include tutte le funzionalità DevSecOps in un'unica applicazione con un archivio dati unificato, per avere tutto ciò che cerchi in un unico posto.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781
                  text: Video di GitLab sull'uso delle metriche DORA
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062?h=752d064728&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: Video sulla dashboard dei flussi di valore di GitLab
                  data_ga_name: GitLab's Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Produttività più elevata per gli sviluppatori
              text: L'applicazione singola di GitLab migliora l'esperienza dell'utente, che a sua volta migliora la durata del ciclo e aiuta a prevenire la commutazione di contesto.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925629920
                  text: Video sulla gestione del portfolio di GitLab
                  data_ga_name: GitLab's Portfolio Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925632272
                  text: Video sulla gestione degli OKR di GitLab
                  data_ga_name: GitLab's OKR Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925633691
                  text: Video sui problemi di progettazione su GitLab
                  data_ga_name: Design Uploads to GitLab issues
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Migliore automazione
              text: Gli strumenti di automazione di GitLab sono più affidabili e ricchi di funzionalità e ti aiutano a ridurre il carico cognitivo e il lavoro non necessario.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023715
                  text: Video di panoramica sulla CD di GitLab
                  data_ga_name: GitLab's CD Overview
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://docs.gitlab.com/ee/operations/error_tracking.html
                  text: Documentazione sul monitoraggio degli errori
                  data_ga_name: Error tracking documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Documentazione sulla gestione degli incidenti
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/2DKPuM0RDt2jdfzIaTroKS/1aeee8ed2e7939523ef89e7046d5a468/iron-mountain.svg
                quote: GitLab lega la strategia all'ambito d'uso e al codice. La sua visione in questo senso è straordinaria.Apprezzo molto l'investimento continuo di GitLab nella piattaforma.
                metrics:
                  - number: 150.000 $
                    text: risparmio sui costi approssimativo all'anno
                  - number: 20 ore
                    text: di onboarding risparmiate per progetto
                author:
                  headshot: https://images.ctfassets.net/xz1dnu24egyd/1G9lo7UyVxvW6m1CTNND9b/7b8b0449fc8ae1299e3b4fc56f4dd363/JasonManoharan.png
                  name: Jason Monoharan.
                  title: vicepresidente della tecnologia.
                  company: Iron Mountain
                cta: 
                  text: Leggi lo studio
                  url: '/customers/iron-mountain/'
                  dataGaName: iron mountain case study
                  dataGaLocation: body
          cta:
            title: Sfrutta tutta la potenza dell’IA con
            highlight: GitLab Duo
            text: Scopri di più
            externalUrl: /gitlab-duo/
            data_ga_name: GitLab Duo
            data_ga_location: body
            additionalIcon: vero
      - tabButtonText: Sicurezza
        data_ga_name: security
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Sicurezza integrata fin dal principio
              text: Le funzionalità di sicurezza di GitLab, come DAST, test di fuzzing, scansione dei container e screening delle API, sono integrate end-to-end.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925635707
                  text: Test dinamico della sicurezza delle applicazioni (DAST)
                  data_ga_name: Dynamic Application Security Testing (DAST) video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925676815
                  text: Video sulla scansione dei container
                  data_ga_name: Container scanning video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925677603
                  text: Video sulla sicurezza dell'API e sul fuzzing dell'API web
                  data_ga_name: API security and web API Fuzzing video
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Conformità e gestione precisa dei criteri
              text: GitLab offre una soluzione di governance completa che consente la separazione dei compiti tra i team. Con l'editor dei criteri di GitLab puoi personalizzare le regole di approvazione in base ai requisiti di conformità aziendali, riducendo i rischi.
              ctas:
                - externalUrl: https://docs.gitlab.com/ee/administration/compliance.html
                  text: Documentazione sulla gestione della conformità
                  data_ga_name: Compliance Management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://player.vimeo.com/video/925679314
                  text: Video sui framework di conformità di GitLab
                  data_ga_name: GitLab's Compliance Frameworks
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925679982
                  text: Video sulla gestione dei requisiti di GitLab
                  data_ga_name: GitLab's Requirements Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Automazione della sicurezza
              text: Gli strumenti di automazione avanzati di GitLab abilitano le misure protettive della velocità, garantendo che il codice venga scansionato automaticamente per individuare eventuali vulnerabilità.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925680640
                  text: Video della dashboard di sicurezza di GitLab
                  data_ga_name: GitLab's Security Dashboard
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/4mEBdmMQrpjOn1VJ2idd9h/39fbbf0fc290dda417a35d1e437de4a3/hackerone.svg
                quote: GitLab ci aiuta a individuare tempestivamente le falle nella sicurezza e ha integrato questi strumenti nel flusso degli sviluppatori. Gli ingegneri possono inviare codice alla CI di GitLab, ottenere un feedback immediato da uno dei tanti audit a cascata e vedere se è presente una vulnerabilità di sicurezza integrata. Possono persino creare un nuovo passaggio per testare un problema di sicurezza specifico.
                metrics:
                  - number: 7,5 volte
                    text: maggiore la velocità di creazione delle pipeline
                  - number: 4 ore
                    text: il tempo di sviluppo per tecnico risparmiato alla settimana
                author:
                  name: Mitch Trale
                  title: responsabile infrastrutture
                  company: Hackerone
                cta: 
                  text: Leggi lo studio
                  url: '/customers/hackerone/'
                  dataGaName: hackerone case study
                  dataGaLocation: body
          cta:
            title: Scopri come aggiungere analisi di sicurezza alla tua
            highlight: Pipeline CI
            text: Avvia la demo
            data_ga_name: ci pipeline
            data_ga_location: body
            modal: true
            iconName: slp-laptop-video
            demo:
              subtitle: Aggiungi scansioni di sicurezza alla tua pipeline CI/CD
              video:
                url: https://capture.navattic.com/clq78b76l001b0gjnbxbd5k1f
              scheduleButton:
                text: Pianifica una demo personalizzata
                href: /sales/
                data_ga_name: demo
                data_ga_location: body
      - tabButtonText: Operazioni
        data_ga_name: operations
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Scala i carichi di lavoro aziendali
              text: GitLab offre la massima scalabilità e la possibilità di gestire ed eseguire l'upgrade con tempi di inattività quasi nulli.
              ctas:
                - externalUrl: https://docs.gitlab.com/ee/user/infrastructure/iac/
                  text: Documentazione sull'Infrastructure as Code (IaC)
                  data_ga_name: Infrastructure as code (IaC) documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Documentazione sulla gestione degli incidenti
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Visibilità eccezionale sulle metriche
              text: L'archivio dati unificato di GitLab fornisce dati e analisi per l'intero ciclo di sviluppo software in un unico luogo, eliminando la necessità di ulteriori integrazioni del prodotto.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781
                  text: Video di GitLab sull'uso delle metriche DORA
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062
                  text: Video sulla dashboard dei flussi di valore di GitLab
                  data_ga_name: GitLab's Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Supporto cloud native, multi cloud e legacy
              text: La piattaforma DevSecOps completa di GitLab consente ai team di avere le stesse metriche di produttività e governance, indipendentemente dal mix di infrastrutture.
              ctas:
                - externalUrl: /topics/multicloud/
                  text: Documentazione multicloud
                  data_ga_name: Multicloud documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: /solutions/gitops/
                  text: Documentazione GitOps
                  data_ga_name: GitOps documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Riduzione del costo totale di proprietà
              text: ''
              ctas:
                - description: "Scopri come il più grande appaltatore della difesa al mondo usa GitLab per ridurre le toolchain, accelerare la produzione e migliorare la sicurezza:"
                  externalUrl: /customers/lockheed-martin/
                  text: Case study su Lockheed Martin
                  data_ga_name: Lockheed Martin case study
                  data_ga_location: body
                  iconName: gl-doc-text
                - description: 'Scopri come CARFAX ha perfezionato la sua toolchain DevSecOps e migliorato la sicurezza con GitLab:'
                  externalUrl: /customers/carfax/
                  text: Caso di studio CARFAX
                  data_ga_name: CARFAX case study
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/rdWpo3pKJBUFLzDFmTNIE/d69e61550635ebba0556216c44d401a7/forrester-logo.svg
                quote: L'efficienza di sviluppo e deployment è stata migliorata di oltre l'87%, con un risparmio di più di 23 milioni di dollari.GitLab ha permesso alle aziende di ridurre drasticamente il tempo trascorso in ogni fase dell'intero ciclo di vita DevOps."
                metrics:
                  - number: 200,5 milioni di dollari
                    text: i benefici totali in tre anni
                  - number: 427%
                    text: il ritorno totale sull'investimento (ROI)
                cta: 
                  text: Leggi lo studio
                  url: 'https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html'
                  dataGaName: resources study forrester
                  dataGaLocation: body
          cta:
            title: La tua toolchain
            highlight: è costosa?
            text: Prova il nostro calcolatore del ROI
            externalUrl: /calculator/
            data_ga_name: try our roi calculator
            data_ga_location: body
  video_spotlight:
    header: Vuoi aumentare la velocità? Consolida subito la tua toolchain.
    list:
      - text: Migliora la collaborazione
        iconName: gl-check
      - text: Riduci gli oneri amministrativi
        iconName: gl-check
      - text: Migliora la sicurezza
        iconName: gl-check
      - text: Riduci il costo totale di proprietà
        iconName: gl-check
      - text: Scala in modo fluido
        iconName: gl-check
    blurb: |
      **Non sai da dove iniziare?**
       Il nostro reparto vendite può aiutarti.
    video:
      title: Perché scegliere GitLab
      url: https://player.vimeo.com/video/799236905?h=4eee39a447
      text: Scopri di più
      data_ga_name: Learn More
      data_ga_location: body
      image: https://images.ctfassets.net/xz1dnu24egyd/7KcXktuJTizzgMwF5o8n66/eddd4708263c41d1afa49399222d8f55/platform-video.png
    button:
      externalUrl: /sales/
      text: Contatta il reparto vendite
      data_ga_name: sales
      data_ga_location: body
  recognition:
    heading: I leader del settore scelgono GitLab
    subtitle: GitLab è leader in tutte le categorie DevOps sulla piattaforma G2.
    badges:
          - src: https://images.ctfassets.net/xz1dnu24egyd/712lPKncEMDkCErGOD91KQ/b3632635c9e3bf13a2d02ada6374029b/DevOpsPlatforms_Leader_Leader.png
            alt: G2 - Winter 2024 - Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/17EE9DHTQpLyGbJAQslSEk/e04bb1e6e71d895af94d10ffb85471e3/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 - Winter 2024 - Easiest to Use
          - src: https://images.ctfassets.net/xz1dnu24egyd/1SNrMzYNTrOvEnOuNucAMD/be22fc71855a96d9c05627c7085148a9/users-love-us.png
            alt: G2 - Winter 2024 - Users love us
          - src: https://images.ctfassets.net/xz1dnu24egyd/3UhXS8fSwKieddbliKZEGn/9bd6e04223769aa0869470befd78dc62/ValueStreamManagement_BestUsability_Total.png
            alt: G2 - Winter 2024 - Best Usability
          - src: https://images.ctfassets.net/xz1dnu24egyd/7AODDnwkskODEsylwXkBys/908eb94a40d8ab2f64bdcacbd18f35a3/DevOpsPlatforms_Leader_Europe_Leader.png
            alt: G2 - Winter 2024 - Leader Europe
          - src: https://images.ctfassets.net/xz1dnu24egyd/4xk6CmKqUndyuir0m4rF5b/b069fcc665b3b11c2a017fb36aa55130/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 - Winter 2024 - Leader Enterprise
          - src: https://images.ctfassets.net/xz1dnu24egyd/7jG9DylTxCnElENurGpocF/4241ca192ba6ec8c1d73d07c5a065d76/DevOpsPlatforms_MomentumLeader_Leader.png
            alt: G2 - Winter 2024 - Momentum Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/I7XhKHEf8rB7o1zYwYyBf/0b62ae05ecb749e83cd453de13972482/DevOps_Leader_Americas_Leader.png
            alt: G2 - Winter 2024 - Leader Americas
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartner logo
        text: 'GitLab è leader nel Gartner® Magic Quadrant™ 2024 per le piattaforme DevOps'
        link:
          text: Leggi il report
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forrester logo
        text: "GitLab è l'unico leader nella valutazione The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
        link:
          text: Leggi il report
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
  pricing:
    header: Scopri il migliore piano di tariffe per il tuo team in crescita
    ctas:
      - button:
          externalUrl: /pricing/premium/
          text: Perché scegliere GitLab Premium?
          data_ga_name: why gitlab premium
          data_ga_location: body
      - button:
          externalUrl: /pricing/ultimate/
          text: Perché scegliere GitLab Ultimate?
          data_ga_name: why gitlab ultimate
          data_ga_location: body