---
  title: ROI計算ツール
  description: ここでは、GitLabのROI計算ツールカテゴリに関する情報を確認できます。詳細をこちらで確認してGitLabがもたらす価値をご覧ください！
  heading: ツールチェーンにどれだけコストをかけていますか？
  tabData:
    - tabName: コストを節約
      tabId: cost
    - tabName: 時間を節約
      tabId: time
  cost_steps: 
    - 利用者
    - 現在の支出
    - 節約できる額
  time_steps:
    - 利用者
    - 現在の時間
    - 節約できる時間
  cost_roi:
    people: ツールチェーンを使用するメンテナーの人数はどのくらいいますか？
    users: ユーザー数
    maintainers: DevSecOpsツールのメンテナー数
    nextStep: 次のステップに進む
    spendPerYear: これらの機能に対する年間の支出（米ドル）はおおよそいくらですか？
    sourceCodeManagement: ソースコード管理
    agileProjectManagement: アジャイルプロジェクト管理
    continuousIntegration: 継続的インテグレーション
    apm: アジャイルポートフォリオ管理
    continuousDelivery: 継続的なデリバリー
    applicationSecurity: アプリケーションセキュリティ
    registries: レジストリ
    savings: 節約できる金額を確認
    restart: 計算ツールを再起動
  time_roi:
    people: ツールチェーンを使用するメンテナーの人数はどのくらいいますか？
    users: ユーザー数
    maintainers: DevSecOpsツールのメンテナー数
    nextStep: 次のステップに進む
    howManyDaysToProduction: アプリケーションの更新（コードコミット）が本番環境に到達するのに何日かかりますか？
    daysToProduction: 本番環境までの日数
    hoursPerWeek: 1週間あたりの時間
    developmentConfigurationHours: デベロッパーは、DevOpsツールチェーンでさまざまなツールの構成／統合に週に何時間を費やしていますか？
    developmentSecurityHours: デベロッパーは、本番環境に入る前に、セキュリティ上の脆弱性の修正に週に何時間を費やしていますか？
    savings: 節約できる金額を確認
    restart: 計算ツールを再起動
  tooltips:
    numberOfUsers: 既存のツールセットを使用している従業員は何人いますか？
    numberOfMaintainers: |
      何人の従業員がツールセットをインストール、統合、管理していますか？平均年間給与は97,000米ドルと想定します。出典：<a href="https://www.glassdoor.co.in/Salaries/us-software-developer-salary-SRCH_IL.0,2_IN1_KO3,21.htm">Glassdoor</a>
    sourceCodeManagement: アセットのバージョン管理、ブランチ機能、コードレビューなどを含む、ソースコード管理ツールの年間支出（米ドル）。例：GitHub、BitBucketなどのツール。
    continuousIntegration: ビルド、テスト、検証を含む継続的インテグレーションツールの年間支出（米ドル）。例：Jenkins、CircleCIなどのツール。
    continuousDelivery: stagingステージや本番環境に安全にデプロイするために使用している継続的なデリバリーツールの年間支出（米ドル）。例：Harness、ArgoCDなどのツール。
    applicationSecurity: 静的および動的テストを含むアプリケーションセキュリティツールの年間支出（米ドル）。例：Snyk、Veracode、Sonarcubeなどのツール。
    agileProjectManagement: チームやプロジェクト内での計画に使用するアジャイルプロジェクト管理ツール（Jira、Trello、Asanaなどのツール）の年間支出（米ドル）。
    agilePortfolioManagement: 複数のチームやプロジェクトにまたがる計画時に使用するアジャイルポートフォリオ管理ツールの年間支出（米ドル）。例：Planview Enterprise One、Jira Alignなどのツール。
    registries: コンテナおよびパッケージレジストリの年間支出（米ドル）。例：Artifactory、Docker Hubなどのツール。
  currentSpendTooltip: 現在の支出にはツールとメンテナンスの費用が含まれます
  cost_results:
    heading: ツールチェーンには現在大きなコストがかかっています
    monetary_sign: $
    premium_description: GitLab Premiumは、チームの生産性とコラボレーションを強化するのに最適な選択肢です。
    disclaimer: 表示されている結果は、同様の規模の組織が報告した節約額に基づいています。結果は推定値であり、計算に使用されたさまざまな要因に基づいて変動することがあります。
    youCouldSave: GitLab Ultimateに切り替えた場合の推定節約額
    ultimateSwitch: ！
    ultimate_description: GitLab Ultimateは、組織全体のセキュリティ、コンプライアンス、プランニングを実現するための最良の選択肢です。
    current_spend: 現在の支出
    vs: 対
    gitlab_ultimate: GitLab Ultimate
    free_trial: 無料トライアルを開始
    buy_ultimate: Ultimateを今すぐ購入
    results_disclaimer: 結果は推定値であり、計算に使用されたさまざまな要因に基づいて変動することがあります。
    premiumSwitch: ！
    gitlab_premium: GitLab Premium
    buy_premium: Premiumを今すぐ購入
  time_results: 
    toolchain_cost: 現在の支出額
    hoursPerProject: プロジェクトあたりの時間
    hours: 時間
    save: 削減
    switching: 切り替えた場合の年間節約額
    efficiency: 効率性の向上
    premium: GitLab Premium
    ultimate: GitLab Ultimate
    results: 表示されている結果は、同様の規模の組織が報告した節約額に基づいています。結果は推定値であり、計算に使用されたさまざまな要因に基づいて変動することがあります。
    tooltip: 計算値はGitLabがForrester Consultingに委託した2022年のTotal Economic Impact™調査に基づいています。
    ultimateButton: 
      text: Ultimateの詳細はこちら
      href: /pricing/ultimate/
      variant: primary
      data_ga_name: ultimate learn more
      data_ga_location: calculator body with time calculator selected
    premiumButton: 
      text: Premiumの詳細はこちら
      href: /pricing/premium/
      variant: primary
      data_ga_name: premium learn more
      data_ga_location: calculator body with time calculator selected
    salesButton: 
      text: セールスチームへのお問い合わせ
      href: /sales/
      variant: tertiary
      data_ga_name: sales
      data_ga_location: calculator body with time calculator selected
  marketoForm:
    contactUs: 削減できるコストについて詳細を確認するにはお問い合わせください
    requiredFields: すべてのフィールドは必須です
    submissionReceived: お申し込みを受け付けました！
  premium_features:
    header: 'GitLab Premiumに含まれるもの'
    button:
      text: Premiumの詳細はこちら
      url: '/pricing/premium/'
      data_ga_location: premium roi results
      data_ga_name: learn more about premium
      listHeading: "GitLab Premiumに含まれるもの"
      freeTrial: 無料トライアルを開始
      learnMore: Premiumの詳細はこちら
    list:
      - text: コードレビューの高速化
        information:
          - text: コードレビューでの複数の承認者
          - text: GitLabコードオーナー
          - text: コードレビュー分析
          - text: マージリクエストのレビュー
          - text: コード品質レポート
          - text: マージ結果パイプライン
          - text: Review Appsへのコメント投稿
      - text: 高度なCI/CD
        information:
          - text: CI/CDパイプラインダッシュボード
          - text: マージトレイン
          - text: マルチプロジェクトパイプライングラフ
          - text: 外部リポジトリのCI/CD
          - text: GitOpsデプロイ管理
          - text: 環境ダッシュボード
          - text: グループファイルテンプレート
          - text: 堅牢なデプロイとロールバックの組み合わせ
      - text: エンタープライズアジャイルデリバリー
        information:
          - text: ロードマップ
          - text: 複数人によるイシューの担当
          - text: 単一レベルのエピック
          - text: イシューボードマイルストーンリスト
          - text: 範囲指定したラベル
          - text: イシューのエピックへのプロモート
          - text: 複数のグループイシューボード
          - text: イシューの依存関係
      - text: リリース管理
        information:
          - text: コードレビューの承認ルール
          - text: マージリクエスト承認が必要
          - text: マージリクエスト依存関係
          - text: プッシュルール
          - text: プッシュとマージのアクセスを制限
          - text: 保護環境
          - text: プロジェクトメンバーシップのグループへのロック
          - text: ジオロケーション対応DNS
      - text: Self-Managedの信頼性
        information:
          - text: 障害復旧
          - text: メンテナンスモード
          - text: GitLab Geoによる分散クローン作成
          - text: GitalyによるフォールトトレラントのGitストレージ
          - text: 拡張アーキテクチャのサポート
          - text: コンテナレジストリのジオレプリケーション
          - text: PostgreSQLデータベースのロードバランシング
          - text: ログ転送
      - text: 毎月10,000分のコンピューティング時間
      - text: サポート
  ultimate_features:
    header: 'GitLab Ultimateに含まれるもの'
    button:
      text: Ultimateの詳細はこちら
      url: '/pricing/ultimate/'
      data_ga_location: premium roi results
      data_ga_name: learn more about ultimate
    list:
      - text: 高度なセキュリティテスト
        information:
          - text: 動的アプリケーションセキュリティテスト
          - text: セキュリティダッシュボード
          - text: 脆弱性管理
          - text: コンテナのスキャン
          - text: 依存関係スキャン
          - text: 脆弱性レポート
          - text: APIファジングテスト
          - text: 脆弱性データベース
      - text: 脆弱性管理
        information:
          - text: セキュリティ承認
          - text: セキュリティポリシー
      - text: コンプライアンスパイプライン
        information:
          - text: 監査イベントレポート
          - text: 監査イベントのインターフェース
          - text: ライセンスのコンプライアンス
          - text: コンプライアンスレポート
          - text: 品質管理
          - text: 要求事項管理
          - text: コードのマージ前にJiraのイシューを必要とする
          - text: カスタムコンプライアンスフレームワーク
      - text: ポートフォリオ管理
        information:
          - text: エピックボード
          - text: マルチレベルのエピック
          - text: イシューとエピックのヘルスレポート作成
          - text: 計画の階層
          - text: ポートフォリオ単位のロードマップ
          - text: エピックの一括編集
      - text: バリューストリーム管理
        information:
          - text: インサイト
          - text: 4つのDORA指標 - デプロイ頻度
          - text: 4つのDORA指標 - 変更のリードタイム
      - text: 毎月50,000分のコンピューティング時間
      - text: サポート
      - text: 無料のゲストユーザー
  time_card:
    img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
    text: HackerOneが、GitLabのインテグレーション型セキュリティへの切り替えによってデプロイ速度を5倍も高めた方法をご紹介します
    cta:
      text: お客様事例を読む
      url: /customers/hackerone/
      data_ga_name: HackerOne case study
      data_ga_location: calculator body with time calculator selected
  cards:
  - icon:
      name: money
      alt: お金のアイコン
      variant: marketing
    title: GitLabの料金プランの詳細はこちら
    link_text: 価格ページへ移動
    link_url: /pricing/
    data_ga_name: learn more about gitlab pricing plans
    data_ga_location: roi calculator
  - icon:
      name: cloud-thin
      alt: シンクラウドアイコン
      variant: marketing
    title: クラウドマーケットプレイスからGitLabを購入する
    link_text: セールスチームへのお問い合わせ
    link_url: /sales/
    data_ga_name: purchase gitlab through cloud marketplaces
    data_ga_location: roi calculator