title: デジタルエイジのアプリケーションセキュリティ
og_title: デジタル時代のアプリケーションセキュリティ
description: >-
  組織は、アタックサーフェス（攻撃対象領域）の拡大や、AIに起因する不確実性に直面しています。このレポートでは、世界各地のDevSecOpsの専門家5,000名以上を対象に実施した調査の結果をご紹介します。
twitter_description: >-
  組織は今、拡大し続けるアタックサーフェスとAIに関する不確実の問題に直面しています。このレポートでは、世界各地のDevSecOpsの専門家5,000名以上を対象に実施した調査の結果をご紹介します。
og_description: >-
  組織は今、拡大し続けるアタックサーフェスとAIに関する不確実の問題に直面しています。このレポートでは、世界各地のDevSecOpsの専門家5,000名以上を対象に実施した調査の結果をご紹介します。
og_image: /nuxt-images/developer-survey/2024/2024-devops-survey-security-compliance-meta.png
twitter_image: /nuxt-images/developer-survey/2024/2024-devops-survey-security-compliance-meta.png
form:
    tag: 無料
    header: レポート全文を読むにはご登録ください
    text: |
      世界39か国5,000人以上のDevSecOpsの関係者を対象に調査し、判明した興味深い情報が満載のレポート全文を無料でご覧いただけます。

      **レポートの内容**

      - アプリケーション開発ブーム
      - AIが標準となった今、セキュリティに何が求められるのか？
      - スピードとセキュリティのバランスの維持
    formId: 1002 # Localized versions use a separate form ID
    formDataLayer: resources
    submitted:
      tag: 送信が完了しました
      text: 2024年度セキュリティとコンプライアンスに関するグローバルDevSecOpsレポートをまもなくメールでお届けします。
      img_src: '/nuxt-images/developer-survey/2024/security-hero.png'
hero:
  year: 2024年グローバルDevSecOpsレポート
  title: デジタルエイジのアプリケーションセキュリティ
  img_src: '/nuxt-images/developer-survey/2024/security-hero.png'
  mobile_img_src: '/nuxt-images/developer-survey/2024/sec-com-mobile-hero-img.svg'
intro:
  survey_header:
    text: >-
      ソフトウェア開発が急成長する中で、組織はアタックサーフェス（攻撃対象領域）の拡大や、AIに起因する不確実性に直面しています。このレポートでは、世界各地のDevSecOpsの専門家5,000名以上を対象に実施した調査の結果をご紹介します。
    img_variant: 1
  summary:
    title: レポートの要約
    text: |
      本レポートは、Omdia社とGitLabが2024年4月に実施した調査の結果を分析したものです。この調査では、世界中の5,000人以上のソフトウェア開発担当者、セキュリティ担当者、オペレーション担当者に対して、DevSecOpsの方針とアプローチに関する組織の立場と導入状況について尋ねました。

      今年の調査では、セキュリティ上の脆弱性に関連する大きな混乱が生じていることが明らかになりました。組織はソフトウェアを迅速に提供しなければならないというプレッシャーをこれまで以上に感じており、そのニーズに応えるために人工知能（AI）やオープンソースライブラリの活用に取り組んでいます。しかし、それがアタックサーフェス（攻撃対象領域）を拡大し、AIツールのセキュリティやプライバシーに関する新たな懸念を引き起こしています。一方で、組織が戦略的なアプリケーションセキュリティプログラムを確立することで、セキュリティを犠牲にすることなくソフトウェアデリバリーを高速化できることを実感する結果でもありました。
  highlights:
    title: ハイライト
    type: barGraph
    buttons:
      previous: 前へ
      next: 次へ
    gaLocation: summary
    blocks:
      - text: >-
          <div class="stat orange"><span class="number">66</span>%</div> の回答者が、組織が昨年の2倍またはそれ以上の速度でソフトウェアをリリースしていると回答
        dataGaName: デリバリー速度
      - text: >-
          <div class="stat teal"><span class="number">67</span>%</div> のデベロッパーが作業中のコードの4分の1以上がオープンソースライブラリのものであると回答している一方、現在ソフトウェアコンポーネントの構成要素を文書化するためにソフトウェア部品表（SBOM）を使用している組織はわずか5分の1に留まる
        dataGaName: オープンソースライブラリ
      - text: >-
          <div class="stat red"><span class="number">55</span>%</div> の回答者が、ソフトウェア開発ライフサイクルへのAIの導入にはリスクが伴い、プライバシーとデータセキュリティが最大の懸念事項であると回答
        dataGaName: AIの懸念
      - text: >-
          <div class="stat purple"><span class="number">34</span>%</div> の回答者が、組織が動的アプリケーションセキュリティテスト（DAST）を導入していると回答（2023年から8%増加）
        dataGaName: セキュリティテスト
