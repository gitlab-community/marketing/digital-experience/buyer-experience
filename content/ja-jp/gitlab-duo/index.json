{
  "metadata": {
    "title": "GitLab Duo",
    "description": "ワークフローを強化する一連のAI機能",
    "og:url": "https://about.gitlab.com/gitlab-duo/"
  },
  "heroCentered": {
    "button": {
      "href": "/solutions/gitlab-duo-pro/sales/",
      "text": "利用を開始する",
      "dataGaName": "get started",
      "dataGaLocation": "hero",
      "variant": "primary"
    },
    "description": "ソフトウェア開発ライフサイクル全体にわたってAIを使用し、より安全なソフトウェアをスピーディに出荷",
    "banner": {
        "href": "/solutions/gitlab-duo-pro/sales/",
        "show": true,
        "text": "GitLab Duo Enterpriseが利用可能になりました。ソフトウェア開発ライフサイクル全体にわたってAIを使用し、より安全なソフトウェアをスピーディに出荷できます。",
        "data_ga_name": "gitlab duo pill",
        "data_ga_location": "hero"
    },
    "video": {
      "video_url":"https://player.vimeo.com/video/945979565?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479",
      "mp4_url":"/nuxt-images/gitlab-duo/hero.mp4",
      "webm_url":"/nuxt-images/gitlab-duo/hero.webm"
    }
  },
  "sidenav": {
    "hyperlinks": [
      {
        "title":"メリット",
        "href":"#benefits"
      },
      {
        "title":"機能",
        "href":"#features"
      },
      {
        "title":"価格設定",
        "href":"#pricing"
      },
      {
        "title":"よくある質問",
        "href":"#faq"
      },
      {
        "title":"リソース",
        "href":"#resources"
      }
    ]
  },
  "why": {
    "header": "GitLab Duoが選ばれる理由",
    "card": [
      {
        "title": "市場投入までの時間を短縮",
        "iconName":"speed-alt-2",
        "description":"プランニングとコーディングから、セキュリティ保護とデプロイまで、GitLab Duoはワークフローのすべての段階でデベロッパーをサポートする唯一のAIソリューションです。"
      },
      {
        "title":"プライバシー優先AI",
        "iconName":"ai-vulnerability-resolution",
        "description":"GitLab Duoでは、AIを活用した機能を利用できるユーザー、プロジェクト、グループを自由に設定できます。また、貴社のプロプライエタリコードやデータがAIモデルのトレーニングに使用されることはありません。"
      },
      {
        "title":"デベロッパーエクスペリエンスの向上",
        "iconName":"values-three",
        "description":"コードの理解からセキュリティ上の脆弱性の修正まで、ワークフロー全体にわたってユースケースごとに最適なAIモデルが統合された、単一のプラットフォームをデベロッパーに提供しましょう。"
      },
      {
        "title":"透明性の高いAIへの取り組み",
        "iconName":"eye",
        "description":"組織やチームがAIを信頼できるようになるには、透明性が不可欠です。GitLabの[AI Transparency Center](https://about.gitlab.com/ai-transparency-center/){data-ga-name=\"ai transparency center\" data-ga-location=\"body\"}では、GitLabがAI搭載機能においてどのように倫理性と透明性を確保しているかについて詳しく説明しています。"
      }
    ],
    "highlight": {
      "eyebrow": "今後の取り組み",
      "header": "GitLab Duo Workflowのご紹介",
      "text": "AI主導の次世代の開発環境。ワークフローはインテリジェントで常時稼働するエージェントです。プロジェクトを自律的にモニタリング、最適化、保護し、デベロッパーがイノベーションに集中できるようにします。",
      "video_src": "https://player.vimeo.com/video/967982166",
      "video_thumbnail": "https://images.ctfassets.net/xz1dnu24egyd/1i5ZSm9ITugyFYHjEBsiar/30f35a754636bdf5a7595b6a91586faa/Image.jpg",
      "cta": {
        "text": "ブログ記事を読む",
        "url": "/blog/2024/06/27/meet-gitlab-duo-workflow-the-future-of-ai-driven-development/",
        "dataGaName": "gitlab duo workflow blog post",
        "dataGaLocation": "body"
      }

    }
  },
  "benefits": {
    "fields": {
      "header": "ソフトウェア開発ライフサイクル全体にわたるAI搭載機能",
      "description": "プランニングとコーディングから、セキュリティ保護とデプロイまで、Duoはワークフローのすべての段階でデベロッパーをサポートする唯一のAIソリューションです。",
      "card": [
      {
        "title": "プライバシー優先AI",
        "iconName": "ai-vulnerability-resolution",
        "description": "GitLab Duoでは、AIを活用した機能を利用できるユーザー、プロジェクト、グループを自由に設定できます。また、貴社のプロプライエタリコードやデータがAIモデルのトレーニングに使用されることはありません。"
      },
      {
        "title": "デベロッパーエクスペリエンスの向上",
        "iconName": "values-three",
        "description": "コードの理解からセキュリティ上の脆弱性の修正まで、ワークフロー全体にわたってユースケースごとに最適なAIモデルが統合された、単一のプラットフォームをデベロッパーに提供しましょう。"
      },
      {
        "title": "透明性の高いAIへの取り組み",
        "iconName": "eye",
        "description": "組織やチームがAIを信頼できるようになるには、透明性が不可欠です。GitLabの[AI Transparency Center](/ai-transparency-center/){data-ga-name=\"ai transparency center\" data-ga-location=\"body\"}では、GitLabがAI搭載機能においてどのように倫理性と透明性を確保しているかについて詳しく説明しています。"
      }
    ]
    }
  },
  "features": {
    "sections": [
      {
        "video" : {
          "title":"Vimeo動画2",
          "internalName":"features-video-1",
          "thumbnail":"/nuxt-images/gitlab-duo/code-suggestions.png",
          "mp4_url":"/nuxt-images/gitlab-duo/code-suggestions.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/code-suggestions.webm"
        },
        "header": "スマートコードアシスタンスで生産性を向上",
        "text": "20以上の言語に対応するAIによる提案機能は希望のIDEで利用でき、安全なコードをより迅速に書くことができます。日常的に発生するタスクを自動化し、開発サイクルを短縮します。",
        "pill": "コード",
        "icon": "ai-code-suggestions",
        "left": true
      },
      {
        "video": {
          "title":"Vimeo動画2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/chat.png",
          "mp4_url":"/nuxt-images/gitlab-duo/chat.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/chat.webm"
        },
        "header": "開発期間を通してあなたをサポートするAIコンパニオン",
        "text": "ソフトウェア開発ライフサイクル全体にわたってリアルタイムのガイダンスを提供します。テストの生成、コードの説明、効率的なリファクタリングに加え、IDEまたはWebインターフェースでの直接チャットも可能です。",
        "pill": "検索",
        "icon": "ai-gitlab-chat",
        "left": false
      },
      {
        "video": {
          "title":"Vimeo動画2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/root-cause-analysis.png",
          "mp4_url":"/nuxt-images/gitlab-duo/root-cause-analysis.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/root-cause-analysis.webm"
        },
        "header": "CI/CDパイプラインの問題を迅速に解決",
        "text": "CI/CDジョブの失敗に対するAIアシストの根本原因分析により、問題解決の時間を節約します。提案された修正を活用して重要なタスクに集中しましょう。",
        "pill": "トラブルシューティング",
        "icon": "ai-root-cause-analysis",
        "left": true
      },
      {
        "video": {
          "title":"Vimeo動画2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/vulnerability.png",
          "mp4_url":"/nuxt-images/gitlab-duo/vulnerability.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/vulnerability.webm"
        },
        "header": "AI搭載のセキュリティでコードをパワーアップ",
        "text": "脆弱性をより効率的に把握して修正します。詳細な説明と自動生成されたマージリクエストを取得し、セキュリティリスクを軽減します。",
        "pill": "セキュア",
        "icon": "ai-vulnerability-bug",
        "left": false
      },
      {
        "video": {
          "title":"Vimeo動画2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/ai.png",
          "mp4_url":"/nuxt-images/gitlab-duo/ai.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/ai.webm"
        },
        "header": "AI投資のROI測定",
        "text": "AIの有効性をリアルタイムで追跡します。 サイクルタイムとデプロイ頻度の具体的な改善を確認し、投資収益率を定量化します。",
        "pill": "計測",
        "icon": "ai-value-stream-forecast",
        "left": true
      }
    ]
  },
  "promo":{
    "header": "『2024 Gartner® Magic Quadrant™』のAIコードアシスタント部門でGitLabがリーダーの1社として評価されました",
    "button": {
      "href": "/gartner-mq-ai-code-assistants/",
      "text": "レポートを読む",
      "dataGaName": "Read the report",
      "dataGaLocation": "body"
    }
  },
  "categories": {
    "header": "ソフトウェア開発ライフサイクル全体にわたるAIを搭載した機能",
    "section": [
      {
        "name":"開発用の機能",
        "data": "developing",
        "icon":"package-alt-2"
      },
      {
        "name":"アプリケーションセキュリティ強化",
        "data": "securing",
        "icon":"secure-alt-2"
      },
      {
        "name":"共同作業の促進",
        "data": "facilitating",
        "icon":"plan-alt-2"
      },
      {
        "name":"高度なトラブルシューティング",
        "data": "troubleshooting",
        "icon":"monitor-alt-2"
      }
    ],
    "card": [
      {
        "icon":"ai-gitlab-chat",
        "header":"チャット",
        "description":"対話形式でテキストとコードを処理して生成します。イシュー、エピック、コード、GitLabドキュメントの大量のテキストの中から有用な情報をすばやく特定します。",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo_chat/index.html",
        "category":"developing",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-root-cause-analysis",
        "header":"コードの説明",
        "description":"自然言語での説明により、コードをわかりやすく理解できます。",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/index.html#code-explanation",
        "category":"developing",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-git-suggestions",
        "header":"コード提案",
        "description":"毎日のコーディング作業を処理し、デベロッパーが安全なコードをより効率的に作成してサイクルタイムを短縮できるようにします。",
        "url":"https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html",
        "category":"developing",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-git-suggestions",
        "header":"コマンドラインインターフェース（CLI）用のGitLab Duo",
        "description":"必要なタイミングと場所でGitコマンドを検出またはリコールします。",
        "url":"https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/index.html#gitlab-duo-commands",
        "category":"developing",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-tests-in-mr",
        "header":"テスト生成",
        "description":"繰り返し行う必要のある作業を自動化し、バグを早期に発見できます。",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/#test-generation",
        "category":"developing",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-vulnerability-bug",
        "header":"脆弱性の説明",
        "description":"脆弱性をより効率的に解決し、スキルを向上できるようにすることで、より安全なコードを書けるようにします。",
        "url":"https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#explaining-a-vulnerability",
        "category":"securing",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-vulnerability-resolution",
        "header":"脆弱性の修正",
        "description":"脆弱性を軽減するために必要な変更が含まれるマージリクエストを生成します。",
        "url":"https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#vulnerability-resolution",
        "category":"securing",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-value-stream-forecast",
        "header":"AIインパクトダッシュボード",
        "description":"サイクルタイムとデプロイ頻度のリアルタイムの改善を確認",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/#ai-impact-dashboard",
        "category":"facilitating",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-issue",
        "header":"ディスカッションサマリー",
        "description":"長いやり取りの概要をすばやく確認できるよう助け、全員に情報が伝達されるようにします。",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary",
        "category":"facilitating",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-summarize-mr-review",
        "header":"コードレビューのサマリー",
        "description":"作成者とレビュアー間のマージリクエストのハンドオフをサポートし、レビュアーが提案を効率的に理解できるようにします。",
        "url":"https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html#summarize-a-code-review",
        "category":"facilitating",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-merge-request",
        "header":"マージリクエストのサマリー",
        "description":"マージリクエストの変更による影響を効率的に伝達します。",
        "url":"https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html",
        "category":"facilitating",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-generate-issue-description",
        "header":"イシューの説明の生成",
        "description":"イシューの説明を生成します。",
        "url":"https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html#summarize-a-code-review",
        "category":"facilitating",
        "text": "詳細はこちら"
      },
      {
        "icon":"ai-root-cause-analysis",
        "header":"根本原因分析",
        "description":"パイプラインの障害と、失敗したCI/CDのビルドの根本原因を特定するお手伝いをします。",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/index.html#root-cause-analysis",
        "category":"troubleshooting",
        "text": "詳細はこちら"
      }
    ],
    "disclaimer": "Ultimateライセンスでは、[GitLabテスト規約](https://handbook.gitlab.com/handbook/legal/testing-agreement/)の対象となる実験またはベータ版として記載されている特定の機能をテストできます。AI機能のベータ版から一般公開への移行後は、PremiumまたはUltimateライセンスではGitLab Duoエンタープライズのアドオンを購入することでGitLab Duo Pro機能を引き続きご利用いただけます。"
  },
  "faq": {
    "header":"よくある質問",
    "texts":{
      "show":"すべて表示",
      "hide":"すべて非表示"
    },
    "questions": [
      {
        "question":"コード提案でサポートされているプログラミング言語は？",
        "answer":"コード提案で最適な結果を得るには、[Google Vertex AI Codey API](https://cloud.google.com/vertex-ai/generative-ai/docs/code/code-models-overview#supported_coding_languages)が直接サポートする言語であるC++、C#、Go、Google SQL、Java、JavaScript、Kotlin、PHP、Python、Ruby、Rust、Scala、Swift、TypeScriptのいずれかを使用することをおすすめします。"
      },
      {
        "question":"GitLab Duoはどの言語モデルを使用していますか？",
        "answer":"GitLabの抽象化レイヤーにより、適切なユースケースに適したモデルを用いてAI機能が強化されています。GitLab Duoの各機能に使用されている言語モデルについては、[こちら](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html)からご覧ください。"
      },
      {
        "question":"私のコードはAIモデルのトレーニングに使用されますか？",
        "answer":"GitLabが、プライベート（非公開）データを使用して生成AIモデルをトレーニングすることはありません。また、当社が協力しているベンダーもプライベートデータを使用してモデルをトレーニングすることはありません。[詳細はこちら](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html#training-data){data-ga-name=\"learn more about ai training models\" data-ga-location=\"faq\"}。"
      },
      {
        "question":"GitLab Duoはオープンコアですか？",
        "answer":"はい。GitLabはユーザーとともに構築できるオープンコアビジネスモデルを採用しており、誰でもGitLabに新機能をコントリビュートすることができます。"
      },
      {
        "question":"GitLab Duoにより生成された出力結果を使用するにはどうすればよいですか？",
        "answer":"GitLab Duoによって生成された出力結果は、ご自身の判断でご利用いただけます。GitLab Duoによって生成された出力結果を使用したことでサードパーティからの申し立てが発生した場合は、GitLabが介入して当該ユーザーを保護します。"
      },
      {
        "question":"GitLab Duo Pro機能にアクセスできるユーザーを設定するにはどうすればよいですか？",
        "answer":"組織コントロール機能を使用すると、管理者は新しい管理者用UIを介して特定のユーザーにライセンスシートを割り当てることができます。コード提案は、IDE拡張設定から直接有効または無効にすることができます。ChatなどのExperiment/Beta版のAI機能はトップレベルグループ設定から変更できます。[詳細はこちら](https://docs.gitlab.com/ee/user/gitlab_duo/index.html#experimental-features){data-ga-name=\"learn more about experiemental ai features\" data-ga-location=\"faq\"}。"
      },
      {
        "question":"GitLab Duo Proは接続されていないGitLabインスタンス、または接続が制限されているGitLabインスタンスで使用できますか？",
        "answer":"いいえ。Self-Managedのユーザーがコード提案とチャットにアクセスするためには、インターネット接続とクラウドライセンスが必要となります。"
      },
      {
        "question":"（ソースファイル内の）コメントにプロンプトを入力する際、英語以外の言語を使用できますか？",
        "answer":"GitLabでは意図的に使用する自然言語の種類を制限することはありませんが、通常英語を使用することで最適な結果が得られます。英語以外の言語でプロンプトを入力した実験によると、その言語に合わせたコード提案が提供されています（特定の言語構文とキーワードを尊重）。"
      },
      {
        "question":"コード提案ではどのようにIPを保護し、データプライバシーを確保していますか？",
        "answer":"コード提案のデータプライバシーとIP保護の詳細については、[こちら](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html#data-privacy){data-ga-name=\"data privacy and ip protection\" data-ga-location=\"faq\"}をご覧ください。無料期間中およびChatのBeta版提供中に、コード提案とChatをご利用いただく場合、[GitLabテスト契約](https://about.gitlab.com/handbook/legal/testing-agreement/){data-ga-name=\"gitlab testing agreement\" data-ga-location=\"faq\"}が適用されます。[コード提案の使用時のデータ使用についてはこちら](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html){data-ga-name=\"coe suggestions data usage\" data-ga-location=\"faq\"}を、[Duo Chatデータの使用についてはこちら](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html){data-ga-name=\"chat data usage\" data-ga-location=\"faq\"}をご覧ください。"
      }
    ]
  },
  "pricing": {
    "header":"価格"
  },
  "resources": {
    "id":"resources",
    "align":"left",
    "grouped": "true",
    "column_size": 4,
    "title":"GitLab Duoの詳細はこちら",
    "header_cta_text":"すべてのリソースを表示",
    "header_cta_href":"/resources/",
    "header_cta_ga_name":"View all resources",
    "header_cta_ga_location":"body",
    "cards":[
      {
        "event_type":"動画",
        "header":"GitLab Duoのご紹介",
        "link_text":"今すぐ視聴する",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/wS2NkIeyOYrkiqadM1Jkd/eeabed3c2d08fa50047c70989d70b14d/Meet_GitLab_Duo_Thumbnail.png",
        "href":"https://player.vimeo.com/video/855805049?title=0&byline=0&portrait=0&badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"meet gitlab duo"
      },
      {
        "event_type":"動画",
        "header":"コード提案",
        "link_text":"今すぐ視聴する",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/7paG5yX4I2FErEKb3022EN/cd480897c76cd1c5dd2a51426c8e0013/Code_Suggestions_Thumbnail.png",
        "href":"https://player.vimeo.com/video/894621401?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479",
        "data_ga_name":"code suggestions"
      },
      {
        "event_type":"動画",
        "header":"チャット",
        "link_text":"今すぐ視聴する",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/4y29zj5spjIUh2DaGdxk5q/f7d972269fd3b16501ef4d3b013f6c09/Chat_Thumbnail.png",
        "href":"https://player.vimeo.com/video/927753737?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"chat"
      },
      {
        "event_type":"動画",
        "header":"コードレビューサマリー",
        "link_text":"今すぐ視聴する",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/kfMhe63t5l6jHIxmmlq5z/a68a12a38b7faf7b37c821c5e3204ec1/Code_Review_Summary_Thumbnail.png",
        "href":"https://player.vimeo.com/video/929891003?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"code review summary"
      },
      {
        "event_type":"動画",
        "header":"ディスカッションサマリー",
        "link_text":"今すぐ視聴する",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/4gvRwDXZ3zDwKKtCHiYhy6/c26a91ddfa12817c2bcee4e6a991260f/Discussion_Summary_Thumbnail.jpg",
        "href":"https://player.vimeo.com/video/928501915?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"discussion summary"
      },
      {
        "event_type":"動画",
        "header":"脆弱性の説明",
        "link_text":"今すぐ視聴する",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/64mYwKfirCjkHiapVtZlN4/830c78fa1902ed0a61ffdc9da822145c/Vulnerability_Explanation_Thumbnail.jpg",
        "href":"https://player.vimeo.com/video/930066123?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"vulernability explanation"
      },
      {
        "event_type":"動画",
        "header":"コードの説明",
        "link_text":"今すぐ視聴する",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/1N6clW3Mq7Fu40uIEt2iav/6b487d1916276934b3641a456211835c/Code_Explanation_Thumbnail.jpg",
        "href":"https://player.vimeo.com/video/930066090?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"code explanation"
      },
      {
        "event_type":"動画",
        "header":"レビュアーの推奨",
        "link_text":"今すぐ視聴",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/5L9AwAebsBO8eIQxSH96nz/a388c454b847bc134082a352bea32082/Suggested_Reviewers_Thumbnail.png",
        "href":"https://player.vimeo.com/video/930066108?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"suggested reviewers"
      },
      {
        "icon":{
          "name":"blog",
          "variant":"marketing"
        },
        "event_type":"ブログ",
        "header":"AI搭載のGitLab Duoで脆弱性を把握し解決",
        "link_text":"詳細はこちら",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/1bIUeH2qhLxfgPJIZbBzD9/c916adff20eab529510a0e4eea5d9f4d/GitLab-Duo-Blog-Image-1.png",
        "href":"/blog/2024/02/21/understand-and-resolve-vulnerabilities-with-ai-powered-gitlab-duo/",
        "data_ga_name":"Understand and resolve vulnerabilities with AI-powered GitLab Duo"
      },
      {
        "icon":{
          "name":"blog",
          "variant":"marketing"
        },
        "event_type":"ブログ",
        "header":"デベロッパーの生産性メトリクスに留まらない、AIの有効性を測定",
        "link_text":"詳細はこちら",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/2IjSEZ7qEEcaDf2PlvJKRp/bb97ac8129ca9404b74ea0c13b5ed437/GitLab-Duo-Blog-Image-2.png",
        "href":"/blog/2024/02/20/measuring-ai-effectiveness-beyond-developer-productivity-metrics/",
        "data_ga_name":"Measuring AI effectiveness beyond developer productivity metrics"
      },
      {
        "icon":{
          "name":"blog",
          "variant":"marketing"
        },
        "event_type":"ブログ",
        "header":"AIアシストツールに関する新レポートがDevSecOpsにおけるリスクの高まりを示唆",
        "link_text":"詳細はこちら",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/bH5lKxYxiDkZ4LULySnmO/ab7437e32e77d6b2f39bd70184edf913/GitLab-Duo-Blog-Image-3.png",
        "href":"/blog/2024/02/14/new-report-on-ai-assisted-tools-points-to-rising-stakes-for-devsecops/",
        "data_ga_name":"New report on AI-assisted tools points to rising stakes for DevSecOps"
      }
    ]
  }
}
