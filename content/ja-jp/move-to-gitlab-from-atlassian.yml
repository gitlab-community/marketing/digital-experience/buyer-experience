---
  title: "AtlassianからGitLabへの移行"
  description: Atlassianは、Bitbucket、Jira、Bamboo、Confluenceを含むすべてのサーバー製品のサポートを2024年2月に終了します。この変更が原因でチームに適さないツールの導入を余儀なくされることがないよう、対策を取りましょう。ここでは、GitLabへの移行メリットをご紹介します。
  side_navigation_links:
    - title: Bitbucket
      href: '#bitbucket'
      data_ga_name: bitbucket
      data_ga_location: navigation
    - title: Jira
      href: '#jira'
      data_ga_name: jira
      data_ga_location: navigation
    - title: Bamboo
      href: '#bamboo'
      data_ga_name: bamboo
      data_ga_location: navigation
    - title: Confluence
      href: '#confluence'
      data_ga_name: confluence
      data_ga_location: navigation
    - title: 価格設定
      href: '#pricing'
      data_ga_name: pricing
      data_ga_location: navigation
  side_navigation_text_link:
    text: セールスチームに問い合わせる
    url: '/sales/'
    data_ga_name: sales
    data_ga_location: navigation
  solutions_hero:
    title: AtlassianからGitLabへの移行
    badge:
      text: GitLab Enterprise Agile Planningアドオンが利用可能に
      url: "#agile-add-on"
      icon: gl-arrow-right
      data_ga_name: agile delivery add-on
      data_ga_location: hero
    isDark: true
    subtitle: Atlassianは、[Bitbucket](#bitbucket)、[Jira](#jira)、[Bamboo](#bamboo)、[Confluence](#confluence)を含むすべてのサーバー製品のサポートを**2024年2月**に終了します。サーバーのサポート終了が原因でチームに適さないツールの導入を余儀なくされることがないよう、対策を取りましょう。ここでは、GitLabへの移行メリットをご紹介します。
    primary_btn:
      text: お問い合わせ
      url: /sales/
      data_ga_name: sales
      data_ga_location: header
    image:
      image_url: "https://images.ctfassets.net/xz1dnu24egyd/26SlAGBx32ZPYt2I4Wcd9A/95e7a4353d19da0b6a3eaff250e8205c/atlassian-eol-landing.svg"
      alt: "devops lifecycle image"
      bordered: true
  content_card:
    title: 'BitbucketからGitLabへの移行'
    description: |
      **コラボレーションと加速**


      アセットのバージョン管理、フィードバックループ、および強力なブランチパターンを活用した継続的なリリースにより、デベロッパーは効率的に問題を解決し、価値を提供できます。


      &nbsp;

      **コンプライアンスとセキュリティ**


      チームが信頼できる唯一の情報源に基づいてコード変更をレビュー、トラッキング、そして承認できるようにしましょう。
    link: /
    type: "eBook"
    image: https://images.ctfassets.net/xz1dnu24egyd/GF2chSP6KXImk3djWqJxk/d0119e7396e05f112e77583415405c10/principle-1.png
    icon: open-book
    button_text: 'eBookを読む'
    size: half
    button_type: primary
    image_right: true

  benefit_cards:
    title: GitLabで着想からソフトウェアデリバリーまでを最短で実現
    benefits:
      - title: どこにでも自由にデプロイ
        icon: gitlab-cloud
        description: デプロイ手法とデプロイ先を柔軟に決められます。GitLabは、セルフホストソリューションを必要とするお客様をいつでもサポートいたします。
      - title: ソフトウェア開発を効率化
        icon: collaboration
        description: まとまりのないツールチェーンは廃止しましょう。ソフトウェア開発ライフサイクル全体に対応する単一プラットフォームをチームに導入し、サイクルタイムの短縮、生産性の向上、および開発コストの削減を実現しましょう。
      - title: 自由なペースで移行
        icon: monitor-gitlab
        description: 一度にすべてのツールやサービスを廃止する準備ができていないというお客様も、ご安心ください。GitLabは広範なインテグレーションにも対応しているため、組織のペースに合わせて移行を進められます。

  copy_benefits_bitbucket:
    title: BitbucketからGitLabへの移行
    subtitle: すべての人のためのバージョン管理
    background: true
    benefits:
      - title: 強力なバージョン管理
        icon: idea-collaboration
        description: アセットのバージョン管理、フィードバックループ、および強力なブランチパターンを活用した継続的なリリースにより、デベロッパーはソフトウェアを効率的にテストおよびデプロイできます。
      - title: ビルトインのセキュリティとコンプライアンス
        icon: devsecops
        description: チームが信頼できる唯一の情報源に基づいてコード変更をレビュー、トラッキング、および承認できるようにしましょう。
        button_text: 詳しく見る
        button_url: /solutions/source-code-management/
        data_ga_name: bitbucket
        data_ga_location: body

  copy_benefits_jira:
    title: JiraからGitLabへの移行
    subtitle: プロジェクト、プログラム、および製品向けの統合されたアジャイルサポート
    benefits:
      - title: アジャイル計画
        icon: agile
        description: アジャイルチーム機能やポートフォリオ管理機能を標準搭載し、ソフトウェアのビルド、テスト、およびデプロイが可能な単一ソフトウェアデリバリープラットフォームを活用して、ビジネス価値をより迅速に提供しましょう。
        button_text: 詳しく見る
        button_url: /solutions/agile-delivery/
        data_ga_name: agile delivery
        data_ga_location: jira body
      - title: バリューストリーム管理
        icon: visibility
        description: バリューストリームの可視化、ソフトウェア開発ライフサイクル全体に点在するボトルネックの排除、生産性向上を目的とした優先順位の修正、ビジネス価値の促進を可能にしましょう。
        button_text: 詳しく見る
        button_url: /solutions/value-stream-management/
        data_ga_name: value stream management
        data_ga_location: jira body

  copy_benefits_confluence:
    title: ConfluenceからGitLabへの移行
    subtitle: コミュニケーションのサイロ化を解消するナレッジマネジメント
    benefits:
      - title: カスタマイズ可能なWikiページ
        icon: web-alt
        description: 組織が持つ知識やドキュメントをGitLabに保管して、異なるチームや部門から簡単にアクセスできるようにしましょう。
        button_text: 詳しく見る
        button_url: https://docs.gitlab.com/ee/user/project/wiki/
        data_ga_name: confluence
        data_ga_location: body

  copy_benefits_bamboo:
    title: BambooからGitLabへの移行
    background: true
    subtitle: 柔軟なCI/CDによって拡張性に優れたオンデマンドのソフトウェアデリバリーを実現
    benefits:
      - title: シンプルさと拡張性を実現するために構築されたパイプライン
        icon: monitor-pipeline
        description: ビルトインのテンプレートを使用して取り掛かりの手間を抑え、親子パイプラインやマージトレインを活用してスケールアップしましょう。
      - title: ビルトインのセキュリティとコンプライアンス
        icon: shield-check-large-light
        description: コンプライアンスパイプラインから統合セキュリティスキャンまで、すべてを1か所で確認できるGitLabなら、優れた可視性と制御性を維持できます。
        button_text: 詳しく見る
        button_url: /solutions/security-compliance/
        data_ga_name: security compliance
        data_ga_location: bamboo body
      - title: コンテキスト内テスト
        icon: monitor-test-2
        description: コードパフォーマンスからセキュリティまで、すべてを自動でテストし、テスト結果をそのコンテキスト内でレビューおよび承認できます。
        button_text: 詳しく見る
        button_url: /solutions/continuous-integration/
        data_ga_name: bamboo
        data_ga_location: body

  resources:
    col_size: 4
    header: "AtlassianからGitLabへの移行に関するリソース"
    description: Atlassianのサーバーサポート終了およびGitLabへの移行方法についての詳細をご確認いただけます。
    case_studies:
      - header: "あらゆる職務に対応可能なGitLab Enterprise Agile Planningアドオン"
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/
          text: 詳しく見る
          data_ga_name: enterprise agile planning resource
          data_ga_location: body
      - header: GitLab Self-Managedが公共機関にとって最適なパートナーである理由
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/3wDRL8ssQu72bNfsvN1Sds/1b48bc3c8cb63eca3f47cf40b99a6308/gitlabflatlogomap.jpg
          alt: ""
        link:
          href: /blog/2023/12/13/why-gitlab-self-managed-is-the-perfect-partner-for-the-public-sector/
          text: 詳しく見る
          data_ga_name: perfect partner resource
          data_ga_location: body
      - header: "Atlassianサーバーの終了：まとまりのないツールチェーンからDevSecOpsプラットフォームへの移行"
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/HE9XYEbagTAiFYXohe8p9/47f0ab25fda5c00db236e7bb91790492/value-stream.png
          alt: ""
        link:
          href: /blog/2023/09/26/atlassian-server-ending-move-to-a-single-devsecops-platform/
          text: 詳しく見る
          data_ga_name: atlassian resource
          data_ga_location: body
      - header: GitLabでアジャイル計画ツールの設定を簡素化すべき5つの理由
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/10/17/five-reasons-to-simplify-agile-planning-tool-configuration-gitlab/
          text: 詳しく見る
          data_ga_name: simplify agile resource
          data_ga_location: body
      - header: JiraからGitLabへの移行を成功させるためのヒント
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/10/24/tips-for-a-successful-jira-to-gitlab-migration/
          text: 詳しく見る
          data_ga_name: Tips for a successful Jira to GitLab migration resource
          data_ga_location: body
      - header: BambooからGitLab CI/CDへの移行方法
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/26eTPBKrWglfy1ILxWSUsD/7230b6d753d78b80abbcfe736449e218/securitylifecycle-light.png
          alt: ""
        link:
          href: /blog/2023/10/26/migrating-from-bamboo-to-gitlab-cicd/
          text: 詳しく見る
          data_ga_name: How to migrate from Bamboo to GitLab CI/CD resource
          data_ga_location: body
      - header: Bitbucket CloudからGitLabにプロジェクトをインポート
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/7veFR11eLgKvggQfNISDgK/d0ecb9098c5b913e22d90790d8cb7391/bitbucket.png
          alt: ""
        link:
          href: https://docs.gitlab.com/ee/user/project/import/bitbucket.html
          text: 詳しく見る
          data_ga_name: bitbucket resource
          data_ga_location: body
      - header: JiraプロジェクトのイシューをGitLabにインポート
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/5mnlgW5gqy5JBvE819dH2q/25521343423e026ada32fbd7daf80b5a/jira.png
          alt: ""
        link:
          href: https://docs.gitlab.com/ee/user/project/import/jira.html
          text: 詳しく見る
          data_ga_name: jira resource
          data_ga_location: body
      - header: CI/CDイノベーションを促進する10億のパイプライン
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/19z3IxRhCpQf37oD2lEOHN/34faaf9fb0fd927efe6c30a10f74c499/securitylifecycle.png
          alt: ""
        link:
          href: /blog/2023/10/04/one-billion-pipelines-cicd/
          text: 詳しく見る
          data_ga_name: pipeline resource
          data_ga_location: body
  tier_block:
    header: 最適なプランを見つけましょう
    cta:
      url: /pricing/
      text: 最適なプランを見つけましょう
      data_ga_name: pricing
      data_ga_location: free tier
      aria_label: 価格設定
    tiers:
      - id: free
        title: Free
        items:
          - 静的アプリケーションセキュリティテスト（SAST）とシークレット検出
          - JSONファイルの調査
        link:
          href: /pricing/
          text: 詳しく見る
          data_ga_name: pricing
          data_ga_location: free tier
          aria_label: Freeプラン
      - id: premium
        title: Premium
        items:
          - 静的アプリケーションセキュリティテスト（SAST）とシークレット検出
          - JSONファイルの調査
          - MRの承認とその他の一般的な制御機能
        link:
          href: /pricing/
          text: 詳しく見る
          data_ga_name: pricing
          data_ga_location: premium tier
          aria_label: Premiumプラン
      - id: ultimate
        title: Ultimate
        items:
          - Premiumの機能に加え、以下の機能が含まれます
          - 包括的なセキュリティスキャナー（SAST、DAST、秘密、Dependency Scanning、コンテナ、IaC、API、クラスターイメージ、ファジングテストなど）
          - MRパイプライン内での実行可能な結果
          - コンプライアンスパイプライン
          - セキュリティとコンプライアンスのダッシュボード
          - その他
        link:
          href: /pricing/
          text: 詳しく見る
          data_ga_name: pricing
          data_ga_location: ultimate tier
          aria_label: Ultimateプラン
        cta:
          href: /free-trial/
          text: 無料トライアルを開始
          data_ga_name: pricing
          data_ga_location: ultimate tier
          aria_label: Ultimateプラン

  pricing_banner:
    id: agile-add-on
    badge_text: New
    headline: GitLab Enterprise Agile Planning
    blurb: GitLab Ultimateのお客様を対象としたアジャイル計画用の追加シート。
    button:
      text: 価格設定についてはお問い合わせください
      url: "/sales/"
      data_ga_name: contact us for pricing
      data_ga_location: body
    footnote: ''
    features:
      header: 'エンタープライズレベルのアジャイル計画ソリューション：'
      list:
        - text: Jiraの置き換え
        - text: ソフトウェア開発ライフサイクルに関わる全ての人が利用できる統一された計画ワークフロー
        - text: 速度と影響を測定するバリューストリーム分析
        - text: 組織全体の可視性を高める経営陣向けダッシュボード
        - text: GitLab Ultimateを利用している企業向けの、スタンドアロンのエンタープライズアジャイルプランニングのサブスクリプション


  recognition:
    heading: DevSecOpsプラットフォームの最先端を切り開くGitLab
    stats:
      - value: 50%以上
        blurb: フォーチュン100
      - value: 3,000万人以上
        blurb: 登録ユーザー
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartner logo
        text: '『2024 Gartner® Magic Quadrant™』のDevOpsプラットフォーム部門でGitLabがリーダーの1社に位置付け'
        link:
          text: レポートを読む
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forrester logo
        text: "『Forrester Wave™: 2023年度第二四半期統合ソフトウエアデリバリープラットフォーム』においてGitLabが唯一のリーダーに認定"
        link:
          text: レポートを読む
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
      - header: 'DevSecOpsカテゴリ全体でGitLabはG2リーダーにランクイン'
        link:
          text: GitLabに関する業界アナリストの評価
          href: /analysts/
          data_ga_name: g2
          data_ga_location: analyst
        badges:
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 Enterprise Leader - 2023年夏
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MomentumLeader_Leader.png
            alt: G2 Momentum Leader - 2023年夏
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MostImplementable_Total.svg
            alt: G2 Most Implementable - 2023年夏
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestResults_Total.png
            alt: G2 Best Results - 2023年夏
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Enterprise_Total.png
            alt: G2 Best Relationship Enterprise - 2023年夏
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Mid-Market_Total.svg
            alt: G2 Best Relationship Mid-Market - 2023年夏
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 Easiest to use - 2023年夏
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestUsability_Total.png
            alt: G2 Best Usability - 2023年夏

  atlassian_solutions:
    title: 数百もの既存のアプリケーションと統合可能なGitLab
    subtitle: 製品チームとエンジニアリングチームが、ツールを切り替えることなく、効果的に連携する方法を紹介します[カスタムソリューションの詳細をご希望の方は、お問い合わせください](/sales/)。
    solutions:
      - title: Jira
        subtitle: 簡単に[Atlassian Jiraと統合](https://docs.gitlab.com/ee/integration/jira/){data-ga-name="jira docs" data-ga-location="body"}
      - title: GitHub
        subtitle: '[GitLab CI/CDとGitHub SCM](https://docs.gitlab.com/ee/user/project/integrations/github.html){data-ga-name="github docs" data-ga-location="body"}をシームレスに統合'
      - title: Jenkins
        subtitle: 適切に管理された[GitLabプラグイン](https://docs.gitlab.com/ee/integration/jenkins.html){data-ga-name="jenkins docs" data-ga-location="body"}
      - title: API
        subtitle: 'すべてのGitLabコンポーネント用の[API](https://docs.gitlab.com/ee/api/integrations.html){data-ga-name="apis" data-ga-location="body"}'
