---
  title: Calculadora de retorno sobre o investimento (ROI)
  description: Confira aqui informações sobre a calculadora de ROI das categorias do GitLab. Descubra mais aqui e confira o valor!
  heading: Quanto custa sua cadeia de ferramentas?
  tabData:
    - tabName: Economize dinheiro
      tabId: cost
    - tabName: Economize tempo
      tabId: time
  cost_steps: 
    - Seu pessoal
    - Gastos atuais
    - Suas economias
  time_steps:
    - Seu pessoal
    - Tempo atual
    - Economia de tempo
  cost_roi:
    people: Quantas pessoas usam e mantêm sua cadeia de ferramentas?
    users: Número de usuários
    maintainers: Número de mantenedores das ferramentas DevSecOps
    nextStep: Continuar para a próxima etapa
    spendPerYear: Aproximadamente, quanto você gasta por ano (em USD) nestes recursos?
    sourceCodeManagement: Gerenciamento de código-fonte
    agileProjectManagement: Gerenciamento ágil de projetos
    continuousIntegration: Integração contínua
    apm: Gerenciamento de portfólio ágil
    continuousDelivery: Entrega contínua
    applicationSecurity: Segurança de aplicações
    registries: Registros
    savings: Confira suas economias
    restart: Reiniciar calculadora
  time_roi:
    people: Quantas pessoas usam e mantêm sua cadeia de ferramentas?
    users: Número de usuários
    maintainers: Número de mantenedores das ferramentas DevSecOps
    nextStep: Continuar para a próxima etapa
    howManyDaysToProduction: Quantos dias são necessários para que uma atualização de aplicação (commit de código) chegue à produção?
    daysToProduction: Número de dias para chegar à produção
    hoursPerWeek: Horas por semana
    developmentConfigurationHours : Quantas horas por semana seus desenvolvedores passam configurando/integrando diferentes ferramentas na sua cadeia de ferramentas DevOps?
    developmentSecurityHours: Quantas horas por semana seus desenvolvedores passam corrigindo vulnerabilidades de segurança antes de enviar o código para a produção?
    savings: Confira suas economias
    restart: Reiniciar calculadora
  tooltips:
    numberOfUsers: Quantos funcionários estão usando seu conjunto de ferramentas atual?
    numberOfMaintainers: |
      Quantos funcionários instalam, integram e gerenciam seu conjunto de ferramentas? Presume-se um salário anual médio de US$ 97.000 para esses profissionais, conforme dados do <a href="https://www.glassdoor.co.in/Salaries/us-software-developer-salary-SRCH_IL.0,2_IN1_KO3,21.htm">Glassdoor</a>
    sourceCodeManagement: "Gasto anual (em USD) com ferramentas de gerenciamento de código-fonte, incluindo controle de versão de recursos, branching e revisões de código. (Ex.: ferramentas como GitHub, BitBucket)"
    continuousIntegration: "Gasto anual (em USD) com ferramentas de integração contínua, incluindo compilação, teste e validação. (Ex.: ferramentas como Jenkins, CircleCI)"
    continuousDelivery: "Gasto anual (em USD) com ferramentas de entrega contínua usadas para implantações seguras em ambientes de preparo/produção. (Ex.: ferramentas como Harness, ArgoCD)"
    applicationSecurity: "Gasto anual (em USD) com ferramentas de segurança de aplicações, incluindo testes estáticos e dinâmicos, entre outros. (Ex.: ferramentas como Snyk, Veracode ou Sonarcube)"
    agileProjectManagement: "Gasto anual (em USD) com ferramentas de gerenciamento ágil de projetos usadas para planejamento dentro de uma equipe/projeto. (Ex.: ferramentas como Jira, Trello, ou Asana)"
    agilePortfolioManagement: "Gasto anual (em USD) com ferramentas de gerenciamento de portfólio ágil usadas para planejamento entre várias equipes/projetos. (Ex.: ferramentas como Planview Enterprise One, Jira Align)"
    registries: "Gasto anual (em USD) com registros de contêineres e pacotes. (Ex.: ferramentas como Artifactory, Docker Hub)"
  currentSpendTooltip: Os gastos atuais incluem os custos com ferramentas e mantenedores
  cost_results:
    heading: Atualmente, sua cadeia de ferramentas custa
    monetary_sign: $
    premium_description: O GitLab Premium pode ser uma ótima escolha para sua empresa melhorar a produtividade e colaboração das equipes.
    disclaimer: Os resultados são baseados em economias relatadas por empresas de porte semelhante. Os resultados são puramente estimativas e estão sujeitos a alterações com base em diversos fatores que influenciaram o cálculo.
    youCouldSave: Você poderia economizar
    ultimateSwitch: anualmente ao migrar para o GitLab Ultimate!
    ultimate_description: O GitLab Ultimate é a escolha ideal para sua empresa garantir segurança, conformidade e um planejamento eficiente em todos os departamentos.
    current_spend: Seus gastos atuais
    vs: em comparação com o
    gitlab_ultimate: GitLab Ultimate
    free_trial: Faça uma avaliação gratuita
    buy_ultimate: Comprar o Ultimate agora
    results_disclaimer: Os resultados são puramente estimativas e estão sujeitos a alterações com base em diversos fatores que influenciaram o cálculo.
    premiumSwitch: anualmente ao migrar para o GitLab Premium!
    gitlab_premium: GitLab Premium
    buy_premium: Comprar o Premium agora
  time_results: 
    toolchain_cost: Hoje, você gasta
    hoursPerProject: horas por projeto
    hours: horas
    save: Economize
    switching: Ao migrar para
    efficiency: Aumente sua eficiência em
    premium: GitLab Premium
    ultimate: GitLab Ultimate
    results: Os resultados são baseados em economias relatadas por empresas de porte semelhante. Os resultados são puramente estimativas e estão sujeitos a alterações com base em diversos fatores que influenciaram o cálculo.
    tooltip: Calculado com base no estudo Total Economic Impact™ encomendado pela GitLab à Forrester Consulting em 2022.
    ultimateButton: 
      text: Saiba mais sobre o Ultimate
      href: /pricing/ultimate/
      variant: primary
      data_ga_name: ultimate learn more
      data_ga_location: calculator body with time calculator selected
    premiumButton: 
      text: Saiba mais sobre o Premium
      href: /pricing/premium/
      variant: primary
      data_ga_name: premium learn more
      data_ga_location: calculator body with time calculator selected
    salesButton: 
      text: Fale com a equipe de vendas
      href: /sales/
      variant: tertiary
      data_ga_name: sales
      data_ga_location: calculator body with time calculator selected
  marketoForm:
    contactUs: Fale com a gente para obter uma análise mais detalhada das suas economias
    requiredFields: Todos os campos são obrigatórios
    submissionReceived: Envio recebido!
  premium_features:
    header: 'O GitLab Premium inclui:'
    button:
      text: Saiba mais sobre o Premium
      url: '/pricing/premium/'
      data_ga_location: premium roi results
      data_ga_name: learn more about premium
      listHeading: "O GitLab Premium inclui:"
      freeTrial: Faça uma avaliação gratuita
      learnMore: Saiba mais sobre o Premium
    list:
      - text: Revisões de código mais rápidas
        information:
          - text: Vários aprovadores na revisão de código
          - text: Proprietários de código
          - text: Análise de revisão de código
          - text: Revisões de solicitação de merge
          - text: Relatórios de Code Quality
          - text: Pipelines de resultados de merge
          - text: Comentários no Review Apps
      - text: CI/CD avançada
        information:
          - text: Painel de pipelines de CI/CD
          - text: Trens de merge
          - text: Gráficos de pipelines multiprojeto
          - text: CI/CD para repositório externo
          - text: Gerenciamento de implantação do GitOps
          - text: Painel de ambientes
          - text: Modelos de arquivos em grupo
          - text: Pacote robusto de implantação e reversões
      - text: Entrega ágil empresarial
        information:
          - text: Roadmaps
          - text: Vários responsáveis pelo tíquete
          - text: Épicos de nível único
          - text: Listas de marcos do quadro de tíquetes
          - text: Etiquetas com escopo
          - text: Promover o tíquete a um épico
          - text: Vários quadros de tíquetes de Grupo
          - text: Dependências do tíquete
      - text: Controles de lançamento
        information:
          - text: Regras de aprovação para revisão de código
          - text: Aprovações obrigatórias de solicitação de merge
          - text: Dependências de solicitação de merge
          - text: Regras de push
          - text: Restringir acesso a push e merge
          - text: Ambientes protegidos
          - text: Bloquear a associação de projetos ao grupo
          - text: DNS com reconhecimento de geolocalização
      - text: Confiabilidade autogerenciada
        information:
          - text: Recuperação de desastre
          - text: Modo de manutenção
          - text: Clonagem distribuída com GitLab Geo
          - text: Armazenamento Git tolerante a falhas com Gitaly
          - text: Compatível com arquiteturas dimensionadas
          - text: Replicação geográfica de registro de contêiner
          - text: Balanceamento de carga de banco de dados para PostgreSQL
          - text: Encaminhamento de logs
      - text: 10.000 minutos de computação por mês
      - text: Assistência
  ultimate_features:
    header: 'O GitLab Ultimate inclui:'
    button:
      text: Saiba mais sobre o Ultimate
      url: '/pricing/ultimate/'
      data_ga_location: premium roi results
      data_ga_name: learn more about ultimate
    list:
      - text: Testes avançados de segurança
        information:
          - text: Testes Dinâmicos de Segurança de Aplicações
          - text: Painéis de segurança
          - text: Gestão de vulnerabilidades
          - text: Análise de contêiner
          - text: Dependency Scanning
          - text: Relatórios de vulnerabilidades
          - text: Fuzzing de API
          - text: Banco de dados de vulnerabilidades
      - text: Gestão de vulnerabilidades
        information:
          - text: Aprovações de segurança
          - text: Políticas de segurança
      - text: Pipelines de conformidade
        information:
          - text: Relatório de eventos de auditoria
          - text: Interface para eventos de auditoria
          - text: Conformidade de licença
          - text: Relatório de conformidade
          - text: Gestão de qualidade
          - text: Gestão de requisitos
          - text: Solicitar o tíquete do Jira antes de fazer o merge do código
          - text: Estruturas de conformidade personalizadas
      - text: Gestão de portfólio
        information:
          - text: Quadros de épicos
          - text: Épicos de vários níveis
          - text: Relatórios de integridade de tíquetes e épicos
          - text: Hierarquia de planejamento
          - text: Roadmaps em nível de portfólio
          - text: Edição em massa de épicos
      - text: Gestão de fluxo de valor
        information:
          - text: Insights
          - text: Métrica DORA-4 - Frequência de implantações
          - text: Métrica DORA-4 - Prazo de entrega das alterações
      - text: 50.000 minutos de computação por mês
      - text: Assistência
      - text: Usuários convidados gratuitos
  time_card:
    img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
    text: Saiba como a HackerOne conseguiu fazer implantações 5 vezes mais rápidas ao migrar para a segurança integrada do GitLab
    cta:
      text: Leia o estudo de caso do cliente
      url: /customers/hackerone/
      data_ga_name: HackerOne case study
      data_ga_location: calculator body with time calculator selected
  cards:
  - icon:
      name: money
      alt: Ícone de dinheiro
      variant: marketing
    title: Saiba mais sobre as opções de preços dos planos do GitLab
    link_text: Acesse a página de preços
    link_url: /pricing/
    data_ga_name: learn more about gitlab pricing plans
    data_ga_location: roi calculator
  - icon:
      name: cloud-thin
      alt: Ícone de nuvem com traços finos
      variant: marketing
    title: Compre o GitLab por meio de marketplaces de nuvem
    link_text: Fale com a equipe de vendas
    link_url: /sales/
    data_ga_name: purchase gitlab through cloud marketplaces
    data_ga_location: roi calculator