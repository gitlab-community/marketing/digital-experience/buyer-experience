---
  title: "Migrar do Atlassian para o GitLab"
  description: O Atlassian encerrou o suporte para todos os produtos Server, incluindo Bitbucket, Jira, Bamboo e Confluence, em fevereiro de 2024. Não deixe que essa mudança force você a adotar uma ferramenta que não é adequada para sua equipe. Saiba como o GitLab pode ajudar.
  side_navigation_links:
    - title: Bitbucket
      href: '#bitbucket'
      data_ga_name: bitbucket
      data_ga_location: navigation
    - title: Jira
      href: '#jira'
      data_ga_name: jira
      data_ga_location: navigation
    - title: Bamboo
      href: '#bamboo'
      data_ga_name: bamboo
      data_ga_location: navigation
    - title: Confluence
      href: '#confluence'
      data_ga_name: confluence
      data_ga_location: navigation
    - title: Preços
      href: '#pricing'
      data_ga_name: pricing
      data_ga_location: navigation
  side_navigation_text_link:
    text: Fale com a equipe de vendas
    url: '/sales/'
    data_ga_name: sales
    data_ga_location: navigation
  solutions_hero:
    title: Migrar do Atlassian para o GitLab
    badge:
      text: O complemento Enterprise Agile Planning já está disponível
      url: "#agile-add-on"
      icon: gl-arrow-right
      data_ga_name: agile delivery add-on
      data_ga_location: hero
    isDark: true
    subtitle: O Atlassian encerrou o suporte para todos os produtos Server, incluindo [Bitbucket](#bitbucket), [Jira](#jira), [Bamboo](#bamboo) e [Confluence](#confluence), em **fevereiro de 2024**. Não deixe que o fim da vida útil do Server force você a adotar uma ferramenta que não é adequada para sua equipe. Saiba como o GitLab pode ajudar.
    primary_btn:
      text: Fale com um especialista
      url: /sales/
      data_ga_name: sales
      data_ga_location: header
    image:
      image_url: "https://images.ctfassets.net/xz1dnu24egyd/26SlAGBx32ZPYt2I4Wcd9A/95e7a4353d19da0b6a3eaff250e8205c/atlassian-eol-landing.svg"
      alt: "devops lifecycle image"
      bordered: true
  content_card:
    title: 'Vá do Bitbucket para o GitLab'
    description: |
      **Colabore e acelere**


      Sempre faça lançamentos com controle de versão de ativos, ciclos de feedback e padrões de branching poderosos para ajudar seus desenvolvedores a resolver problemas com eficiência e agregar valor.


      &nbsp;

      **Compatível e seguro**


      Permita que as equipes revisem, rastreiem e aprovem alterações no código com uma fonte única de verdade.
    link: /
    type: "eBook"
    image: https://images.ctfassets.net/xz1dnu24egyd/GF2chSP6KXImk3djWqJxk/d0119e7396e05f112e77583415405c10/principle-1.png
    icon: open-book
    button_text: 'Baixe o livro digital'
    size: half
    button_type: primary
    image_right: true

  benefit_cards:
    title: O GitLab é o caminho mais rápido entre uma ideia e o software
    benefits:
      - title: Implantação em qualquer lugar
        icon: gitlab-cloud
        description: Escolha como e onde você deseja implantar. O GitLab sempre atende às necessidades de clientes que precisam de uma solução self-hosted.
      - title: Simplifique o desenvolvimento de software
        icon: collaboration
        description: Chega de cadeias de ferramentas que não conversam entre si. Apresente às suas equipes uma plataforma única para todo o ciclo de vida do desenvolvimento de software para encurtar as durações dos ciclos, aumentar a produtividade e reduzir os custos de desenvolvimento.
      - title: Vá no seu próprio ritmo
        icon: monitor-gitlab
        description: Ainda não está pronto para fazer a transição de todas as suas ferramentas e serviços de uma só vez? Sem problemas. O GitLab também oferece uma ampla seleção de integrações, para que você possa ir na velocidade que faz sentido para a sua empresa.

  copy_benefits_bitbucket:
    title: Vá do Bitbucket para o GitLab
    subtitle: Controle de versão para todos
    background: true
    benefits:
      - title: Controle de versão poderoso
        icon: idea-collaboration
        description: Sempre faça lançamentos com controle de versão de ativos, ciclos de feedback e padrões de branching poderosos para ajudar seus desenvolvedores a testar e implantar software com eficiência.
      - title: Segurança e conformidade integradas
        icon: devsecops
        description: Permita que as equipes revisem, rastreiem e aprovem alterações no código com uma fonte única de verdade.
        button_text: Saiba mais
        button_url: /solutions/source-code-management/
        data_ga_name: bitbucket
        data_ga_location: body

  copy_benefits_jira:
    title: Vá do Jira para o GitLab
    subtitle: Suporte ágil integrado para projetos, programas e produtos
    benefits:
      - title: Planejamento Ágil
        icon: agile
        description: Acelere a entrega de valor de negócios com recursos ágeis de gerenciamento de equipe e portfólio prontos para uso, tudo dentro da mesma plataforma de entrega de software usada para criar, testar e implantar software.
        button_text: Saiba mais
        button_url: /solutions/agile-delivery/
        data_ga_name: agile delivery
        data_ga_location: jira body
      - title: Gestão de fluxo de valor
        icon: visibility
        description: Visualize fluxos de valor e elimine gargalos em todo o ciclo de vida do desenvolvimento de software, repriorize rapidamente para melhorar a produtividade e aumentar o valor do negócio.
        button_text: Saiba mais
        button_url: /solutions/value-stream-management/
        data_ga_name: value stream management
        data_ga_location: jira body

  copy_benefits_confluence:
    title: Vá do Confluence para o GitLab
    subtitle: Gestão do conhecimento para eliminar os silos de comunicação
    benefits:
      - title: Páginas wiki personalizáveis
        icon: web-alt
        description: Armazene o conhecimento e a documentação da empresa diretamente no GitLab para facilitar o acesso entre equipes e funções.
        button_text: Saiba mais
        button_url: https://docs.gitlab.com/ee/user/project/wiki/
        data_ga_name: confluence
        data_ga_location: body

  copy_benefits_bamboo:
    title: Vá do Bamboo para o GitLab
    background: true
    subtitle: CI/CD flexível para tornar a entrega de software sob demanda e com ajuste de escala
    benefits:
      - title: Pipelines construídos para ter simplicidade e ajuste de escala
        icon: monitor-pipeline
        description: Comece facilmente com modelos integrados e aumente a escala com pipelines principais e secundários e trens de merge.
      - title: Segurança e conformidade integradas
        icon: shield-check-large-light
        description: De pipelines de conformidade a análise de segurança integrada, veja tudo em um só lugar e tenha melhor visibilidade e controle.
        button_text: Saiba mais
        button_url: /solutions/security-compliance/
        data_ga_name: security compliance
        data_ga_location: bamboo body
      - title: Teste contextual
        icon: monitor-test-2
        description: Teste tudo automaticamente, do desempenho do código até a segurança, e revise e aprove os resultados dentro do contexto.
        button_text: Saiba mais
        button_url: /solutions/continuous-integration/
        data_ga_name: bamboo
        data_ga_location: body

  resources:
    col_size: 4
    header: "Recursos para migrar do Atlassian para o GitLab"
    description: Saiba mais sobre o fim do suporte do Server do Atlassian e como migrar para o GitLab.
    case_studies:
      - header: "Complemento Enterprise Agile Planning do GitLab para todas as funções"
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/
          text: Saiba mais
          data_ga_name: enterprise agile planning resource
          data_ga_location: body
      - header: Por que o GitLab self-managed é o parceiro perfeito para o setor público
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/3wDRL8ssQu72bNfsvN1Sds/1b48bc3c8cb63eca3f47cf40b99a6308/gitlabflatlogomap.jpg
          alt: ""
        link:
          href: /blog/2023/12/13/why-gitlab-self-managed-is-the-perfect-partner-for-the-public-sector/
          text: Saiba mais
          data_ga_name: perfect partner resource
          data_ga_location: body
      - header: "Fim do Server do Atlassian: diga adeus às cadeias de ferramentas desconexas e olá à plataforma DevSecOps"
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/HE9XYEbagTAiFYXohe8p9/47f0ab25fda5c00db236e7bb91790492/value-stream.png
          alt: ""
        link:
          href: /blog/2023/09/26/atlassian-server-ending-move-to-a-single-devsecops-platform/
          text: Saiba mais
          data_ga_name: atlassian resource
          data_ga_location: body
      - header: Cinco razões para simplificar a configuração da sua ferramenta de planejamento ágil com o GitLab
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/10/17/five-reasons-to-simplify-agile-planning-tool-configuration-gitlab/
          text: Saiba mais
          data_ga_name: simplify agile resource
          data_ga_location: body
      - header: Dicas para uma migração bem-sucedida do Jira para o GitLab
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/10/24/tips-for-a-successful-jira-to-gitlab-migration/
          text: Saiba mais
          data_ga_name: Tips for a successful Jira to GitLab migration resource
          data_ga_location: body
      - header: Como migrar do Bamboo para a CI/CD do GitLab
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/26eTPBKrWglfy1ILxWSUsD/7230b6d753d78b80abbcfe736449e218/securitylifecycle-light.png
          alt: ""
        link:
          href: /blog/2023/10/26/migrating-from-bamboo-to-gitlab-cicd/
          text: Saiba mais
          data_ga_name: How to migrate from Bamboo to GitLab CI/CD resource
          data_ga_location: body
      - header: Importe seu projeto do Bitbucket Cloud para o GitLab
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/7veFR11eLgKvggQfNISDgK/d0ecb9098c5b913e22d90790d8cb7391/bitbucket.png
          alt: ""
        link:
          href: https://docs.gitlab.com/ee/user/project/import/bitbucket.html
          text: Saiba mais
          data_ga_name: bitbucket resource
          data_ga_location: body
      - header: Importe os tíquetes do seu projeto do Jira para o GitLab
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/5mnlgW5gqy5JBvE819dH2q/25521343423e026ada32fbd7daf80b5a/jira.png
          alt: ""
        link:
          href: https://docs.gitlab.com/ee/user/project/import/jira.html
          text: Saiba mais
          data_ga_name: jira resource
          data_ga_location: body
      - header: Um bilhão de pipelines de inovação de CI/CD
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/19z3IxRhCpQf37oD2lEOHN/34faaf9fb0fd927efe6c30a10f74c499/securitylifecycle.png
          alt: ""
        link:
          href: /blog/2023/10/04/one-billion-pipelines-cicd/
          text: Saiba mais
          data_ga_name: pipeline resource
          data_ga_location: body
  tier_block:
    header: Qual nível é ideal para você?
    cta:
      url: /pricing/
      text: Qual nível é ideal para você?
      data_ga_name: pricing
      data_ga_location: free tier
      aria_label: preços
    tiers:
      - id: free
        title: Gratuito
        items:
          - Testes Estáticos de Segurança de Aplicações (SAST) e Detecção de Segredos
          - Descobertas em arquivo json
        link:
          href: /pricing/
          text: Saiba mais
          data_ga_name: pricing
          data_ga_location: free tier
          aria_label: plano gratuito
      - id: premium
        title: Premium
        items:
          - Testes Estáticos de Segurança de Aplicações (SAST) e Detecção de Segredos
          - Descobertas em arquivo json
          - Aprovações de MR e mais controles comuns
        link:
          href: /pricing/
          text: Saiba mais
          data_ga_name: pricing
          data_ga_location: premium tier
          aria_label: plano premium
      - id: ultimate
        title: Ultimate
        items:
          - Todos os recursos do plano Premium mais
          - Scanners de segurança abrangentes que incluem SAST, DAST, Segredos, dependências, contêineres, IaC, APIs, imagens de cluster e testes de fuzz
          - Resultados acionáveis dentro do pipeline de MR
          - Pipelines de conformidade
          - Painéis de segurança e conformidade
          - Muito mais
        link:
          href: /pricing/
          text: Saiba mais
          data_ga_name: pricing
          data_ga_location: ultimate tier
          aria_label: plano ultimate
        cta:
          href: /free-trial/
          text: Faça uma avaliação gratuita
          data_ga_name: pricing
          data_ga_location: ultimate tier
          aria_label: plano ultimate

  pricing_banner:
    id: agile-add-on
    badge_text: Novidade
    headline: GitLab Enterprise Agile Planning
    blurb: Estações adicionais de Agile Planning para clientes do GitLab Ultimate.
    button:
      text: Fale com a gente para saber os preços
      url: "/sales/"
      data_ga_name: contact us for pricing
      data_ga_location: body
    footnote: ''
    features:
      header: 'Solução de Agile Planning de nível empresarial:'
      list:
        - text: Substituto para o Jira
        - text: Um fluxo de trabalho de planejamento para todas as equipes envolvidas no ciclo de vida do desenvolvimento de software
        - text: Análise de fluxo de valor para medir a velocidade e o impacto
        - text: Painéis executivos para visibilidade em toda a empresa
        - text: Estações autônomas de Enterprise Agile Planning para clientes do GitLab Ultimate


  recognition:
    heading: O GitLab é a plataforma líder em DevSecOps
    stats:
      - value: Mais de 50%
        blurb: das empresas Fortune 100
      - value: Mais de 30 milhões
        blurb: de usuários registrados
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartner logo
        text: 'O GitLab é líder no Gartner® Magic Quadrant™ de 2024 para plataformas de DevOps'
        link:
          text: Leia o relatório
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forrester logo
        text: "O GitLab é o único líder no Forrester Wave™: plataformas integradas de entrega de software, segundo trimestre de 2023"
        link:
          text: Leia o relatório
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
      - header: 'O GitLab é classificado como Líder G2 em todas as categorias de DevSecOps'
        link:
          text: O que os analistas do setor estão dizendo sobre o GitLab
          href: /analysts/
          data_ga_name: g2
          data_ga_location: analyst
        badges:
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: Líder, Grande Empresa, G2 – 3º trimestre de 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MomentumLeader_Leader.png
            alt: Líder, Momentum, G2 – 3º trimestre de 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MostImplementable_Total.svg
            alt: Mais Implementável, G2 – 3º trimestre de 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestResults_Total.png
            alt: Melhores Resultados, G2 – 3º trimestre de 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Enterprise_Total.png
            alt: Melhor Relacionamento, Grande Empresa, G2 – 3º trimestre de 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Mid-Market_Total.svg
            alt: Melhor Relacionamento, Empresa de Médio Porte, G2 – 3º trimestre de 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: Mais Fácil de Usar, G2 – 3º trimestre de 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestUsability_Total.png
            alt: Melhor Usabilidade, G2 – 3º trimestre de 2023

  atlassian_solutions:
    title: O GitLab se integra a centenas de aplicações
    subtitle: Quer saber como suas equipes de produto e engenharia podem colaborar melhor sem precisar alternar as ferramentas? [Fale com a gente para saber mais sobre as nossas soluções personalizadas](/sales/).
    solutions:
      - title: Jira
        subtitle: '[Integração simples com o Jira da Atlassian](https://docs.gitlab.com/ee/integration/jira/){data-ga-name="jira docs" data-ga-location="body"}'
      - title: GitHub
        subtitle: Integração perfeita da [CI/CD do GitLab com o SCM do GitHub](https://docs.gitlab.com/ee/user/project/integrations/github.html){data-ga-name="github docs" data-ga-location="body"}
      - title: Jenkins
        subtitle: '[Plugin do GitLab](https://docs.gitlab.com/ee/integration/jenkins.html){data-ga-name="jenkins docs" data-ga-location="body"} bem mantido'
      - title: APIs
        subtitle: '[APIs](https://docs.gitlab.com/ee/api/integrations.html){data-ga-name="apis" data-ga-location="body"} em cada componente do GitLab'
