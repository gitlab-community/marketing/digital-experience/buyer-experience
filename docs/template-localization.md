# Template Localization

In order to handle the localization of pages that uses the same template but each page has a different list of languages 
there is a technical workflow to follow to accomplish this goal alongside the tag assignation within each Contentful page.

For a real live example, the [customers page](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/blob/main/pages/customers/_slug.vue) already has this implementation.

## Tagging pages in contentful

Once a page has been translated, start by going to the `Page` entry in Contentful, clicking `Tags`, and adding any or all of the following tags to the page. Ensure that you are using the tags that are prepended with the word `page`.

- `pageLanguage_es`
- `pageLanguage_ja-JP`
- `pageLanguage_it-IT`
- `pageLanguage_pt-BR`	
- `pageLanguage_fr-FR`	
- `pageLanguage_de-DE`	
- `pageLanguage_en-US`

## Use of Middleware

By using a [Nuxt Middleware](https://v2.nuxt.com/examples/middlewares/anonymous/), the data is fetched initially in this method 
in order for the data to be available in the following lifecycle methods such as `validate`, `asyncData` or `meta`. 
To properly follow a standard structure, the **middleware** method should look like this in the **page template** (The **getAvailableLanguagesFromTags** method returns the list of locales base on the Contentful Tags):

````js
// Page component

export default Vue.extend({
  async middleware(context) {
    if (process.client) {
      return;
    }

    // Fetching logic comes here(getEntries)...

    context.$pageData = result;

    // Reusable method that gets the available language from the entry tags from Contentful 
    context.$availableLanguages = getAvailableLanguagesFromTags(
      result.metadata.tags,
    );
  }
})

````

## Available Languages Mixin

The **validate** method ensures that a localized page is not generated if its locale is not within the **Available Languages**.
To avoid code duplication across localized pages, use this Mixin when implementing localized templates. The Mixin leverages data stored in the context by the **middleware** method.

````js
// available-language.mixin.ts

export const AvailableLanguagesMixin = Vue.extend({
  /**
   * The validate method runs after the "middleware" method
   * Each component should handle the logic to populate the $availableLanguages in the "middleware" method
   * @param $availableLanguages
   * @param i18n
   */
  validate({ $availableLanguages, i18n }: any) {
    if (process.client) {
      return true;
    }

    return $availableLanguages.some((locale) => locale === i18n.locale);
  },
});

````

## Use the content in the AsyncData function

To avoid making the same request twice and decrease build performance, the data is grabbed from the context of the page previously fetched in the **middleware** method. 

````js
// Page component

export default Vue.extend({
  async asyncData({ $pageData, $availableLanguages }) {
    const { fields, sys } = $pageData;
    
    // Process the $pageData as needed

    return { //...otherdata 
      pageData,
      availableLanguages: $availableLanguages // To be used in the head() method
    };
  }
})

````

## Metadata

The SEO attributes generation such as the **hreflang** depends on the list of **Available Languages** of a page, 
which means that it is necessary to pass that list as parameter to the `getMetadata` to properly build the SEO metadata.

````js
// Page Component

export default Vue.extend({
  head(): MetaInfo {
    return getPageMetadata(
      this.metadata,
      this.$route.path,
      this.$nuxtI18nHead({ addSeoAttributes: true }),
      this.availableLanguages,
    );
  }
})
````

## Routing generation

Depending on the domain, the route generation may differ, nevertheless, the code should be pretty similar to the example below:

````js
// route.contentful.js

export async function fetchRoutes() {
  try {
    const { items } = await client.getEntries({
      content_type: CONTENT_TYPES.CASE_STUDY,
      select: ['fields.slug', 'metadata.tags'], // Get the tags and the slug
      limit: 1000,
    });

    // Build the localized routes based on the tags and the slug
    return items.flatMap(({ fields, metadata }) => {
      const availableLanguages = getAvailableLanguagesFromTags(metadata.tags);
      return availableLanguages.map(
        (locale) =>
          `${locale !== LOCALES.DEFAULT ? `/${locale}` : ''}/domain/${fields.slug}`,
      );
    });
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching routes from Contentful:', error);
    throw error;
  }
}

````
