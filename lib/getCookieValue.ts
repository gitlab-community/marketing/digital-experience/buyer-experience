export default function getCookieValue(cookieName: string) {
  const name: string = `${cookieName}=`;
  const decodedCookie: string = decodeURIComponent(document.cookie);
  const ca: string[] = decodedCookie.split(';');

  for (let i = 0; i < ca.length; i++) {
    const c: string = ca[i].trim();
    if (c.indexOf(name) === 0) {
      return c.substring(name.length);
    }
  }
  return null;
}
