/**
 * Checks if a given date string represents a past date excluding today's date.
 *
 * The function creates a Date object from the provided date string and compares it with today's date,
 * both set to 00:00 hours, to determine if the given date is strictly before today. This comparison
 * excludes today's date, meaning if the provided date is exactly today, it will not be considered as
 * past.
 *
 * @param {string} date - The date string to check. The string should be in a format recognized by the
 * Date.parse() method of JavaScript, such as "YYYY-MM-DD", "MM/DD/YYYY" or "MMMM DD, YYYY".
 *
 * @returns {boolean} Returns true if the given date is strictly before today's date at 00:00 hours;
 * otherwise, returns false.
 */
export function isPastDate(date: string): boolean {
  const inputDate = new Date(new Date(date).setHours(0, 0, 0, 0));
  const todayDate = new Date(new Date().setHours(0, 0, 0, 0));

  return inputDate < todayDate;
}
