export function initializeYouTubeIframeApi() {
  let initialized = false;
  const allScripts = document.getElementsByTagName('script');
  for (let i = 0; i < allScripts.length; i++) {
    if (allScripts[i].src === 'https://www.youtube.com/iframe_api') {
      initialized = true;
    }
  }
  if (initialized) {
    return;
  }
  const tag = document.createElement('script');
  tag.src = 'https://www.youtube.com/iframe_api';
  const firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

export function createYouTubePlayer(identifier, onReady) {
  window[`playerFor${identifier}`] = undefined;
  window.onYouTubeIframeAPIReady = () => {
    window[`playerFor${identifier}`] = new window.YT.Player(
      `iframe-for-${identifier}`,
      {
        events: {
          onReady,
        },
      },
    );
  };
}
