import Vue from 'vue';
import { refresh } from 'aos';

export const AosAnimationsMixin = Vue.extend({
  mounted() {
    // If the $AOS instance does not exists, it means AOS has not been initialized,
    if (!this.$AOS) {
      this.applyFallbackStyles();
    }
    refresh();
  },
  methods: {
    applyFallbackStyles() {
      document.querySelectorAll('[data-aos]').forEach((element) => {
        element.style.opacity = '1';
        element.style.transform = 'none';
        element.style.transition = 'none';
      });
    },
  },
});
