import Vue from 'vue';

// If using this mixin, be sure the component inhering this logic has a data object with a videoRefs array.
// Example:
// data : () => ({
//   videoRefs: ['video1', 'video2']
// })

export const EnableReduceMotion = Vue.extend({
  data() {
    return {
      prefersReducedMotion: null,
    };
  },
  mounted() {
    (this.prefersReducedMotion = window.matchMedia(
      '(prefers-reduced-motion: reduce)',
    )),
      this.handleReducedMotion();
  },
  methods: {
    handleReducedMotion() {
      if (this.prefersReducedMotion.matches) {
        this.pauseVideos();
      }
    },
    pauseVideos() {
      if (!this.videoRefs) {
        // eslint-disable-next-line no-console
        return console.error(
          '"videoRefs" data property not present in component',
        );
      }
      if (!Array.isArray(this.videoRefs)) {
        // eslint-disable-next-line no-console
        return console.error('videoRefs is not an array');
      }
      const videoRefs: Array<string> = this.videoRefs;
      if (Array.isArray(videoRefs)) {
        this.videoRefs.forEach((ref) => {
          const video = this.$refs[ref];
          if (video && video.pause) {
            video.pause();
          }
        });
      }
    },
  },
});
