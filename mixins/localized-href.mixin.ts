import Vue from 'vue';
import { LANG_OPTIONS } from '../common/constants';

export const LocalizedHrefMixin = Vue.extend({
  mounted() {
    this.formatLocalizedLinks();
  },
  methods: {
    formatLocalizedLinks() {
      const { localizedContainer } = this.$refs;
      const elems = this.getChildrenWithHref(localizedContainer as Element);

      if (elems.length) {
        elems
          .filter((elem) => elem.href && !elem.href.startsWith('#'))
          .forEach((elem) => {
            // Skip localization for URLs ending with '.pdf'
            if (elem.href.endsWith('.pdf')) {
              return;
            }
            if (!elem.href.includes(window.location.pathname)) {
              elem.setAttribute('href', this.getLocalizedHref(elem.href));
            }
          });
      }
    },
    getChildrenWithHref(element: Element): any[] {
      let result = [];

      if (element.hasAttribute('href')) {
        result.push(element);
      }

      const { children } = element;
      for (let i = 0; i < children.length; i++) {
        const child = children[i];
        const childResult = this.getChildrenWithHref(child);
        result = [...result, ...childResult];
      }

      return result;
    },
    getUnlocalizedPath(path: string) {
      const pathLang = LANG_OPTIONS.filter((lang) => !lang.default).find(
        (lang) => path.includes(lang.path),
      );

      return pathLang ? path.replace(pathLang.path, '') : path;
    },
    getLocalizedHref(href: string) {
      const flatLang = localStorage.getItem('lang');
      const defaultLang = LANG_OPTIONS.filter((lang) => lang.default);
      const selectedLang = flatLang ? JSON.parse(flatLang) : defaultLang;
      let localizedPath;

      const url = new URL(href);
      const unlocalizedPath = this.getUnlocalizedPath(`${url.pathname}`);
      const isHomepage =
        url.href.includes(window.location.origin) && url.pathname === '/';
      const shouldBeLocalized =
        url.href.includes(window.location.origin) &&
        !url.href.includes(selectedLang?.value) &&
        url.pathname !== '/';

      if (isHomepage) {
        // This one is only for the homepage as the method "localePath" returns a "/" when a localized page does not exists
        localizedPath = (this as any).localePath(
          unlocalizedPath,
          selectedLang.value,
        );
        localizedPath = `${url.origin}${localizedPath}`;
      } else if (shouldBeLocalized) {
        // This one is to add localization only to pages that belongs to the site
        localizedPath = (this as any).localePath(
          unlocalizedPath,
          selectedLang.value,
        );
        localizedPath = localizedPath.endsWith('/')
          ? localizedPath
          : `${localizedPath}/`;
        localizedPath =
          localizedPath === '/'
            ? url.href
            : `${url.origin}${localizedPath}${url.search}`;
        // Line above adds the query after the localizedPath has been created,
        // to avoid populating localizedPath with incomplete query links ('/' + query) when a localized page does not exists
      } else {
        localizedPath = url.href;
      }

      return localizedPath;
    },
  },
});
