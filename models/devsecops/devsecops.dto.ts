import { PageDTO } from '../page.dto';

export interface DevSecOpsDTO extends PageDTO {
  hero?: any;
  banner?: any;
  categories_block?: any;
  badges?: any;
  pricing?: any;
}
