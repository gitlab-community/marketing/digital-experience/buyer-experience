import { ContentfulLivePreview } from '@contentful/live-preview';
import { convertToCaseSensitiveLocale } from '~/common/util';

export default function ({ app }) {
  const locale = convertToCaseSensitiveLocale(app.i18n.locale);
  ContentfulLivePreview.init({ locale });
}
