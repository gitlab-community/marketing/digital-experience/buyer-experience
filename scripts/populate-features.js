/* eslint-disable */

const contentful = require('contentful-management');
const fs = require('fs');
const yaml = require('yaml');
const path = require('path');

// Read the access token from the command-line arguments
const accessToken = process.argv[2];
if (!accessToken) {
  console.error('Access token is required as the first argument.');
  process.exit(1);
}

// Load and parse the YAML files
const featuresFile = fs.readFileSync(
  path.resolve('../content/synced/features.yml'),
  'utf8',
);
const features = yaml.parse(featuresFile).features;

const categoriesFile = fs.readFileSync(
  path.resolve('../content/synced/categories.yml'),
  'utf8',
);
const categories = yaml.parse(categoriesFile);

// Hardcoded cardGroups
const cardGroups = [
  'portfolio_management',
  'service_desk',
  'incident_management',
  'product_analytics_visualization',
  'on_call_schedule_management',
];

// Contentful credentials and IDs
const spaceId = 'xz1dnu24egyd';
const environmentId = 'master'; // Change this if you're using a different environment
const parentEntryId = '6gQyO1TMxzU2eeFB8TAFTn';

// Initialize Contentful client
const client = contentful.createClient({
  accessToken: accessToken,
});

async function createCardGroups(environment) {
  const createdCardGroups = {};

  for (const cardGroup of cardGroups) {
    if (!categories[cardGroup]) {
      console.error(`Category '${cardGroup}' not found in categories file`);
      continue;
    }

    try {
      const category = categories[cardGroup];
      let cardGroupEntry;
      const entries = await environment.getEntries({
        content_type: 'cardGroup',
        'fields.internalName': `Visibility & Measurement - ${category.name}`,
      });

      if (entries.items.length > 0) {
        cardGroupEntry = entries.items[0];
      } else {
        cardGroupEntry = await environment.createEntry('cardGroup', {
          fields: {
            internalName: {
              'en-US': `Visibility & Measurement - ${category.name}`,
            },
            description: {
              'en-US': category.description,
            },
            header: {
              'en-US': category.name,
            },
            componentName: {
              'en-US': 'solutions-platform-link-group',
            },
            headerAnchorId: {
              'en-US': cardGroup.replace(/_/g, '-'),
            },
            card: {
              'en-US': [],
            },
          },
        });
      }

      createdCardGroups[cardGroup] = cardGroupEntry;
      console.log(
        `CardGroup '${cardGroup}' is created/exists with ID: ${cardGroupEntry.sys.id}`,
      );
    } catch (error) {
      console.error(
        `Error creating/getting cardGroup '${cardGroup}': ${error}`,
      );
    }
  }

  return createdCardGroups;
}

async function createAndPublishCards(environment, cardGroupEntries) {
  for (const feature of features) {
    if (!feature.category) {
      console.log('No category found for feature:', feature.title);
      continue; // Skip this feature and continue with the next one
    }

    if (feature.category.some((cardGroup) => cardGroups.includes(cardGroup))) {
      try {
        // Check if the card already exists
        const existingEntries = await environment.getEntries({
          content_type: 'card',
          'fields.internalName': `Visibility & Measurement - ${feature.title}`,
        });

        let cardEntry;
        if (existingEntries.items.length > 0) {
          cardEntry = existingEntries.items[0];
          console.log(`Card already exists: ${feature.title}`);
        } else {
          // Create the card if it doesn't exist
          cardEntry = await environment.createEntry('card', {
            fields: {
              internalName: {
                'en-US': `Visibility & Measurement - ${feature.title}`,
              },
              title: {
                'en-US': feature.title,
              },
              cardLink: {
                'en-US': feature.link,
              },
              cardLinkDataGaName: {
                'en-US': feature.title,
              },
              cardLinkDataGaLocation: {
                'en-US': 'solutions',
              },
            },
          });
          await cardEntry.publish();
          console.log(`Created and published card: ${feature.title}`);
        }

        // Add the card reference to the corresponding cardGroup
        for (const cardGroup of feature.category) {
          if (!cardGroupEntries[cardGroup]) {
            console.error(
              `CardGroup '${cardGroup}' not found in cardGroupEntries`,
            );
            continue;
          }

          const cardGroupEntry = cardGroupEntries[cardGroup];

          // Ensure cardGroupEntry.fields.card is initialized
          if (!cardGroupEntry.fields.card) {
            cardGroupEntry.fields.card = {};
          }

          if (!cardGroupEntry.fields.card['en-US']) {
            cardGroupEntry.fields.card['en-US'] = [];
          }

          const cardLinks = cardGroupEntry.fields.card['en-US'];

          // Check if the card is already linked to the cardGroup
          if (!cardLinks.some((link) => link.sys.id === cardEntry.sys.id)) {
            cardLinks.push({
              sys: {
                type: 'Link',
                linkType: 'Entry',
                id: cardEntry.sys.id,
              },
            });
          }
        }
      } catch (error) {
        console.error(
          `Error creating or linking card '${feature.title}': ${error}`,
        );
      }
    }
  }

  // Update and publish cardGroups with new card references
  for (const cardGroup in cardGroupEntries) {
    try {
      const cardGroupEntryId = cardGroupEntries[cardGroup].sys.id;

      // Fetch the latest version of the cardGroup entry before updating
      const cardGroupEntry = await environment.getEntry(cardGroupEntryId);

      // Ensure cardGroupEntry.fields.card is initialized
      if (!cardGroupEntry.fields.card) {
        cardGroupEntry.fields.card = {};
      }

      if (!cardGroupEntry.fields.card['en-US']) {
        cardGroupEntry.fields.card['en-US'] = [];
      }

      const cardLinks = cardGroupEntry.fields.card['en-US'];

      // Link the cards
      for (const feature of features) {
        if (feature.category && feature.category.includes(cardGroup)) {
          const existingEntries = await environment.getEntries({
            content_type: 'card',
            'fields.internalName': `Visibility & Measurement - ${feature.title}`,
          });

          let cardEntry =
            existingEntries.items.length > 0 ? existingEntries.items[0] : null;
          if (
            cardEntry &&
            !cardLinks.some((link) => link.sys.id === cardEntry.sys.id)
          ) {
            cardLinks.push({
              sys: {
                type: 'Link',
                linkType: 'Entry',
                id: cardEntry.sys.id,
              },
            });
          }
        }
      }

      // Update and publish the cardGroup entry
      await cardGroupEntry.update();
      console.log(`Updated cardGroup: ${cardGroup}`);
    } catch (error) {
      console.error(`Error updating cardGroup '${cardGroup}': ${error}`);
    }
  }
}

async function linkCardGroupsToParent(environment, cardGroupEntries) {
  try {
    // Fetch the latest version of the parent entry
    let parentEntry = await environment.getEntry(parentEntryId);

    // Prepare the content links
    const contentLinks = Object.values(cardGroupEntries).map(
      (cardGroupEntry) => ({
        sys: {
          type: 'Link',
          linkType: 'Entry',
          id: cardGroupEntry.sys.id,
        },
      }),
    );

    // Filter out any card groups that are already linked in the parent entry
    const existingContentIds = (
      parentEntry.fields.content?.['en-US'] || []
    ).map((link) => link.sys.id);
    const newContentLinks = contentLinks.filter(
      (link) => !existingContentIds.includes(link.sys.id),
    );

    if (newContentLinks.length > 0) {
      // Update the parent entry's content field
      if (parentEntry.fields.content) {
        parentEntry.fields.content['en-US'].push(...newContentLinks);
      } else {
        parentEntry.fields.content = {
          'en-US': newContentLinks,
        };
      }

      // Attempt to update and publish the parent entry
      let updateSuccessful = false;
      while (!updateSuccessful) {
        try {
          await parentEntry.update();
          updateSuccessful = true;
          console.log('Linked cardGroups to parent entry');
        } catch (error) {
          if (error.name === 'VersionMismatch') {
            // Refetch the latest version of the parent entry and retry
            parentEntry = await environment.getEntry(parentEntryId);
          } else {
            throw error;
          }
        }
      }
    } else {
      console.log('No new cardGroups to link to the parent entry.');
    }
  } catch (error) {
    console.error(`Error linking cardGroups to parent entry: ${error}`);
  }
}

async function run() {
  try {
    const space = await client.getSpace(spaceId);
    const environment = await space.getEnvironment(environmentId);

    const cardGroupEntries = await createCardGroups(environment);
    await createAndPublishCards(environment, cardGroupEntries);
    await linkCardGroupsToParent(environment, cardGroupEntries);
  } catch (error) {
    console.error(`Error: ${error}`);
  }
}

run();
