import { mapQuoteCarousel } from './solutions/solutions-default.helper';

export function developerExperienceDataHelper(pageContent: any[], metadata) {
  let hero, stat, accordion, benefits, resources, quotes;
  let cards = [];

  pageContent.forEach((item) => {
    switch (item.fields.componentName) {
      case 'DevExHero':
        hero = {
          ...item.fields,
        };
        break;
      case 'DevExStat':
        stat = {
          ...item.fields,
        };
        break;
      case 'DevExResources':
        resources = {
          ...item.fields,
          column_size: 4,
          title: item.fields.header,
          link: {
            text: item.fields.cta?.fields.text,
            href: item.fields.cta?.fields.externalUrl,
            data_ga_name: item.fields.cta?.fields.dataGaName,
            data_ga_location: item.fields.cta?.fields.dataGaLocation,
          },
          cards: item.fields.card.map((card: any) => {
            return {
              link_text: card.fields.button?.fields.text,
              data_ga_location:
                card.fields.button?.fields.dataGaLocation ||
                card.fields.cardLinkDataGaLocation ||
                'body',
              event_type: card.fields.subtitle,
              data_ga_name:
                card.fields.button?.fields.dataGaName ||
                card.fields.cardLinkDataGaName ||
                card.fields.title,
              header: card.fields.title,
              href:
                card.fields.button?.fields.externalUrl || card.fields.cardLink,
              icon: {
                name: card.fields.iconName,
                variant: 'marketing',
                alt: '',
              },
              image: card.fields.image.fields.file.url,
            };
          }),
        };
        break;
      case 'DevExBenefits':
        benefits = {
          ...item.fields,
        };
        break;
      case 'DevExCards':
        cards.push({ ...item.fields });
        break;
      case 'DevExCaseStudies':
        quotes = {
          ...item.fields,
        };
      case 'quote-carousel':
        quotes = mapQuoteCarousel({ ...item.fields });
        break;
      default:
        break;
    }
    switch (item.fields?.customFields?.componentName) {
      case 'DevExAccordion':
        accordion = {
          ...item.fields,
        };
        break;
      default:
        break;
    }
  });

  return {
    title: metadata[0].fields?.ogTitle,
    description: metadata[0].fields?.ogDescription,
    title_image: metadata[0].fields?.ogImage?.fields.image.fields.file.url,
    twitter_image: metadata[0].fields?.ogImage?.fields.image.fields.file.url,
    hero,
    stat,
    accordion,
    benefits,
    cards,
    quotes,
    resources,
  };
}
