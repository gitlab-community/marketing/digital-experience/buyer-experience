import { Context } from '@nuxt/types';
import {
  dataCleanUp,
  dateHelper,
  faqHelper,
  formHelper,
  metaDataHelper,
  objectCleanUp,
  pageCleanup,
  regionsBuilder,
  sectionsHelper,
  worldTourAgendaHelper,
  worldTourHeroHelper,
  worldTourLandingHeroBuilder,
} from '~/services/events/events.helpers';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import {
  convertToCaseSensitiveLocale,
  getAvailableLanguagesFromTags,
} from '~/common/util';
import { CtfEntry, CtfEventPage, CtfPage } from '~/models';
import {
  WorldTourEventsData,
  WorldTourExecutiveEventsData,
  WorldTourLandingData,
} from '~/models/events/world-tour.dto';
interface EventAddionalContent {
  faq?: {};
}
export class WorldTourEventsService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  private async getContentfulData(slug: string): Promise<CtfEntry<CtfPage>> {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      include: 10,
      'fields.slug': slug,
    });

    if (items.length === 0) {
      throw new Error('World Tour page not found in Contentful');
    }

    const [contentfulPage] = items;

    return contentfulPage as CtfEntry<CtfPage>;
  }

  private async getEventsContentfulData(slug: string) {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.EVENT,
      include: 10,
      'fields.slug': slug,
      locale,
    });

    if (items.length === 0) {
      throw new Error('World Tour page not found in Contentful');
    }

    const [contentfulEventPage] = items;

    return contentfulEventPage as CtfEntry<CtfEventPage>;
  }

  async getLandingContent() {
    const { fields, sys } = await this.getContentfulData(
      'events/devsecops-world-tour',
    );
    const spaceId = sys.space.sys.id;
    const entryId = sys.id;
    const events = await this.getWorldTourEvents();

    return {
      content: this.worldTourLandingHelper(fields),
      spaceId,
      entryId,
      events,
    };
  }

  async getWorldTourEvent(slug: string) {
    const { fields, sys, metadata } = await this.getEventsContentfulData(slug);
    const spaceId = sys.space.sys.id;
    const entryId = sys.id;
    const isExecutive = slug.includes('executive');
    const city = this.worldTourEventsDataHelper(fields);
    const availableLanguages = getAvailableLanguagesFromTags(metadata.tags);

    return {
      city,
      spaceId,
      entryId,
      availableLanguages,
    };
  }

  async getWorldTourEvents() {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.EVENT,
      include: 10,
      'fields.eventType': 'World Tour',
    });

    if (items.length === 0) {
      throw new Error('World Tour page not found in Contentful');
    }

    const events = items;

    return events;
  }
  additionalContentBuilder(page: any) {
    let result: EventAddionalContent = {};

    for (const section of page) {
      const id = section.sys.contentType?.sys.id;
      switch (id) {
        case CONTENT_TYPES.FAQ: {
          const FaqItems = objectCleanUp(section);
          result.faq = faqHelper(FaqItems);
          break;
        }
      }
    }

    return result;
  }

  worldTourLandingHelper(data: CtfEventPage): WorldTourLandingData {
    const rawData = pageCleanup(data) as WorldTourLandingData;

    return {
      title: rawData.title,
      slug: rawData.slug,
      description: rawData.description,
      hero:
        rawData.pageContent &&
        worldTourLandingHeroBuilder(rawData.pageContent.hero),
      regions: rawData.pageContent.regions && rawData.pageContent.regions,
      form: rawData.pageContent?.form,
      faq:
        rawData.pageContent &&
        rawData.pageContent.faq &&
        faqHelper(rawData.pageContent.faq),
      metadata: metaDataHelper(rawData),
      banner: rawData.pageContent?.banner,
      nextSteps: rawData.pageContent?.nextSteps,
      section: rawData.pageContent.section && rawData.pageContent.section,
    };
  }

  worldTourEventsDataHelper(data: CtfEventPage): WorldTourEventsData {
    const additionalContent =
      data.additionalContent &&
      this.additionalContentBuilder(data.additionalContent);
    const rawData = dataCleanUp(data) as WorldTourEventsData;
    const date = rawData.date && dateHelper(rawData.date);

    return {
      metadata: metaDataHelper(rawData),
      name: rawData.name,
      internalName: rawData.internalName,
      slug: rawData.slug,
      eventType: rawData.eventType,
      description: rawData.description,
      date,
      endDate: rawData.endDate,
      location: rawData.location,
      locationState: rawData.locationState,
      region: rawData.region,
      hero:
        rawData.hero &&
        worldTourHeroHelper(
          rawData.hero.fields,
          rawData.staticFields?.breadcrumbs,
        ),
      agenda: rawData.agenda && worldTourAgendaHelper(rawData.agenda),
      form: rawData.form && formHelper(rawData.form?.fields),
      sponsors:
        rawData.featuredContent &&
        sectionsHelper(rawData.featuredContent, 'sponsors'),
      footnote: rawData.footnote && rawData.footnote,
      additionalContent: additionalContent && { ...additionalContent },
    };
  }

  worldTourExecutiveEventsDataHelper(
    data: CtfEventPage,
  ): WorldTourExecutiveEventsData {
    const rawData = dataCleanUp(data) as WorldTourExecutiveEventsData;
    const date = rawData.date && dateHelper(rawData.date);

    return {
      metadata: metaDataHelper(rawData),
      name: rawData.name,
      internalName: rawData.internalName,
      slug: rawData.slug,
      eventType: rawData.eventType,
      description: rawData.description,
      date,
      endDate: rawData.endDate,
      location: rawData.location,
      region: rawData.region,
      hero:
        rawData.hero &&
        rawData.hero.fields &&
        worldTourHeroHelper(rawData.hero.fields),
      form: rawData.form?.fields && formHelper(rawData.form?.fields),
      sponsors:
        rawData.featuredContent &&
        sectionsHelper(rawData.featuredContent, 'sponsors'),
      footnote: rawData.footnote,
      nextSteps: rawData.nextSteps?.fields?.variant,
    };
  }
}
