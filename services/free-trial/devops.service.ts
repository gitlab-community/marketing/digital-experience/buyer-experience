import { getUrlFromContentfulImage } from '~/common/util';
import { CONTENT_TYPES } from '~/common/content-types';
function customerBuilder(customers) {
  return customers.map((customer) => {
    return {
      image_url: customer.fields.image?.fields?.file?.url,
      alt: customer.fields.altText || '',
      url: customer.fields.link,
      size: customer.fields.customFields?.size,
      ...(customer.fields.customFields?.id && {
        id: customer.fields.customFields.id,
      }),
    };
  });
}
function customerLogosBuilder(section) {
  const sectionData = {
    text: {
      legend: section.text,
      url: section.link[0],
      data_ga_name: section.text,
      data_ga_location: section.componentName,
    },
    showcased_enterprises: customerBuilder(section.logo),
    segment: section.segment && section.segment,
  };

  return sectionData;
}
// This service is also being used by free-trial/devops, please keep that in mind when making changes to this file
export function assemblePageContent(contentfulData) {
  const componentNames = {
    HERO: 'hero',
    G2_CATEGORIES: 'accolades',
    INFORMATION_SQUARES: 'squares',
    NEXT_STEPS: 'nextSteps',
    CUSTOMER_LOGOS: 'customerLogos',
    HEADER_AND_TEXT: 'block',
  };

  let components = [];
  let heroData = {
    name: componentNames.HERO,
    data: {},
  }; // Object to hold data for single Hero

  contentfulData.forEach((obj) => {
    const { id } = obj.sys.contentType.sys;
    switch (id) {
      case CONTENT_TYPES.HERO: {
        const {
          internalName,
          title,
          subheader,
          description,
          video,
          primaryCta,
          customFields,
        } = obj.fields;

        let key = 'default';

        if (internalName.toLowerCase().includes('smb')) {
          key = 'smb';
        } else if (internalName.toLowerCase().includes('ci')) {
          key = 'ci';
        } else if (internalName.toLowerCase().includes('security')) {
          key = 'security';
        }

        // Add data to heroData object
        heroData.data[key] = {
          internalName,
          video_url: video?.fields?.url,
          button: {
            text: primaryCta?.fields?.text,
            url: primaryCta?.fields?.externalUrl,
            dataGaName: primaryCta?.fields?.dataGaName,
          },
          heading: title,
          subtitle: subheader,
          terms: description,
          icon_list: customFields?.icon_list && customFields?.icon_list,
        };

        break;
      }
      case CONTENT_TYPES.TWO_COLUMN_BLOCK: {
        const { header, subheader, cta, assets } = obj.fields;

        components.push({
          name: componentNames.G2_CATEGORIES,
          data: {
            header,
            subtitle: subheader,
            button: {
              text: cta?.fields?.text,
              href: cta?.fields?.externalUrl,
              data_ga_name: cta?.fields?.dataGaName,
              data_ga_location: 'body',
            },
            badges: assets.map((asset) => ({
              src: asset?.fields.image?.fields.file?.url,
              alt: asset?.fields.altText,
            })),
          },
        });
        break;
      }
      case CONTENT_TYPES.CARD_GROUP: {
        const cards = obj.fields.card.map((card) => {
          return {
            heading: card?.fields?.title,
            text: card?.fields?.description,
            icon_src: card?.fields?.customFields?.icon_src,
          };
        });

        components.push({
          name: componentNames.INFORMATION_SQUARES,
          data: {
            header: obj.fields.header,
            information_squares: cards,
          },
        });
        break;
      }
      case CONTENT_TYPES.HEADER_AND_TEXT: {
        const { header, text } = obj.fields;
        components.push({
          name: componentNames.HEADER_AND_TEXT,
          data: {
            header,
            text,
          },
        });
        break;
      }
      case CONTENT_TYPES.NEXT_STEPS: {
        const { heading, tagline, secondaryTagline, primaryButton } =
          obj.fields;

        components.push({
          name: componentNames.NEXT_STEPS,
          data: {
            heading,
            subtitle: tagline,
            text: secondaryTagline,
            button: {
              cta_text: primaryButton?.fields?.text,
              url: primaryButton?.fields?.externalUrl,
              dataGaName: primaryButton?.fields?.dataGaName,
              dataGaLocation: primaryButton?.fields?.dataGaLocation,
            },
          },
        });
        break;
      }
      case CONTENT_TYPES.CUSTOMER_LOGOS:
        components.push({
          name: componentNames.CUSTOMER_LOGOS,
          data: {
            ...customerLogosBuilder(obj?.fields),
          },
        });
        break;
    }
  });

  // this unfortunately ruins the flexibility of the page,
  // but it's safe to assume the hero variants will only be used at the top of the page
  components.unshift(heroData);

  return {
    components,
  };
}
