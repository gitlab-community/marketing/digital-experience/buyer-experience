import { getUrlFromContentfulImage } from '~/common/util';
import { CONTENT_TYPES } from '~/common/content-types';

// This service is also being used by free-trial/devops, please keep that in mind when making changes to this file
export function assemblePageContent(contentfulData) {
  const componentNames = {
    HERO: 'hero',
    G2_CATEGORIES: 'accolades',
    INFORMATION_SQUARES: 'squares',
    RECOGNITION_CARDS: 'free-trial-recognition-cards',
    NEXT_STEPS: 'nextSteps',
  };

  let components = [];
  let heroData = {
    name: componentNames.HERO,
    data: {},
  }; // Object to hold data for single Hero

  contentfulData.forEach((obj) => {
    const { id } = obj.sys.contentType.sys;
    switch (id) {
      case CONTENT_TYPES.HERO: {
        const {
          internalName,
          title,
          subheader,
          description,
          video,
          primaryCta,
          customFields,
        } = obj.fields;

        let key = 'default';

        if (internalName.toLowerCase().includes('smb')) {
          key = 'smb';
        } else if (internalName.toLowerCase().includes('ci')) {
          key = 'ci';
        } else if (internalName.toLowerCase().includes('security')) {
          key = 'security';
        }

        // Add data to heroData object
        heroData.data[key] = {
          internalName,
          video_url: video?.fields?.url,
          button: {
            text: primaryCta?.fields?.text,
            url: primaryCta?.fields?.externalUrl,
            dataGaName: primaryCta?.fields?.dataGaName,
            icon_name: primaryCta?.fields?.iconName,
          },
          heading: title,
          subtitle: subheader,
          terms: description,
          video_note: customFields?.video_note && customFields.video_note,
          icon_list: customFields?.icon_list && customFields?.icon_list,
        };

        break;
      }
      case CONTENT_TYPES.TWO_COLUMN_BLOCK: {
        const { header, subheader, cta, assets } = obj.fields;

        components.push({
          name: componentNames.G2_CATEGORIES,
          data: {
            header,
            subtitle: subheader,
            button: {
              text: cta?.fields?.text,
              href: cta?.fields?.externalUrl,
              data_ga_name: cta?.fields?.dataGaName,
              data_ga_location: 'body',
            },
            badges: assets.map((asset) => ({
              src: asset?.fields.image?.fields.file?.url,
              alt: asset?.fields.altText,
            })),
          },
        });
        break;
      }
      case CONTENT_TYPES.CARD_GROUP: {
        if (obj.fields.componentName === 'free-trial-recognition-cards') {
          const cards = obj.fields.card.map((card) => {
            return {
              header: card?.fields.title,
              text: card?.fields?.description,
              icon_src: card?.fields?.customFields?.icon_src,
              logo: card?.fields?.image?.fields.file?.url,
              link: {
                text: card?.fields?.button.fields?.text,
                href: card?.fields?.button.fields?.externalUrl,
                data_ga_name: card?.fields?.button.fields?.dataGaName,
                data_ga_location: card?.fields?.button.fields?.dataGaLocation,
              },
            };
          });
          components.push({
            name: componentNames.RECOGNITION_CARDS,
            data: { cards },
          });
        }
        if (obj.fields.componentName === 'squares') {
          const cards = obj.fields.card.map((card) => {
            return {
              heading: card?.fields.title,
              text: card?.fields?.description,
              icon_src: card?.fields?.customFields?.icon_src,
              icon: card?.fields?.iconName,
            };
          });

          components.push({
            name: componentNames.INFORMATION_SQUARES,
            data: {
              information_squares: cards,
            },
          });
        }
        break;
      }
      case CONTENT_TYPES.NEXT_STEPS: {
        const { heading, tagline, secondaryTagline, primaryButton } =
          obj.fields;

        components.push({
          name: componentNames.NEXT_STEPS,
          data: {
            heading,
            subtitle: tagline,
            text: secondaryTagline,
            button: {
              cta_text: primaryButton?.fields?.text,
              url: primaryButton?.fields?.externalUrl,
              dataGaName: primaryButton?.fields?.dataGaName,
              dataGaLocation: primaryButton?.fields?.dataGaLocation,
            },
          },
        });
        break;
      }
    }
  });

  // this unfortunately ruins the flexibility of the page,
  // but it's safe to assume the hero variants will only be used at the top of the page
  components.unshift(heroData);
  return {
    components,
  };
}
