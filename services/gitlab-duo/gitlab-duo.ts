import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { CtfCard, CtfEntry } from '~/models';
import { convertToCaseSensitiveLocale } from '~/common/util';

const LANGUAGE_TAGS = {
  'ja-JP': 'language_ja-JP',
  'de-DE': 'language_de-DE',
  'fr-FR': 'language_fr-FR',
  'it-IT': 'language_it-IT',
  'pt-BR': 'language_pt-BR',
  es: 'language_es-419',
};

async function grabPricingCards(locale: string): Promise<CtfEntry<CtfCard>[]> {
  try {
    const queries = [
      getClient().getEntries({
        content_type: CONTENT_TYPES.CARD,
        'sys.id': '3uwIEJT93Q7DMkDGYrZgnM',
        include: 4,
      }),
      getClient().getEntries({
        content_type: CONTENT_TYPES.CARD,
        'sys.id': '3zMrhwWAMWosvf6kPzihg3',
        include: 4,
      }),
    ];

    const results = await Promise.all(queries);

    // Base case
    if (!locale || locale === 'en-us') {
      return results.map((result) => result.items[0]);
    }

    // Localized case
    // NOTE: this depends on the localized entry having language tags.
    const localizedQueries = [
      getClient().getEntries({
        content_type: CONTENT_TYPES.CARD,
        'fields.sourceEntry.sys.id': '3uwIEJT93Q7DMkDGYrZgnM',
        include: 4,
        locale: convertToCaseSensitiveLocale(locale),
      }),
      getClient().getEntries({
        content_type: CONTENT_TYPES.CARD,
        'fields.sourceEntry.sys.id': '3zMrhwWAMWosvf6kPzihg3',
        include: 4,
        locale: convertToCaseSensitiveLocale(locale),
      }),
    ];

    const localizedResults = await Promise.all(localizedQueries);

    const mappedLocale = localizedResults.map((result) => {
      const items = result.items;

      // From all available locales, return the localized version that corresponds language tag <-> locale
      for (let i = 0; i < items.length; i++) {
        const item = items[i];
        if (
          item.metadata.tags[0].sys.id ===
          LANGUAGE_TAGS[convertToCaseSensitiveLocale(locale)]
        ) {
          return item;
        }
      }
      return null;
    });

    return mappedLocale;
  } catch (e) {
    throw new Error(e);
  }
}

export async function mapGitlabDuoPricingCards(app, locale: string) {
  const pricing = (await grabPricingCards(locale)).map((item) =>
    app.$pricingService.mapFeaturedAddonCard(item),
  );

  return {
    pricing,
  };
}
