import { CONTENT_TYPES } from '~/common/content-types';
export function techPartnerChildrenHelper(data: Record<string, any>[]) {
  const pageData: Record<string, any> = {};

  //  Data Mapping
  const componentsData = {} as any;
  const techPartners = {} as any;

  data.forEach((entry) => {
    if (entry.sys?.contentType?.sys?.id == CONTENT_TYPES.SIDE_MENU) {
      componentsData.navLinks = entry;
    } else if (entry.sys?.contentType?.sys?.id == CONTENT_TYPES.NEXT_STEPS) {
      componentsData.nextSteps = entry;
    } else {
      componentsData[entry.fields.componentName] = entry;
    }
  });

  // Hero Component:
  techPartners.hero = {
    title: componentsData.hero.fields.title,
    subtitle: componentsData.hero.fields.description,
    primaryButton: componentsData.hero?.fields?.primaryCta?.fields,
    secondaryButton: componentsData.hero?.fields?.secondaryCta?.fields && {
      ...componentsData.hero?.fields?.secondaryCta?.fields,
    },
  };

  // Headlines Component:
  techPartners.headlines = {
    cards: componentsData.textAndTitle.fields.card.map((card: any) => ({
      title: card.fields.title,
      description: card.fields.description,
      button: {
        url: card.fields?.button?.fields.externalUrl,
        title: card.fields?.button?.fields.text,
        variant: card.fields?.button?.fields.variation,
      },
      secondaryButton: {
        url: card.fields?.secondaryButton?.fields.externalUrl,
        title: card.fields?.secondaryButton?.fields.text,
        variant: card.fields?.secondaryButton?.fields.variation,
      },
      video: {
        url: card.fields?.video?.fields.url,
        title: card.fields?.video?.fields.title,
      },
    })),
  };

  // Benefits Cards Component:
  techPartners.benefits = {
    cards: componentsData.benefitsCards.fields.card.map((card: any) => ({
      title: card.fields.title,
      description: card.fields.description,
      icon: {
        name: card.fields.iconName,
        alt: `${card.fields.iconName} icon`,
        variant: card.fields.iconVariant || 'marketing',
      },
      button: { ...card.fields.button },
    })),
  };

  // Quote Component:
  techPartners.quote = {
    title: componentsData.quote.fields.description,
    subtitle: componentsData.quote.fields.subtitle,
    image: {
      alt: componentsData.quote.fields?.image?.fields.title,
      url: componentsData.quote.fields?.image?.fields.file.url,
    },
  };

  // Solutions Component:
  techPartners.solutions = {
    title: componentsData.solutions.fields.header,
    description: componentsData?.solutions?.fields.description,
    cards: componentsData.solutions.fields.card.map((card: any) => ({
      title: card.fields.title,
      description: card.fields.description,
      button: {
        text: card.fields.button?.fields.text,
        href: card.fields.button?.fields.externalUrl || card.fields.cardLink,
      },
    })),
  };

  // Resource Cards Component:
  techPartners.resourceCards = {
    name: 'solutions-resource-cards',
    column_size: 4,
    title: componentsData['solutions-resource-cards'].fields.header,
    grouped: true,
    link: {
      text: componentsData['solutions-resource-cards'].fields.cta?.fields.text,
      href: componentsData['solutions-resource-cards'].fields.cta?.fields
        .externalUrl,
      data_ga_name:
        componentsData['solutions-resource-cards'].fields.cta?.fields
          .dataGaName,
      data_ga_location:
        componentsData['solutions-resource-cards'].fields.cta?.fields
          .dataGaLocation,
    },
    cards: componentsData['solutions-resource-cards'].fields.card.map(
      (card: any) => {
        return {
          link_text: card.fields.button?.fields.text,
          data_ga_location:
            card.fields.button?.fields.dataGaLocation ||
            card.fields.cardLinkDataGaLocation ||
            'body',
          event_type:
            card.fields.subtitle ?? card.fields.customFields.event_type,
          header: card.fields.title,
          href:
            card.fields.button?.fields.externalUrl ||
            card.fields.cardLink ||
            card.fields.video?.fields.url,
          icon: {
            name: card?.fields.customFields.icon.name,
            variant: 'marketing',
            alt: '',
          },
          image: card.fields.image.fields.file.url,
        };
      },
    ),
  };

  // Side Navigation
  techPartners.navLinks = {
    anchors: componentsData.navLinks?.fields.anchors.map((anchor: any) => ({
      href: anchor.fields.anchorLink,
      title: anchor.fields.linkText,
    })),
    image: {
      alt: componentsData.navLinks?.fields.navImage.fields.title,
      url: componentsData.navLinks?.fields.navImage.fields.file.url,
    },
    buttons: componentsData.navLinks?.fields?.content?.map((button: any) => ({
      externalUrl: button.fields.externalUrl,
      text: button.fields.text,
      variant: button.fields.variation,
      data_ga_name: button?.fields?.dataGaName,
      data_ga_location: button?.fields?.dataGaLocation,
      icon: {
        name: button?.fields?.iconName,
        variant: button.fields.iconVariant || 'marketing',
        alt: '',
      },
    })),
    textLink: componentsData.navLinks?.fields?.textLink && {
      url:
        componentsData.navLinks?.fields?.textLink?.fields?.externalUrl &&
        componentsData.navLinks?.fields?.textLink?.fields?.externalUrl,
      variation:
        componentsData.navLinks?.fields?.textLink?.fields?.externalUrl &&
        componentsData.navLinks?.fields?.textLink?.fields?.externalUrl,
      data_ga_name:
        componentsData.navLinks?.fields?.textLink?.fields?.dataGaName,
      data_ga_location:
        componentsData.navLinks?.fields?.textLink?.fields?.dataGaLocation,
      text:
        componentsData.navLinks?.fields?.textLink?.fields?.text &&
        componentsData.navLinks?.fields?.textLink?.fields?.text,
    },
    footer:
      componentsData.navLinks?.fields?.footer &&
      componentsData.navLinks?.fields?.footer,
  };

  techPartners.nextSteps = componentsData.nextSteps?.sys?.id;
  return techPartners;
}
