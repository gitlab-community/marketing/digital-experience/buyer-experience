export function salesDataHelper(items: any) {
  const data = items[0].fields.pageContent;
  const metadata = items[0].fields.seoMetadata[0].fields;

  let sales = { ...metadata };

  sales.components = [];

  // Hero
  const hero = data.filter(
    (entry) => entry.sys.contentType.sys.id == 'eventHero',
  );

  sales.components.push({
    name: hero[0].fields.componentName,
    data: {
      title: hero[0].fields.title,
      subtitle: hero[0].fields.description,
    },
  });

  //  Form Section
  const form = data.filter((entry) => entry.sys.contentType.sys.id == 'form');

  const supportCard = data.filter(
    (entry) => entry.sys.contentType.sys.id == 'card',
  );

  const carousel = data.filter(
    (entry) => entry.sys.contentType.sys.id == 'cardGroup',
  );

  sales.components.push({
    name: 'sales-form',
    data: {
      form: {
        formId: form[0].fields.formId,
        form_header: '',
        datalayer: form[0].fields.formDataLayer,
        required_text: form[0].fields.requiredFieldsText,
      },
      card: {
        title: supportCard[0].fields.title,
        description: supportCard[0].fields.description,
        link: {
          text: supportCard[0].fields.subtitle,
          href: supportCard[0].fields.cardLink,
          ga_name: 'Support',
          ga_location: 'body',
        },
      },
      quotes_carousel: {
        quotes: carousel[0].fields.card.map((card: any) => ({
          title_img: {
            url: card.fields.image.fields.file.url,
            alt: card.fields.image.fields.title,
          },
          quote: card.fields.description,
          author: card.fields.title,
          url: card.fields.cardLink,
          data_ga_name: `${card.fields.title} quote`,
          data_ga_location: 'body',
        })),
      },
    },
  });

  // FAQ section
  const faqBlock = data.filter(
    (entry) =>
      entry.sys.contentType.sys.id == 'blockGroup' &&
      entry.fields.componentName == 'faq',
  );

  const faq = faqBlock[0].fields;
  const accordions = faq.blocks;

  sales.faq = {
    header: faq.header,
    texts: {
      show: accordions[0].fields.expandAllCarouselItemsText,
      hide: accordions[0].fields.hideAllCarouselItemsText,
    },
    groups: accordions.map((block: any) => ({
      header: block.fields.title,
      questions: block.fields.singleAccordionGroupItems.map((item: any) => ({
        question: item.fields.header,
        answer: item.fields.text,
      })),
    })),
  };

  return sales;
}
