/**
 * This mapper links each content type to its corresponding component, using a default value for those without a specified component name.
 * It simplifies the process of connecting Contentful content types with Buyer Experience components, reducing the amount of code required
 **/

import {
  mapBenefits,
  mapBusinessShowcase,
  mapCompanyShowcase,
  mapCopy,
  mapCopyForm,
  mapCopyMedia,
  mapDemoCta,
  mapDevExBenefits,
  mapDevExCards,
  mapDevExCarousel,
  mapDevExHero,
  mapEducationCaseStudyCarousel,
  mapEducationCaseStudyTabs,
  mapEducationStats,
  mapEducationTiers,
  mapFeaturedMedia,
  mapFormSection,
  mapGroupButtons,
  mapIntegratedApplications,
  mapLogoLinks,
  mapNewQuotesCarousel,
  mapPricingTiers,
  mapQuote,
  mapQuoteCarousel,
  mapQuotesCarousel,
  mapReportCta,
  mapResourceCarousel,
  mapSideNavigation,
  mapSolutionsCards,
  mapSolutionsCtaCard,
  mapSolutionsFeatureList,
  mapSolutionsHero,
  mapSolutionsMetrics,
  mapSolutionsResourceCards,
  mapSolutionsVideo,
  mapTierBlock,
  mapVideoCarousel,
} from '~/services/solutions/solutions-default.helper';
import { CONTENT_TYPES } from '~/common/content-types';
import { COMPONENT_NAMES } from '~/common/constants';
import {
  mapByIndustryCaseStudies,
  mapByIndustryCtaShowcase,
  mapByIndustryQuotesCarousel,
  mapByIndustrySolutionsBlock,
  mapDemoCtaCard,
  mapFaq,
  mapFedramp,
  mapHeaderText,
  mapHomeSolutions,
  mapIndustryIntro,
  mapNonprofitIntro,
  mapNonprofitOverview,
  mapOpenSourceBlog,
  mapOpenSourceFormSection,
  mapOpenSourceOverview,
  mapOpenSourcePartnersGrid,
  mapOpenSourcePartnersGridFull,
  mapSolutionsCostCards,
  mapSolutionsForm,
  mapStartupsIntro,
  mapStartupsLink,
  mapStartupsOverview,
} from '~/services/solutions/solutions-by-industry.helper';
import {
  mapBySolutionBenefits,
  mapBySolutionIntro,
  mapBySolutionLink,
  mapBySolutionList,
  mapBySolutionShowcase,
  mapBySolutionValueProp,
} from '~/services/solutions/solutions-by-solution.helper';
import {
  mapRecognition,
  mapSolutionsPlatformColumnCopy,
  mapSolutionsPlatformCtaCard,
  mapSolutionsPlatformHeaderText,
  mapSolutionsPlatformLinkGroup,
  mapSolutionsPlatformTabLinks,
} from '~/services/solutions/solutions-by-platform.helper';

interface SolutionsMapper {
  contentType: string;
  mapComponent: string;
  componentName?: string;
  default?: boolean;
}

/**
 * Hero Content Type
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/eventHero/fields
 */
const heroMapper: Array<SolutionsMapper> = Object.freeze([
  {
    contentType: CONTENT_TYPES.HERO,
    mapComponent: mapDevExHero,
    componentName: COMPONENT_NAMES.DEV_EX_HERO,
  },
  {
    contentType: CONTENT_TYPES.HERO,
    mapComponent: mapSolutionsHero,
    componentName: COMPONENT_NAMES.SOLUTIONS_HERO,
  },
  {
    contentType: CONTENT_TYPES.HERO,
    mapComponent: mapSolutionsHero,
    default: true,
  },
]);

/**
 * Block Group Content Type
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/blockGroup/fields
 */
const blockGroupMapper: Array<SolutionsMapper> = Object.freeze([
  {
    contentType: CONTENT_TYPES.BLOCK_GROUP,
    mapComponent: mapCopyMedia,
    componentName: COMPONENT_NAMES.COPY_MEDIA,
  },
  {
    contentType: CONTENT_TYPES.BLOCK_GROUP,
    mapComponent: mapCopy,
    componentName: COMPONENT_NAMES.COPY,
  },
  {
    contentType: CONTENT_TYPES.BLOCK_GROUP,
    mapComponent: mapByIndustryQuotesCarousel,
    componentName: COMPONENT_NAMES.BY_INDUSTRY_QUOTES_CAROUSEL,
  },
  {
    contentType: CONTENT_TYPES.BLOCK_GROUP,
    mapComponent: mapByIndustryCtaShowcase,
    componentName: COMPONENT_NAMES.BY_INDUSTRY_CTA_SHOWCASE,
  },
]);

/**
 * Card Group Content Type
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/cardGroup/fields
 */
const cardGroupMapper: Array<SolutionsMapper> = Object.freeze([
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapBenefits,
    componentName: COMPONENT_NAMES.BENEFITS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapCompanyShowcase,
    componentName: COMPONENT_NAMES.COMPANY_SHOWCASE,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapBusinessShowcase,
    componentName: COMPONENT_NAMES.BUSINESS_SHOWCASE,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapFeaturedMedia,
    componentName: COMPONENT_NAMES.FEATURED_MEDIA,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapEducationCaseStudyCarousel,
    componentName: COMPONENT_NAMES.BY_SOLUTION_EDUCATION_CAROUSEL,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapBySolutionBenefits,
    componentName: COMPONENT_NAMES.BY_SOLUTION_BENEFITS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapByIndustrySolutionsBlock,
    componentName: COMPONENT_NAMES.BY_INDUSTRY_SOLUTIONS_BLOCK,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapNewQuotesCarousel,
    componentName: COMPONENT_NAMES.NEW_QUOTES_CAROUSEL,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapTierBlock,
    componentName: COMPONENT_NAMES.TIER_BLOCK,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapPricingTiers,
    componentName: COMPONENT_NAMES.PRICING_TIERS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapSolutionsResourceCards,
    componentName: COMPONENT_NAMES.SOLUTIONS_RESOURCE_CARDS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapSolutionsCostCards,
    componentName: COMPONENT_NAMES.SOLUTIONS_COST_CARDS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapResourceCarousel,
    componentName: COMPONENT_NAMES.RESOURCE_CAROUSEL,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapSolutionsCards,
    componentName: COMPONENT_NAMES.SOLUTIONS_CARDS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapSolutionsMetrics,
    componentName: COMPONENT_NAMES.SOLUTIONS_METRICS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapBySolutionValueProp,
    componentName: COMPONENT_NAMES.BY_SOLUTION_VALUE_PROP,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapByIndustryCaseStudies,
    componentName: COMPONENT_NAMES.BY_INDUSTRY_CASE_STUDIES,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapHomeSolutions,
    componentName: COMPONENT_NAMES.HOME_SOLUTIONS_CONTAINER,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapGroupButtons,
    componentName: COMPONENT_NAMES.GROUP_BUTTONS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapReportCta,
    componentName: COMPONENT_NAMES.REPORT_CTA,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapOpenSourceOverview,
    componentName: COMPONENT_NAMES.OVERVIEW_BLOCKS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapOpenSourceBlog,
    componentName: COMPONENT_NAMES.OPEN_SOURCE_BLOG,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapBySolutionShowcase,
    componentName: COMPONENT_NAMES.BY_SOLUTION_SHOWCASE,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapSolutionsFeatureList,
    componentName: COMPONENT_NAMES.SOLUTIONS_FEATURE_LIST,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapStartupsOverview,
    componentName: COMPONENT_NAMES.STARTUPS_OVERVIEW,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapNonprofitOverview,
    componentName: COMPONENT_NAMES.NONPROFIT_OVERVIEW,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapDemoCta,
    componentName: COMPONENT_NAMES.DEMO_CTA,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapQuotesCarousel,
    componentName: COMPONENT_NAMES.QUOTES_CAROUSEL,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapIntegratedApplications,
    componentName: COMPONENT_NAMES.INTEGRATED_APPLICATIONS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapSolutionsPlatformLinkGroup,
    componentName: COMPONENT_NAMES.SOLUTIONS_PLATFORM_LINK_GROUP,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapRecognition,
    componentName: COMPONENT_NAMES.RECOGNITION,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapDevExCards,
    componentName: COMPONENT_NAMES.DEV_EX_CARDS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapDevExCarousel,
    componentName: COMPONENT_NAMES.DEV_EX_CAROUSEL,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapQuoteCarousel,
    componentName: COMPONENT_NAMES.QUOTE_CAROUSEL,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapEducationTiers,
    componentName: COMPONENT_NAMES.BY_SOLUTION_EDUCATION_TIERS,
  },
  {
    contentType: CONTENT_TYPES.CARD_GROUP,
    mapComponent: mapEducationStats,
    componentName: COMPONENT_NAMES.BY_SOLUTION_EDUCATION_STATS,
  },
]);

/**
 * Card Content Type
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/card/fields
 */
const cardMapper: Array<SolutionsMapper> = Object.freeze([
  {
    contentType: CONTENT_TYPES.CARD,
    mapComponent: mapBySolutionIntro,
    componentName: COMPONENT_NAMES.BY_SOLUTION_INTRO,
  },
  {
    contentType: CONTENT_TYPES.CARD,
    mapComponent: mapBySolutionLink,
    componentName: COMPONENT_NAMES.BY_SOLUTION_LINK,
  },
  {
    contentType: CONTENT_TYPES.CARD,
    mapComponent: mapBySolutionList,
    componentName: COMPONENT_NAMES.BY_SOLUTION_LIST,
  },
  {
    contentType: CONTENT_TYPES.CARD,
    mapComponent: mapSolutionsCtaCard,
    componentName: COMPONENT_NAMES.SOLUTIONS_CTA_CARD,
  },
  {
    contentType: CONTENT_TYPES.CARD,
    mapComponent: mapStartupsLink,
    componentName: COMPONENT_NAMES.STARTUPS_LINK,
  },
  {
    contentType: CONTENT_TYPES.CARD,
    mapComponent: mapFedramp,
    componentName: COMPONENT_NAMES.FEDRAMP,
  },
  {
    contentType: CONTENT_TYPES.CARD,
    mapComponent: mapDemoCtaCard,
    componentName: COMPONENT_NAMES.DEMO_CTA_CARD,
  },
  {
    contentType: CONTENT_TYPES.CARD,
    mapComponent: mapSolutionsPlatformCtaCard,
    componentName: COMPONENT_NAMES.SOLUTIONS_PLATFORM_CTA_CARD,
  },
  {
    contentType: CONTENT_TYPES.CARD,
    mapComponent: mapSolutionsPlatformColumnCopy,
    componentName: COMPONENT_NAMES.SOLUTIONS_PLATFORM_COLUMN_COPY,
  },
]);

/**
 * Two Column Block Content Type
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/twoColumnBlock/fields
 */
const twoColumnBlockMapper: Array<SolutionsMapper> = Object.freeze([
  {
    contentType: CONTENT_TYPES.TWO_COLUMN_BLOCK,
    mapComponent: mapFormSection,
    componentName: COMPONENT_NAMES.APPLICATION_FORM,
  },
  {
    contentType: CONTENT_TYPES.TWO_COLUMN_BLOCK,
    mapComponent: mapOpenSourcePartnersGrid,
    componentName: COMPONENT_NAMES.OPEN_SOURCE_PARTNERS_GRID,
  },
  {
    contentType: CONTENT_TYPES.TWO_COLUMN_BLOCK,
    mapComponent: mapOpenSourcePartnersGridFull,
    componentName: COMPONENT_NAMES.OPEN_SOURCE_PARTNERS_GRID_FULL,
  },
  {
    contentType: CONTENT_TYPES.TWO_COLUMN_BLOCK,
    mapComponent: mapOpenSourceFormSection,
    componentName: COMPONENT_NAMES.OPEN_SOURCE_FORM_SECTION,
  },
  {
    contentType: CONTENT_TYPES.TWO_COLUMN_BLOCK,
    mapComponent: mapDevExBenefits,
    componentName: COMPONENT_NAMES.DEV_EX_BENEFITS,
  },
  {
    contentType: CONTENT_TYPES.TWO_COLUMN_BLOCK,
    mapComponent: mapOpenSourcePartnersGrid,
    default: true,
  },
]);

/**
 * Header And Text Content Type
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/headerAndText/fields
 */
const headerAndTextMapper: Array<SolutionsMapper> = Object.freeze([
  {
    contentType: CONTENT_TYPES.HEADER_AND_TEXT,
    mapComponent: mapHeaderText,
    componentName: COMPONENT_NAMES.HEADER_AND_TEXT,
  },
  {
    contentType: CONTENT_TYPES.HEADER_AND_TEXT,
    mapComponent: mapStartupsIntro,
    componentName: COMPONENT_NAMES.STARTUPS_INTRO,
  },
  {
    contentType: CONTENT_TYPES.HEADER_AND_TEXT,
    mapComponent: mapNonprofitIntro,
    componentName: COMPONENT_NAMES.NONPROFIT_INTRO,
  },
  {
    contentType: CONTENT_TYPES.HEADER_AND_TEXT,
    mapComponent: mapSolutionsPlatformHeaderText,
    componentName: COMPONENT_NAMES.SOLUTIONS_PLATFORM_HEADER_TEXT,
  },
  {
    contentType: CONTENT_TYPES.HEADER_AND_TEXT,
    mapComponent: mapSolutionsForm,
    componentName: COMPONENT_NAMES.SOLUTIONS_FORM,
  },
  {
    contentType: CONTENT_TYPES.HEADER_AND_TEXT,
    mapComponent: mapCopyForm,
    componentName: COMPONENT_NAMES.COPY_FORM,
  },
  {
    contentType: CONTENT_TYPES.HEADER_AND_TEXT,
    mapComponent: mapStartupsIntro,
    default: true,
  },
]);

/**
 * Customer Logos Content Type
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/customerLogos/fields
 */
const customerLogosMapper: Array<SolutionsMapper> = Object.freeze([
  {
    contentType: CONTENT_TYPES.CUSTOMER_LOGOS,
    mapComponent: mapLogoLinks,
    componentName: COMPONENT_NAMES.LOGO_LINKS,
  },
  {
    contentType: CONTENT_TYPES.CUSTOMER_LOGOS,
    mapComponent: mapIndustryIntro,
    componentName: COMPONENT_NAMES.BY_INDUSTRY_INTRO,
  },
  {
    contentType: CONTENT_TYPES.SIDE_MENU,
    mapComponent: mapSideNavigation,
    default: true,
  },
]);

/**
 * Tabs Control Container Content Type
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/tabControlsContainer/fields
 */
const tabControlContainerMapper = Object.freeze([
  {
    contentType: CONTENT_TYPES.TAB_CONTROLS_CONTAINER,
    mapComponent: mapSolutionsPlatformTabLinks,
    componentName: COMPONENT_NAMES.SOLUTIONS_PLATFORM_TAB_LINKS,
  },
  {
    contentType: CONTENT_TYPES.TAB_CONTROLS_CONTAINER,
    mapComponent: mapEducationCaseStudyCarousel,
    componentName: COMPONENT_NAMES.BY_SOLUTION_EDUCATION_CAROUSEL,
  },
  {
    contentType: CONTENT_TYPES.TAB_CONTROLS_CONTAINER,
    mapComponent: mapEducationCaseStudyTabs,
    componentName: COMPONENT_NAMES.BY_SOLUTION_EDUCATION_TABS,
  },
]);

const miscMapper: Array<SolutionsMapper> = Object.freeze([
  // Video Carousel
  {
    contentType: CONTENT_TYPES.VIDEO_CAROUSEL,
    mapComponent: mapVideoCarousel,
    default: true,
  },
  // Quote
  {
    contentType: CONTENT_TYPES.QUOTE,
    mapComponent: mapQuote,
    default: true,
  },
  // External Video
  {
    contentType: CONTENT_TYPES.EXTERNAL_VIDEO,
    mapComponent: mapSolutionsVideo,
    default: true,
  },
  // Faq
  { contentType: CONTENT_TYPES.FAQ, mapComponent: mapFaq, default: true },
  // Custom Component
  {
    contentType: CONTENT_TYPES.CUSTOM_COMPONENT,
    mapComponent: (fields) => fields,
    default: true,
  },
]);

export const componentMapper = [
  ...heroMapper,
  ...blockGroupMapper,
  ...cardGroupMapper,
  ...cardMapper,
  ...twoColumnBlockMapper,
  ...headerAndTextMapper,
  ...customerLogosMapper,
  ...tabControlContainerMapper,
  ...miscMapper,
];
