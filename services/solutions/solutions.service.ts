import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful.js';
import { TEMPLATES } from '~/common/constants';
import {
  convertToCaseSensitiveLocale,
  getUrlFromContentfulImage,
} from '~/common/util';
import { CtfEntry } from '~/models/content-types.dto';
import { componentMapper } from '~/services/solutions/solutions.mapper';
import { mapReportCta } from '~/services/solutions/solutions-default.helper';

export class SolutionsService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns Solutions content for the pages
   * @param slug
   * @param isIndexSlug
   */
  async getContent(slug: string) {
    return await this.getContentfulData(slug, this.$ctx.i18n.locale);
  }

  async getAvailableLanguages(slug) {
    const contentfulSlug = `solutions/${slug}`.replaceAll('//', '/');

    const options = {
      'fields.slug': contentfulSlug,
      content_type: CONTENT_TYPES.PAGE,
      include: 3,
    };

    const { items } = await getClient().withAllLocales.getEntries({
      ...options,
      select: ['fields.description'],
    });

    const [page] = items;

    if (!page) {
      throw new Error(
        `Page not found while getting the available languages in Contentful: ${contentfulSlug} `,
      );
    }

    return Object.keys(page.fields.description).map((item) =>
      item.toLowerCase(),
    );
  }

  private async getContentfulData(slug: string, locale: string) {
    const caseLocale = convertToCaseSensitiveLocale(locale);

    const solutionEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 5,
      locale: caseLocale,
      [`fields.description.${caseLocale}[exists]`]: true,
    });
    if (solutionEntry.items.length === 0) {
      throw new Error(`Solutions page not found: ${slug}`);
    }

    const [items] = solutionEntry.items;
    return { items, solutions: this.transformContentfulData(items) };
  }

  transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata, schema } = ctfData.fields;

    const mappedContent = this.mapPageContent(pageContent, schema?.template);
    const seoImage = getUrlFromContentfulImage(
      seoMetadata[0]?.fields?.ogImage?.fields?.image,
    );

    return {
      ...seoMetadata[0]?.fields,
      ...schema,
      twitter_image: seoImage,
      og_image: seoImage,
      image_title: seoImage,
      ...mappedContent,
    };
  }

  private mapPageContent(pageContent: any[], template: string) {
    const ctfReport = pageContent.find(
      (item) =>
        item.fields &&
        item.fields.componentName === 'report-cta' &&
        template !== TEMPLATES.Industry,
    );

    const ctfComponents = pageContent.filter(
      (item) => item.fields && item.fields.componentName !== 'report-cta',
    );

    const components = ctfComponents
      .map((component) => {
        try {
          const mappedComponent = this.getComponentFromEntry(component);

          // Logs the entry that won't be rendered
          if (!mappedComponent) {
            // eslint-disable-next-line no-console
            console.warn(this.buildComponentNotRenderedMessage(component));
          }

          return mappedComponent;
        } catch (error) {
          // eslint-disable-next-line no-console
          console.warn(this.buildComponentNotRenderedMessage(component, error));
          return null;
        }
      })
      .filter((component) => component);

    const report_cta: any = ctfReport ? mapReportCta(ctfReport.fields) : {};

    return { components, report_cta: report_cta.data };
  }

  /**
   * Main method that switches which components should be used depending on the Content Type and Component Name
   * @param ctfEntry
   */
  private getComponentFromEntry({ fields, sys }: CtfEntry<any>) {
    const contentType = sys?.contentType?.sys?.id;
    const component = componentMapper.find(
      (item) =>
        (item.componentName === fields?.componentName &&
          item.contentType === contentType) ||
        (item.contentType === contentType && item.default),
    );

    if (component) {
      return component.contentType === CONTENT_TYPES.SIDE_MENU
        ? component.mapComponent(fields, this.getComponentFromEntry)
        : component.mapComponent(fields);
    } else {
      return null;
    }
  }

  private buildComponentNotRenderedMessage(component: CtfEntry<any>, error?) {
    return `
    An error occurred while building the 'Solutions' component. The page will be rendered without this component
    Path -> ${this.$ctx.route.path}
    Component Name -> ${component.fields.componentName || 'Not defined'}
    Content Type -> ${component.sys.contentType.sys.id}
    Entry Internal Name -> ${component.fields.internalName}
    Page -> ${this.$ctx.route.path}
    ${error ? `Error -> ${error.stack}` : ''}
    `;
  }
}
