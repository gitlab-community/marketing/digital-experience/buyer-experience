import { SupportChildData } from './support-interfaces';

export function supportChildDataHelper(data: any[]) {
  const supportData: SupportChildData = {
    supportHero: {
      title: undefined,
      content: undefined,
      variant: undefined,
      image: undefined,
    },
    side_menu: {
      anchors: {
        text: '',
        data: [],
      },
      hyperlinks: {
        text: '',
        data: [],
      },
    },
    components: [],
    cards: [],
    cardGroups: [],
  };

  // Take in raw data from Contentful.
  const heroObjects = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'eventHero',
  );
  const sideNavObjects = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'sideMenu',
  );
  const rawSections = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'titleDescriptionBodyText',
  );
  const rawCards = data.filter(
    (obj) =>
      obj.sys.contentType.sys.id === 'card' &&
      obj.fields.componentName != 'support-solutions',
  );
  const rawCardGroups = data.filter(
    (obj) =>
      obj.fields.componentName === 'support-solutions' ||
      obj.sys.contentType.sys.id === 'cardGroup',
  );

  const createHero = (content: any) => {
    supportData.supportHero = {
      title: content.fields.title || null,
      content: content.fields.description || null,
      subtitle: content.fields.description || null,
      variant: content.fields.variant,
      primary_btn: content.fields.primaryCta?.fields && {
        url: content.fields.primaryCta?.fields?.externalUrl || null,
        text: content.fields.primaryCta?.fields?.text || null,
        dataGaName: content.fields.primaryCta?.fields?.dataGaName || null,
        dataGaLocation:
          content.fields.primaryCta?.fields?.dataGaLocation || null,
      },
      image: content.fields?.backgroundImage && {
        image_url: content.fields?.backgroundImage?.fields?.file?.url,
        bordered: true,
      },
    };
  };

  const createSideNav = (rawObj: any) => {
    const content = rawObj.fields;

    supportData.side_menu = {
      anchors: {
        text: content.anchorsText,
        data: content.anchors.map((anchor) => {
          const anchorData = {
            title: anchor.fields.linkText,
            href: anchor.fields.anchorLink,
            nodes: [],
          };

          if (anchor.fields.nodes) {
            anchorData.nodes = anchor.fields.nodes.nodes.map((node) => ({
              title: node.text,
              href: node.href,
            }));
          }

          return anchorData;
        }),
      },
      hyperlinks: {
        text: '',
        data: [],
      },
    };
  };

  const createSections = (rawSections: any[]) => {
    const sections = rawSections.map((section) => ({
      header: section.fields.header || null,
      subheader: section.fields.subheader || null,
      headerId: section.fields.anchorId || null,
      text: section.fields.text,
    }));
    supportData.components = sections;
  };

  function cardBuilder(rawCards) {
    const cards = rawCards.map((card) => ({
      title: card.fields.title || null,
      description: card.fields.description || null,
      button: card.fields?.button || null,
      column2Description: card.fields.column2Description || null,
      iconName: card.fields.iconName || null,
      ...card.fields.customFields,
      cardLink: card.fields.cardLink || null,
      cardLinkDataGaName: card.fields.cardLinkDataGaName || null,
      cardLinkDataGaLocation: card.fields.cardLinkDataGaLocation || null,
    }));
    return cards;
  }
  const createCards = (rawCards: any[]) => {
    const cards = cardBuilder(rawCards);
    supportData.cards = cards;
  };

  const createCardGroups = (rawCardGroups: any[]) => {
    const sections = rawCardGroups.map((card) => ({
      header: card.fields.header || card.fields.title || null,
      componentName: card.fields.componentName || null,
      headerAnchorId:
        card.fields.headerAnchorId ||
        card.fields.customFields?.headerAnchorId ||
        null,
      description: card.fields.description || null,
      iconName: card.fields.iconName || null,
      column2Description: card.fields.column2Description || null,
      button: card.fields?.button || null,
      cards: card.fields.card && cardBuilder(card.fields.card),
      ...card.fields.customFields,
    }));
    supportData.cardGroups = sections;
  };

  // Build out the page structure (different for each page)
  if (heroObjects[0]) createHero(heroObjects[0]);
  if (sideNavObjects[0]) createSideNav(sideNavObjects[0]);
  createCards(rawCards);
  createSections(rawSections);
  createCardGroups(rawCardGroups);

  // Return data to Vue template
  return supportData;
}
