export const state = () => ({
  greenhouseApiData: null,
});

export const mutations = {
  setGreenhouseApiData(state, data) {
    state.greenhouseApiData = data;
  },
};

export const actions = {
  async fetchGreenhouseData({ commit }) {
    try {
      const greenhouseResponse = await fetch(
        'https://boards-api.greenhouse.io/v1/boards/gitlab/departments',
      );
      const greenhouseData = await greenhouseResponse.json();
      commit('setGreenhouseApiData', greenhouseData);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error('Error fetching Greenhouse data: ', error);
      return;
    }
  },
};
